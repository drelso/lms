<?php
// Editar grupo

include('inc/header.php');

error_reporting(E_ALL);
ini_set('display_errors', 1);

if( isset( $_GET['id'] ) ) {
	
	require_once( 'inc/config/config.php' );
	
	require_once( 'inc/db/bd.class.php' );
	$bd = new BD();
	
	
	// Consulta tabla materias
	$resultados = $bd->query("SELECT * FROM grupos WHERE id = " . intval( $_GET['id'] ) );
	
	// Consulta a join de tabla de tipo de usuario y usuario
	$tiposUsuarios = $bd->query("SELECT a.id, a.nombre
								FROM usuarios a, tipo_usuario b
								WHERE a.id = b.id_usuario AND b.id_tipo = 2");

	// Consulta a join de tabla de materia
	$materias = $bd->query("SELECT * FROM materia");
	
	// Consulta a join de tabla de periodos
	$periodos = $bd->query("SELECT * FROM periodos");
	
	

	foreach( $resultados as $resultado ) {
		
		// Formulario para editar grupos
		?>
		
	    <form action="<?= BASEDIR; ?>/controlador-bd.php" method="post" class="agregar-grupo" id="agregar-grupo-<?= $resultado['id']; ?>">
	        <h2>Grupo <?= $resultado['id']; ?></h2>
	        <?php
	        
	        if( $tiposUsuarios == false ) {
	            echo 'Hubo un error con la base de datos:' . $bd->error();
	        } else {
	            $output = '<select name="profesor">';
	            $output .= '<option value="0">Elegir profesor</option>';
	            
	            foreach( $tiposUsuarios as $tipoUsuarios ) {
					$selected = ( $resultado['id_profesor'] == $tipoUsuarios['id'] ) ? 'selected' : '';
	                $output .= '<option value="' . $tipoUsuarios['id'] . '" ' . $selected . '>' . $tipoUsuarios['nombre'] . '</option>';
	            } // foreach( $tiposUsuarios as $tipoUsuarios ) {
	            
	            $output .= '</select> <!-- /profesor -->';
	            $output .= '<h6 class="error-profesor"></h6>';
	            echo $output;
	        } // if($tiposUsuarios == false) { ... else ...
	        
	        
	        if( $materias == false ) {
	            echo 'Hubo un error con la base de datos:' . $bd->error();
	        } else {
	            $output = '<select name="materia">';
	            $output .= '<option value="0">Elegir materia</option>';
	            
	            foreach( $materias as $materia ) {
					$selected = ( $resultado['id_materia'] == $materia['id'] ) ? 'selected' : '';
	                $output .= '<option name="materia" value="' . $materia['id'] . '" ' . $selected . '>' . $materia['nombre'] . '</option>';
	            } // foreach( $tiposUsuarios as $tipoUsuarios ) {
	            
	            $output .= '</select> <!-- /materia -->';
	            $output .= '<h6 class="error-materia"></h6>';
	            echo $output;
	        } // if($tiposUsuarios == false) { ... else ...
	        
	        
	        if( $periodos == false ) {
	            echo 'Hubo un error con la base de datos:' . $bd->error();
	        } else {
	            $output = '<select name="periodo">';
	            $output .= '<option value="0">Elegir periodo</option>';
	            
	            foreach( $periodos as $periodo ) {
					$selected = ( $resultado['id_periodo'] == $periodo['id'] ) ? 'selected' : '';
	                $output .= '<option value="' . $periodo['id'] . '" ' . $selected . '>' . $periodo['nombre'] . ' – ' . $periodo['descripcion'] . '</option>';
	            } // foreach( $tiposUsuarios as $tipoUsuarios ) {
	            
	            $output .= '</select> <!-- /periodo -->';
	            $output .= '<h6 class="error-periodo"></h6>';
	            echo $output;
	        } // if($tiposUsuarios == false) { ... else ...
	        ?>
	        
	        <input type="number" name="numero" value="<?= $resultado['numero']; ?>" placeholder="Número" />
	        <h6 class="error-numero"></h6>
	        
	        <input type="submit" value="Actualizar"/>
	        <input type="hidden" name="agregar-grupo" value="<?= $resultado['id']; ?>"/>
	        <img class="loading" style="display: none;" src="inc/img/loading.gif" width="30" height="30"/>
	        
	    </form> <!-- /agregar-grupo-<?= $resultado['id']; ?> -->
		<?php
	} // foreach( $resultados as $resultado ) {
	
	
	require_once( 'inc/func/tabla-usuarios-grupo.func.php' );
	
	echo '<div id="tabla-usuarios-grupo">';
	echo tablaUsuariosGrupo($_GET['id']);
	echo '</div> <!-- /tabla-usuarios-grupo -->';
	
} // if( isset( $_GET['id'] ) ) {

?>

<h3>Subir CSV de grupo</h3>

<h6 class="error-subir-csv" style="display: none;">No se eligió ningún archivo</h6>

<form action="" method="post" id="subir-csv-estudiantes">
	<input type="file" name="csv" id="archivo-csv"/>
	
	<input type="submit" value="Subir"/>
</form>

<div id="id-grupo" style="display: none;"><?= $_GET['id']; ?></div>

<div id="estudiantes-csv"></div><!-- /estudiantes-csv -->

<button id="actualizar-usuarios-grupo" style="display: none;">Actualizar estudiantes</button>

<script type="text/javascript" src="inc/js/jquery-1.11.3.min.js"></script>
<script type="text/javascript" src="inc/js/editar-grupo.js"></script>