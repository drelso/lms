// JavaScript de Edición de grupo

$('.eliminar-usuario-grupo').click(function(e) {
	
	var elemento = $(this).parents('tr');
	
	elemento.remove();
}); // $('.eliminar-usuario-grupo').click(function(e) {

$('#subir-csv-estudiantes').submit(function(e) {
    
	e.preventDefault();
    
    $('.error-subir-csv').hide();
    $('#actualizar-usuarios-grupo').hide();
    
	var file_data = $('#archivo-csv').prop('files')[0];
    
    if( file_data != null ) {
        var form_data = new FormData();                  
        form_data.append('csv', file_data);
        
        console.log('File data ' + file_data);
        
        $.ajax({
            url: 'inc/func/subir-csv-grupo.php',
            dataType: 'text',  // what to expect back from the PHP script, if anything
            cache: false,
            contentType: false,
            processData: false,
            data: form_data,                         
            type: 'post',
            success: function(datos){
                //alert(php_script_response); // display response from the PHP script, if any

                $('#estudiantes-csv').html(datos); 
                
                $('.eliminar-usuario-grupo').click(function(e) {
                    var elemento = $(this).parents('tr');
                    
                    elemento.remove();
                }); // $('.eliminar-usuario-grupo').click(function(e) {
                
                $('#actualizar-usuarios-grupo').show();
            } // success: function(datos){
        }); // $.ajax({
    } else {
        console.log('No se subió ningún archivo');
        
        $('.error-subir-csv').show();
    } // if( file_data != null ) {
     
}); // $('#subir-csv-estudiantes').submit(function(e) {


$('#actualizar-usuarios-grupo').click(function() {
    
    var tablaEstudiantes = $('#estudiantes-csv table tbody tr');
    
    var hayError = false;
    
    var datos = [];
    
    tablaEstudiantes.each(function(i,v) {
        var error = false;
        var mensajeError = 'Error:<br>';
        var matricula   = $(this).find('td input[name="matricula"]').val();
        var nombre      = $(this).find('td input[name="nombre"]').val();
        var correo      = $(this).find('td input[name="correo"]').val();
        
        var actRef      = $(this).find('td input[name="activo-reflexivo"]').val();
        var sensInt     = $(this).find('td input[name="sensitivo-intuitivo"]').val();
        var visVerb     = $(this).find('td input[name="visual-verbal"]').val();
        var secGlob     = $(this).find('td input[name="secuencial-global"]').val();
        var verbal      = $(this).find('td input[name="verbal"]').val();
        var logMat      = $(this).find('td input[name="logico-matematico"]').val();
        var espacial    = $(this).find('td input[name="espacial"]').val();
        var musical     = $(this).find('td input[name="musical"]').val();
        var kines       = $(this).find('td input[name="kinestesico"]').val();
        var intrap      = $(this).find('td input[name="intrapersonal"]').val();
        var interp      = $(this).find('td input[name="interpersonal"]').val();
        var natur       = $(this).find('td input[name="naturalista"]').val();
        
        var errorHTML   = $(this).find('td.error-datos-csv');
        
        errorHTML.html('');
        
        if( matricula.length < 2 || matricula.length > 10 ) {
            error = true;
            mensajeError += 'La matrícula debe contener de 2 a 10 caracteres<br>';
        } // if( matricula.length < 2 || matricula.length > 10 ) {
        
        
        if( nombre.length < 2 || nombre.length > 255 ) {
            error = true;
            mensajeError += 'El nombre debe contener de 2 a 255 caracteres<br>';
        } // if( nombre.length < 2 || nombre.length > 255 ) {
        
        
        if( correo.length < 5 || correo.length > 255 ) {
            error = true;
            mensajeError += 'El correo debe contener de 5 a 255 caracteres<br>';
        } // if( correo.length < 5 || correo.length > 255 ) {
        
        if( !validarCorreo(correo) ) {
            error = true;
            mensajeError += 'El formato del correo es inválido';
        } // if( !validarCorreo(correo) ) {
        
        if( error ) {
            errorHTML.html(mensajeError);
            hayError = true;
        } // if( error ) {
        
        
        if( !hayError ) {
            datos.push({
                matricula:  matricula,
                nombre:     nombre,
                correo:     correo,
                
                actRef:     actRef,
                sensInt:    sensInt,
                visVerb:    visVerb,
                secGlob:    secGlob,
                
                verbal:     verbal,
                logMat:     logMat,
                espacial:   espacial,
                musical:    musical,
                kines:      kines,
                intrap:     intrap,
                interp:     interp,
                natur:      natur
            });
        } // if( !hayError ) {
        
    }); // tablaEstudiantes.each(function(i,v) {
    
    //console.log(JSON.stringify(datos,undefined,4));
    
    if( !hayError ) {
        console.log('Actualizando estudiantes');
        $.ajax({
            url: 'inc/func/actualizar-estudiantes-grupo.php',
            //contentType: 'application/json',
            data: {
                idGrupo:        $('#id-grupo').html(),
                estudiantes:    JSON.stringify(datos)
            },
            type: 'post',
            success: function(datos){
                //alert(php_script_response); // display response from the PHP script, if any

                $('#tabla-usuarios-grupo').html(datos);
                $('#estudiantes-csv').html(''); 
                
                $('.eliminar-usuario-grupo').click(function(e) {
                    var elemento = $(this).parents('tr');
                    
                    elemento.remove();
                }); // $('.eliminar-usuario-grupo').click(function(e) {
                
                $('#actualizar-usuarios-grupo').show();
            } // success: function(datos){
        }); // $.ajax({
    } // if( !hayError ) {
    
}); // $('#actualizar-usuarios-grupo').click(function() {

function validarCorreo( correo ) {
    var pattern = /^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i;
    return pattern.test( correo );
} // function validarCorreo( correo ) {
