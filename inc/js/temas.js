// Script para agregar y acomodar temas

/* Segundo intento de impresión de información
$('#agregar-tema input[type="submit"]').click(function(e) {
	'use strict';
	
	e.preventDefault();
	
	var $this = $('#agregar-tema');
	
	var temas = $this.children('fieldset');
	
	console.log(temas);
	
}); // $('#agregar-tema input[type="submit"]').click(function(e) {
*/

$('#agregar-otro-tema').click(function() {
	'use strict';
	
            //$('#agregar-tema fieldset').append('<input type="text" name="tema[]" placeholder="Módulo" />');
			//$('#temas fieldset:first').clone(true).appendTo('#temas');
			
	$('#campo-agregar-tema fieldset').clone(true).appendTo('#temas');
	
	$( "#temas, #temas .contenidos" ).sortable({
		revert: true,
		axis: 'y',
		//containment: '#temas',
		cursor: 'move',
		delay: 150,
		forcePlaceholderSize: true,
		placeholder: "sortable-placeholder"
	}); // $( "#temas, #temas .contenidos" ).sortable({
	
	$( "fieldset, .contenido" ).disableSelection();
	
	$( "#temas, #temas .contenidos" ).sortable('refresh');
	
	
}); // $('#agregar-tema').click(function() {


$('.agregar-contenido').click(function() {
	'use strict';
	
	$('#campo-agregar-contenido .contenido').clone(true).appendTo($(this).siblings('.contenidos'));
	
	$( "#temas, #temas .contenidos" ).sortable('refresh');
}); // $('.agregar-contenido').click(function() {


$('.agregar-evaluacion').click(function() {
	'use strict';
	
	var evaluacion = $('#campo-agregar-evaluacion .contenido').clone(true);
	var pregunta = $('#campo-agregar-pregunta .pregunta').clone(true);
	var respuesta = $('#campo-agregar-respuesta .respuesta').clone(true);
	
	pregunta.find('.respuestas').append( respuesta );
	
	evaluacion.find('.preguntas').append( pregunta );
	
	evaluacion.appendTo($(this).siblings('.contenidos'));
	
	$( "#temas, #temas .contenidos" ).sortable('refresh');
}); // $('.agregar-evaluacion').click(function() {


$('.agregar-pregunta').click(function() {
	'use strict';
	
	var pregunta = $('#campo-agregar-pregunta .pregunta').clone(true);
	var respuesta = $('#campo-agregar-respuesta .respuesta').clone(true);
	
	pregunta.find('.respuestas').append( respuesta );
	
	pregunta.appendTo($(this).siblings('.preguntas'));
	
	$( "#temas, #temas .contenidos" ).sortable('refresh');
}); // $('.agregar-pregunta').click(function() {


$('.agregar-respuesta').click(function() {
	'use strict';
	
	var respuestas = $(this).siblings('.respuestas');
	
	// Asegura que nunca se agreguen más de 8 respuestas
	// en una pregunta
	if( respuestas.children('.respuesta').length < 8 ) {
	
		$('#campo-agregar-respuesta .respuesta').clone(true).appendTo( respuestas );
		
	} else {
		alert('No pueden agregarse más de ocho (8) respuestas por preguntas');
	} // if( respuestas.children('.respuesta').length <= 8 ) {
	
	$( "#temas, #temas .contenidos" ).sortable('refresh');
}); // $('.agregar-respuesta').click(function() {


$('.eliminar-contenido').click(function() {
	'use strict';
	
	var $this = $(this);
	
	// Bloque para desplegar diálogo de confirmación
	$('#shadow-box h1').html('¿Quiere eliminar este contenido?');
	$('#shadow-box').fadeIn( 300 );
	$('#shadow-box .confirmar').focus();
	
	// Si el usuario confirma se elimina el contenido
	$('#shadow-box .confirmar').on('click', function() {
		$('#shadow-box').fadeOut( 300 );
		$this.parent('.contenido').remove();
		
		$('#shadow-box .confirmar').on('click', function() {});
	}); // $('#shadow-box .confirmar').on('click', function() {
	
	// Si el usuario rechaza desaparece el cuadro de diálogo
	$('#shadow-box .rechazar').on('click', function() {
		$('#shadow-box').fadeOut( 300 );
		
		$('#shadow-box .rechazar').on('click', function() {});
	}); // $('#shadow-box .rechazar').on('click', function() {
		
	$( "#temas, #temas .contenidos" ).sortable('refresh');
}); // $('.eliminar-contenido').click(function() {


$('.eliminar-pregunta').click(function() {
	'use strict';
	
	
	var $this = $(this);
	
	// Bloque para desplegar diálogo de confirmación
	$('#shadow-box h1').html('¿Quiere eliminar esta pregunta?');
	$('#shadow-box').fadeIn( 300 );
	$('#shadow-box .confirmar').focus();
	
	// Si el usuario confirma se elimina el contenido
	$('#shadow-box .confirmar').on('click', function() {
		$('#shadow-box').fadeOut( 300 );
		$this.parent('.pregunta').remove();
		
		$('#shadow-box .confirmar').on('click', function() {});
	}); // $('#shadow-box .confirmar').on('click', function() {
	
	// Si el usuario rechaza desaparece el cuadro de diálogo
	$('#shadow-box .rechazar').on('click', function() {
		$('#shadow-box').fadeOut( 300 );
		
		$('#shadow-box .rechazar').on('click', function() {});
	}); // $('#shadow-box .rechazar').on('click', function() {
	
	
	$( "#temas, #temas .contenidos" ).sortable('refresh');
}); // $('.eliminar-contenido').click(function() {


$('.eliminar-respuesta').click(function() {
	'use strict';
	
	
	var $this = $(this);
	
	// Bloque para desplegar diálogo de confirmación
	$('#shadow-box h1').html('¿Quiere eliminar esta respuesta?');
	$('#shadow-box').fadeIn( 300 );
	$('#shadow-box .confirmar').focus();
	
	// Si el usuario confirma se elimina el contenido
	$('#shadow-box .confirmar').on('click', function() {
		$('#shadow-box').fadeOut( 300 );
		$this.parent('.respuesta').remove();
		
		$('#shadow-box .confirmar').on('click', function() {});
	}); // $('#shadow-box .confirmar').on('click', function() {
	
	// Si el usuario rechaza desaparece el cuadro de diálogo
	$('#shadow-box .rechazar').on('click', function() {
		$('#shadow-box').fadeOut( 300 );
		
		$('#shadow-box .rechazar').on('click', function() {});
	}); // $('#shadow-box .rechazar').on('click', function() {
	
	
	$( "#temas, #temas .contenidos" ).sortable('refresh');
}); // $('.eliminar-contenido').click(function() {


$(function() {
	'use strict';
	
	// Inicia el módulo 'sortable' para mover
	// y reacomodar los módulos y contenidos
	$( "#temas, #temas .contenidos" ).sortable({
		revert: true,
		axis: 'y',
		//containment: '#temas',
		cursor: 'move',
		delay: 150,
		forcePlaceholderSize: true,
		placeholder: "sortable-placeholder"
	}); // $( "#temas, #temas .contenidos" ).sortable({
	
	$( "fieldset, .contenido" ).disableSelection();
}); // $(function() {


$('#agregar-tema').submit(function(e) {
	'use strict';
	
    e.preventDefault();
	
	console.log('AGREGANDO TEMA');
	
	// Variable de datos a enviar
	var datos = {};
	
	var id = $(this).children('input[name="id"]');
	
	if( id ) {
		datos.id = id.val();
	} // if( id ) {
	
	var nombreMateria = $(this).children('input[name="nombre-materia"]');
	
	if( nombreMateria ) {
		datos.nombreMateria = nombreMateria.val();
	} // if( nombreMateria ) {
	
	var departamento = $(this).children('select[name="departamento"]');
	
	if( departamento ) {
		datos.departamento = departamento.val();
	} // if( departamento ) {
	
	datos.temas = [];
	
	var temas = $(this).find('fieldset.tema');
	
	temas.each(function(index, element) {

		// Elemento de la variable datos
		var elemento = {};

		var tema = temas.eq( index );
        //console.log( tema.find('input[name="tema[]"]').val() );
		
       	elemento.nombre = tema.children('input[name="tema[]"]')[0].value;

		//console.log( 'tema ' + tema.children('input[name="tema[]"]')[0].value );
		
		//console.log('DUMP ' + dump(tema.children('input[name="tema[]"]')[0].value));
		
		var contenidos = tema.find('.contenidos .contenido');
		
		elemento.contenidos = [];

		contenidos.each(function(i, el) {
            
			var contenido = contenidos.eq( i );
			var elementoContenido = {};

			var nombreContenido = contenido.find('input[name="nombre-contenido"]')[0];
	
			if( nombreContenido ) {
				//console.log( 'Contenido: ' + nombreContenido.value );
				elementoContenido.nombre = nombreContenido.value;
			} // if( contenido.find('input[name="nombre-contenido"]')[0] ) {
			
			var tipoContenido = contenido.find('select[name="tipo-contenido"]')[0];
			
			if( tipoContenido ) {
				//console.log( 'Tipo: ' + tipoContenido.value );
				elementoContenido.tipo = tipoContenido.value;
			} // if( tipoContenido ) {
			
			var archivoContenido = contenido.find('input[name="contenido"]')[0];
			
			
			if( archivoContenido != undefined ) {
				
				var tieneArchivo = false;
				
				if( archivoContenido.value == '' ) {
					
					console.log('Archivo no definido ' + nombreContenido.value );
					
					var archivoActual = contenido.find('input[name="contenido-actual"]')[0];
					
					if( archivoActual != undefined ) {
						if( archivoActual.value != '' ) {
							console.log('Archivo actual ' + nombreContenido.value + ' = ' + archivoActual.value);
							archivoContenido = archivoActual;
							
							tieneArchivo = true;
						} // if( archivoActual.value != '' ) {
					} // if( archivoActual != undefined ) {
				} // if( archivoContenido == undefined ) {
				
				
				console.log('Archivo contenido ' + nombreContenido.value + ' = ' + archivoContenido.value);
				if( archivoContenido.value != '' ) {
					//console.log( 'Archivo: ' + archivoContenido.value );
					//elementoContenido.archivo = archivoContenido.value;
					
					if( !tieneArchivo ) {
						elementoContenido.archivo = '';
						
						// Variable para envío de archivos por AJAX
						// *** NO ES COMPATIBLE CON IE < 10
				    	var archivo = contenido.find('input[name="contenido"]').prop('files')[0];
						
						var formData = new FormData();
		    			formData.append('archivo', archivo);
						
						console.log('EXISTE ARCHIVO ' + archivoContenido.value);
						
						$.ajax({
			                url: 'inc/func/subir-archivo.php', // point to server-side PHP script 
			                dataType: 'text',  // what to expect back from the PHP script, if anything
			                cache: false,
			                contentType: false,
			                processData: false,
			                data: formData,                         
			                type: 'post',
			                async: false,
			                error: function(xhr, status, error) {
								var err = eval("(" + xhr.responseText + ")");
								alert(err.Message);
							},
			                success: function( resultado ) {
			                	
			                    console.log( resultado );
			                    
			                    // Si el resultado es igual a -1
			                    // hubo un error con la carga del archivo
			                    if( resultado != '-1' ) {
			                    	console.log();
			                    	elementoContenido.archivo = resultado;
			                    } else {
			                    	console.log('Hubo un error al subir el archivo');
			                    } //  if( resultado != '-1' ) {
			                    
			                } // success: function( resultado ) {
			                
					    }); // $.ajax({
						
						console.log('TERMINA EXISTE ARCHIVO');
					} else {
						elementoContenido.archivo = archivoContenido.value;
					} // if( !tieneArchivo ) { ... else ...
				} // if( archivoContenido.value != ''  ) {
			} // if( archivoContenido != 'undefined' ) {
			
			
			var iframeContenido = contenido.find('input[name="iframe-contenido"]')[0];
	
			if( iframeContenido ) {
				elementoContenido.iframe = iframeContenido.value;
			} // if( contenido.find('input[name="iframe-contenido"]')[0] ) {
			
			
			var textoContenido = contenido.find('textarea[name="contenido-texto"]')[0];

			if( textoContenido ) {
				//console.log( 'Texto: ' + textoContenido.value );
				elementoContenido.texto = textoContenido.value;
			} // if( textoContenido ) {
			

			var preguntas = contenido.find('.preguntas .pregunta');
			
			elementoContenido.preguntas = [];
			
			if( preguntas.length > 0 ) {
				
				preguntas.each(function(index, element) {
					var elementoPregunta = {};
                    
					var pregunta = preguntas.eq( index );
					
					var textoPregunta = pregunta.find( 'input[name="pregunta"]' );
					
					if( textoPregunta ) {
						//console.log( textoPregunta.val() );
						elementoPregunta.pregunta = textoPregunta.val();
					} // if( textoPregunta ) {
					
					var imagenPregunta = pregunta.find( 'input[name="imagen-pregunta"]' );
							
					if( imagenPregunta != undefined ) {
						
						var tieneImagen = false;
						
						if( imagenPregunta.val() == '' ) {
							
							console.log('Imagen de pregunta no definida ' + textoPregunta.val() );
							
							var imagenActual = pregunta.find('input[name="imagen-actual"]')[0];
							
							if( imagenActual != undefined ) {
								if( imagenActual.value != '' ) {
									console.log('Imagen actual pregunta ' + textoPregunta.val() + ' = ' + imagenActual.value);
									imagenPregunta = imagenActual;
									
									tieneImagen = true;
								} // if( imagenActual.value != '' ) {
							} // if( imagenActual != undefined ) {
						} // if( imagenPregunta.value == '' ) {
						
						
						if( imagenPregunta.value != '' ) {
							
							if( !tieneImagen ) {
								elementoPregunta.imagen = '';
								
								// Variable para envío de archivos por AJAX
								// *** NO ES COMPATIBLE CON IE < 10
						    	var imagen = pregunta.find('input[name="imagen-pregunta"]').prop('files')[0];
								
								var formData = new FormData();
				    			formData.append('archivo', imagen);
								
								
								$.ajax({
					                url: 'inc/func/subir-archivo.php', // point to server-side PHP script 
					                dataType: 'text',  // what to expect back from the PHP script, if anything
					                cache: false,
					                contentType: false,
					                processData: false,
					                data: formData,                         
					                type: 'post',
					                async: false,
					                error: function(xhr, status, error) {
										var err = eval("(" + xhr.responseText + ")");
										alert(err.Message);
									},
					                success: function( resultado ) {
					                	
					                    console.log( resultado );
					                    
					                    // Si el resultado es igual a -1
					                    // hubo un error con la carga del archivo
					                    if( resultado != '-1' ) {
					                    	console.log('Imagen subida ' + resultado);
					                    	elementoPregunta.imagen = resultado;
					                    } else {
					                    	console.log('Hubo un error al subir el archivo');
					                    } //  if( resultado != '-1' ) {
					                    
					                } // success: function( resultado ) {
					                
							    }); // $.ajax({
							} else {
								elementoPregunta.imagen = imagenPregunta.value;
							} // if( !tienImagen ) { ... else ...
						} // if( imagenPregunta.value != ''  ) {
					} // if( imagenPregunta != 'undefined' ) {
					
					
					
					var respuestas = pregunta.find('.respuestas .respuesta');
					
					if( respuestas.length > 0 ) {
						
						var elementoRespuestas = [];
						
						respuestas.each(function(index, element) {
                            var respuesta = respuestas.eq( index );
							
							var elementoRespuesta = {};
							
							var datoRespuesta = respuesta.find( 'input[name="respuesta"]' );
							
							if( datoRespuesta ) {
								//console.log( datoRespuesta.val() );
								elementoRespuesta.respuesta = datoRespuesta.val();
							} // if( datoRespuesta ) {
								
							var imagenRespuesta = respuesta.find( 'input[name="imagen-respuesta"]' );
							
							if( imagenRespuesta != undefined ) {
								
								var tieneImagen = false;
								
								if( imagenRespuesta.val() == '' ) {
									
									console.log('Imagen de respuesta no definida ' + datoRespuesta.val() );
									
									var imagenActual = respuesta.find('input[name="imagen-respuesta-actual"]')[0];
									
									if( imagenActual != undefined ) {
										if( imagenActual.value != '' ) {
											console.log('Imagen actual respuesta ' + datoRespuesta.val() + ' = ' + imagenActual.value);
											imagenRespuesta = imagenActual;
											
											tieneImagen = true;
										} // if( imagenActual.value != '' ) {
									} // if( imagenActual != undefined ) {
								} // if( imagenRespuesta.value == '' ) {
								
								
								if( imagenRespuesta.value != '' ) {
									
									if( !tieneImagen ) {
										elementoRespuesta.imagen = '';
										
										// Variable para envío de archivos por AJAX
										// *** NO ES COMPATIBLE CON IE < 10
								    	var imagen = respuesta.find('input[name="imagen-respuesta"]').prop('files')[0];
										
										var formData = new FormData();
						    			formData.append('archivo', imagen);
										
										
										$.ajax({
							                url: 'inc/func/subir-archivo.php', // point to server-side PHP script 
							                dataType: 'text',  // what to expect back from the PHP script, if anything
							                cache: false,
							                contentType: false,
							                processData: false,
							                data: formData,                         
							                type: 'post',
							                async: false,
							                error: function(xhr, status, error) {
												var err = eval("(" + xhr.responseText + ")");
												alert(err.Message);
											},
							                success: function( resultado ) {
							                	
							                    console.log( resultado );
							                    
							                    // Si el resultado es igual a -1
							                    // hubo un error con la carga del archivo
							                    if( resultado != '-1' ) {
							                    	console.log('Imagen subida ' + resultado);
							                    	elementoRespuesta.imagen = resultado;
							                    } else {
							                    	console.log('Hubo un error al subir el archivo');
							                    } //  if( resultado != '-1' ) {
							                    
							                } // success: function( resultado ) {
							                
									    }); // $.ajax({
									} else {
										elementoRespuesta.imagen = imagenRespuesta.value;
									} // if( !tienImagen ) { ... else ...
								} // if( imagenRespuesta.value != ''  ) {
							} // if( imagenRespuesta != 'undefined' ) {
							
							
							
							
							if( respuesta.find( 'label input[type="checkbox"]:checked' ).val() ) {
								elementoRespuesta.correcta = true;
								//console.log( "Opción correcta" );
							} else {
								elementoRespuesta.correcta = false;
							} // if( respuesta.find( 'label input[type="checkbox"]...
							
							elementoRespuestas.push(elementoRespuesta);
							
                        }); // respuestas.each(function(index, element) {
						
						elementoPregunta.respuestas = elementoRespuestas;
						
					} // if( respuestas.length > 0 ) {
					
					elementoContenido.preguntas.push(elementoPregunta);
					
                }); // preguntas.each(function(index, element) {
				
			} // if( preguntas.length > 0 ) {

			elemento.contenidos.push(elementoContenido);
			
        }); // contenidos.each(function(i, el) {
	    
	    datos.temas.push(elemento);
	
    }); // temas.each(function(index, element) {
	
	//console.log('EMPIEZA JSON');
	
	//console.log(JSON.stringify(datos));
	
	
	// AJAX para actualizar la base de datos
	// al dar clic sobreescribe todo el contenido
	// de la materia
	$.ajax({
		type: 'post',
		url: '.../../actualizar-materia.php',
		data: JSON.stringify( datos ),
		contentType: "application/json; charset=utf-8",
		traditional: true,
		success: function (data) {
			
			$('#resultado-materia').html(data);
			
			console.log( data );
		}
	}); // $.ajax({
	
}); // $('#agregar-tema').submit(function(e) {





function dump(obj) {
	'use strict';
	
    var out = '';
    for (var i in obj) {
        out += i + ": " + obj[i] + "\n";
    }
	
	return out;
}