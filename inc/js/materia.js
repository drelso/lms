/*
JS de la sección de Materia
*/

// Confirmación del usuario al cerrar el navegador
function advertenciaAlCerrar() {
    return '¿Está seguro que desea abandonar la plataforma?'
} // function advertenciaAlCerrar() {

window.onbeforeunload = advertenciaAlCerrar;


$('.contenedor-estructuras label').click(function(e) {
	$('.contenedor-estructuras label').removeClass('estructura-seleccionada');
	$(this).addClass('estructura-seleccionada');
}); // $('.contenedor-estructuras label').click(function(e)) {


$('.siguiente-paso').click(function(e) {
	
	var id = $(this).data('id');
	var tipo = $(this).data('tipo');
	var error = false;
	
	if( id == 1 ) {
		console.log('Paso 1 a Paso 2');
		
		var idColores = $('input[name="idcolores"]').val();
		
		var mensajeError = '';
		
		if( idColores == '' ) {
			mensajeError += 'Debe seleccionar una paleta de colores<br>';
			error = true;
		} // if( idColores == '' ) {
		
		var estructura = $('input[name="estructura"]:checked').val();
		
		if( estructura == '' || estructura == undefined ) {
			mensajeError += 'Debe seleccionar una estructura';
			error = true;
		} // if( estructura == '' ) {
		
		$('#error-paso-1').html(mensajeError);
		
	} else {
			
		
		if( tipo == 'contenido' ) {
			
			var numSeleccionables = $('#paso-' + id + ' .seleccionable').length;
			
			if( numSeleccionables != 0 ) {
				$('#error-paso-' + id).html('Debe consultar un recurso de aprendizaje antes de continuar');
				error = true;
			} // if( numSeleccionables != 0 ) {
			
		} else if( tipo == 'evaluacion') {
			
			$('#paso-' + id + ' .respuestas').each(function() {
				var numRespuestasSeleccionadas = $(this).children('.respuesta-seleccionada').length;
				
				if( numRespuestasSeleccionadas == 0 ) {
					$('#error-paso-' + id).html('Debe responder todas las preguntas antes de continuar');
					error = true;
				} // if( numRespuestasSeleccionadas != 0 ) {
				
			}); // $('#paso-' + id + ' .respuestas').each(function() {
			
		} // if( tipo == 'contenido' ) { ... else if ...
		
	} // if( id == 1 ) {
	
	
	// A menos que haya un error o sea el último paso
	// cierra el paso anterior y abre el siguiente
	if( !error ) {
		
		if( id == 1 ) {
			$('#encabezado-general').hide();
			$('.encabezado-materia').show();
			
			
			estructura 		= $('.contenedor-estructuras input[name="estructura"]:checked').val();
			
			// Colores seleccionados
			var principal = $('#primarioe').val();
			var complementario = $('#primariob').val();
			var complementarioHover = $('#primarioc').val();
			var medio = $('#secundario1a').val();
			var secundario = $('#secundario2a').val();
			
			var intEstructura = parseInt(estructura);
			
			var numEstructura = ( intEstructura >= 1 && intEstructura <= 4 ) ? intEstructura : 1;
			
			var linkEstilos = 'inc/css/estilos-' + numEstructura + '.php?'
			
			if(principal != '') { linkEstilos += 'p=' + principal + '&'; }
			if(complementario != '') { linkEstilos += 'c=' + complementario + '&'; }
			if(complementarioHover != '') { linkEstilos += 'ch=' + complementarioHover + '&'; }
			if(medio != '') { linkEstilos += 'm=' + medio + '&'; }
			if(secundario != '') { linkEstilos += 's=' + secundario; }
			
			
			$('#hoja-de-estilos').attr('href', linkEstilos);
			
			idcolores 		= $('#idcolores').val();
			
			primarioa 		= $('#primarioa').val();
			primariob 		= $('#primariob').val();
			primarioc 		= $('#primarioc').val();
			primariod 		= $('#primariod').val();
			primarioe 		= $('#primarioe').val();
			
			secundario1a	= $('#secundario1a').val();
			secundario1b	= $('#secundario1b').val();
			secundario1c	= $('#secundario1c').val();
			secundario1d	= $('#secundario1d').val();
			secundario1e	= $('#secundario1e').val();
			
			secundario2a	= $('#secundario2a').val();
			secundario2b	= $('#secundario2b').val();
			secundario2c	= $('#secundario2c').val();
			secundario2d	= $('#secundario2d').val();
			secundario2e	= $('#secundario2e').val();
			
			
			$.ajax({
				url: "controlador-bd-materia.php",
				method: "POST",
				data: {
					preferencias : 'true',
					
					estructura :estructura,
					idcolores : idcolores,
					
					primarioa : primarioa,
					primariob : primariob,
					primarioc : primarioc,
					primariod : primariod,
					primarioe : primarioe,
					
					secundario1a :secundario1a,
					secundario1b :secundario1b,
					secundario1c :secundario1c,
					secundario1d :secundario1d,
					secundario1e :secundario1e,
					
					secundario2a :secundario2a,
					secundario2b :secundario2b,
					secundario2c :secundario2c,
					secundario2d :secundario2d,
					secundario2e :secundario2e
				}
			})
			.done(function( data ) {
				console.log('AJAX terminado');
				console.log( data );
			})
			.fail(function( jqXHR, textStatus ) {
				console.log( "Error de AJAX: " + jqXHR + '  ' + textStatus );
			});
			
		} else {
			
			if( tipo == 'contenido' ) {
				
				var idGrupo = $('#titulo-materia').data('idgrupo');
				var idTema = $('#paso-' + id).data('id');
				var idContenido = $('#paso-' + id + ' .contenido-seleccionado').data('id');
				
				console.log('Almacenando contenido');
				
				$.ajax({
					url: "controlador-bd-materia.php",
					method: "POST",
					data: {
						contenido : 'true',
						
						idGrupo : idGrupo,
						idTema : idTema,
						idContenido : idContenido,
					}
				})
				.done(function( data ) {
					console.log('AJAX contenido terminado');
					console.log( data );
				})
				.fail(function( jqXHR, textStatus ) {
					console.log( "Error de AJAX: " + jqXHR + '  ' + textStatus );
				});
				
			} // if( tipo == 'contenido' ) {
			
			
		} // if( id == 1 ) {
		
		
		$('#menu-materia li[data-id="' + id + '"]').addClass('visitado');
		$('#menu-materia li').removeClass('activo');
		$('#menu-materia li[data-id="' + ( id + 1 ) + '"]').addClass('activo');
		
		$('#paso-' + id).hide();
		$('#paso-' + (id + 1)).show();
		
		$(window).scrollTop(0);
		
	} // if( !error && !ultimoPaso ) {
	
}); // $('.siguiente-paso').click(function(e) {


$('.enviar-evaluacion').click(function(e) {
	var id = $(this).data('id');
	var error = false;
	var preguntas = $('#paso-' + id + ' .pregunta');
	
	$('#paso-' + id + ' .respuestas').each(function() {
		var numRespuestasSeleccionadas = $(this).children('.respuesta-seleccionada').length;
		
		if( numRespuestasSeleccionadas == 0 ) {
			$('#error-paso-' + id).html('Debe responder todas las preguntas antes de continuar');
			error = true;
		} // if( numRespuestasSeleccionadas != 0 ) {
		
	}); // $('#paso-' + id + ' .respuestas').each(function() {
	
	
	// A menos que haya un error
	// cierra la evaluación y abre la calificación
	if( !error ) {

	console.log('Almacenando evaluación');
	
	var datos = [];
	
		preguntas.each(function(e) {
			var pregunta = $(this);
			
			var idEvaluacion = pregunta.data('idevaluacion');
			var orden = pregunta.data('orden');
			
			var respuestas = [];
			
			var idGrupo = $('#titulo-materia').data('idgrupo');
			
			pregunta.find('input[name="checkbox-respuesta"]:checked').each(function() {
				respuestas.push($(this).val());
			});
			
			datos.push({
				idEvaluacion: idEvaluacion,
				orden: orden,
				respuestas: respuestas,
				idGrupo : idGrupo
			});
			
		}); // preguntas.each(function(e) {
		
		for( var obj in datos ) {
			console.log( dump( datos[obj] ) );
		}
		
		
		$.ajax({
			url: "controlador-bd-materia.php",
			method: "POST",
			data: {
				evaluacion: 'true',
				
				datos: JSON.stringify(datos)
			}
		})
		.done(function( data ) {
			
			
			$('#paso-' + id + ' .resultados-evaluacion h2').html(data);
			
			console.log('AJAX contenido terminado');
			console.log( data );
		})
		.fail(function( jqXHR, textStatus ) {
			console.log( "Error de AJAX: " + jqXHR + '  ' + textStatus );
		}); // $.ajax({
		
		
		$('#paso-' + id + ' .contenido-evaluacion').hide();
		$('#paso-' + id + ' .resultados-evaluacion').show();
		
	}  // if( !error ) {
}); // $('.enviar-evaluacion').click(function(e) {


function dump(obj) {
	'use strict';
	
    var out = '';
    for (var i in obj) {
        out += i + ": " + obj[i] + "\n";
    }
	
	return out;
}


$('#boton-finalizar').click(function(e) {
	var id = $(this).data('id');
	
	var error = false;
	
	var mensajeError = '';
	var errorIncompleto = false;
	var errorLongitud = false;
	
	var idGrupo = $('#titulo-materia').data('idgrupo');
	
	$('#paso-' + id + ' .opcion-multiple .respuestas').each(function() {
		numRespuestasSeleccionadas = $(this).children('.respuesta-seleccionada').length;
		
		if( numRespuestasSeleccionadas == 0 ) {
			errorIncompleto = true;
			error = true;
		} // if( numRespuestasSeleccionadas != 0 ) {
		
	}); // $('#paso-' + id + ' .respuestas').each(function() {
	
	
	$('#paso-' + id + ' .respuestas textarea').each(function() {
		if( $(this).val() == '' ) {
			errorIncompleto = true;
			error = true;
		} // if( $(this).val() == '' ) {
		
		if( $(this).val().length > 1000 ) {
			errorLongitud = true;
			error = true;
		} // if( $(this).val().length > 1000 ) {
	}); // $('#paso-' + id + ' .respuestas textarea').each(function() {
	
	if( errorIncompleto ) {
		mensajeError += 'Debe responder todas las preguntas antes de finalizar<br>';
	} // if( errorIncompleto ) {
	
	if( errorLongitud ) {
		mensajeError += 'Las respuestas no pueden tener más de 1,000 caracteres<br>';
	} // if( errorLongitud ) {
		
	if( !error ) {
		
		var respuesta1 = $('#retro-1 .respuestas input:checked').val();
		var respuesta2 = $('#retro-2 .respuestas input:checked').val();
		var respuesta3 = $('#retro-3 .respuestas input:checked').val();
		var respuesta4 = $('#retro-4 .respuestas input:checked').val();
		var respuesta5 = $('#retro-5 .respuestas input:checked').val();
		var respuesta6 = $('#retro-6 .respuestas input:checked').val();
		
		var respuesta7 = $('#retro-7 .respuestas textarea').val();
		var respuesta8 = $('#retro-8 .respuestas textarea').val();
		var respuesta9 = $('#retro-9 .respuestas textarea').val();
		
		
		$.ajax({
			url: "controlador-bd-materia.php",
			method: "POST",
			data: {
				retroalimentacion : 'true',
				
				idGrupo : idGrupo,
				
				respuesta1 : respuesta1,
				respuesta2 : respuesta2,
				respuesta3 : respuesta3,
				respuesta4 : respuesta4,
				respuesta5 : respuesta5,
				respuesta6 : respuesta6,
				
				respuesta7 : respuesta7,
				respuesta8 : respuesta8,
				respuesta9 : respuesta9
				
			}
		})
		.done(function( data ) {
			console.log('AJAX retro terminado');
			console.log( data );
		})
		.fail(function( jqXHR, textStatus ) {
			console.log( "Error de AJAX: " + jqXHR + '  ' + textStatus );
		}); // $.ajax({
		
		
		$('#paso-' + id).hide();
		$('#agradecimiento').show();
	} else {
		$('#error-finalizar').html(mensajeError);
	} // if( !error ) {
	
}); // $('#boton-finalizar').click(function(e) {


// Código para desplegar el recurso seleccionado
// y desactivar el resto
$('.seleccionable').click(function(e) {
	var contenedor = $(this).parent('.contenidos');
	var id = contenedor.data('id');
	
	var ancho = contenedor.outerWidth(false);
	
	var $this = $(this);
	
	contenedor.children('.contenido').not(this).animate({ opacity: 0.6 }, 300).addClass('inactivo');
	
	$this.addClass('contenido-seleccionado');
	
	$this.animate({ width: ancho + 'px' }, 300, function() {
		$this.children('.info-contenido').slideDown();
	});
	
	contenedor.children('.contenido').removeClass('seleccionable').unbind('click');
}); // $('.seleccionable').click(function(e) {


// Código para seleccionar y resaltar la respuesta seleccionada
$('.respuestas input[type="checkbox"]').click(function(e) {
	$(this).parent('label').toggleClass('respuesta-seleccionada');
});

$('.respuestas input[type="radio"]').click(function(e) {
	var etiqueta = $(this).parent('label');
	
	etiqueta.siblings('label').removeClass('respuesta-seleccionada');
	
	etiqueta.addClass('respuesta-seleccionada');
});