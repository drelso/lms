// JavaScript Global

$(document).ready(function(e) {
	$('#interfaces-adaptativas').mouseenter(function(e) {
        $('#interfaces-adaptativas span').stop().fadeIn(300);
    });
	
	$('#interfaces-adaptativas').mouseleave(function(e) {
        $('#interfaces-adaptativas span').stop().fadeOut(300);
    });
	
    $('.ayuda-colores').hide();
	
	$('#mostrar-instrucciones').click(function(e) {
        $('.ayuda-colores').slideDown(500);
    });
	
	$('#cerrar-instrucciones').click(function(e) {
        $('.ayuda-colores').slideUp(500);
    });
});

/* The Colorizer OPTIONS object */

var colorizerOptions = {

	width:	960,
	/* min width 640 */

	height: 500,
	/* min height 500 */

	dark: false,
	/* or true */

	//templateURL: 'http://colorschemedesigner.com/colorizer-demo/template-less/index.html',
	templateURL: 'http://localhost/lms/inc/materia/plantillas-paletton/demo-colores2.html',
	paletteUID: '53K0u0kllll1h++89I7rXeByi9v',
	colorizeMode: 'class'
}

var btn = document.getElementById('col-btn-less');

btn.onclick = function(e)
{
	e.preventDefault();
	_paletton.open(colorizerOptions, colorizerCallback);
}

function colorizerCallback(data){
	
	var res = document.getElementById('resultado-color'), html = '';

	if (!data)
	{
		res.innerHTML = 'cancelled';
		return
	}
	
	colorizerOptions.paletteUID = data.paletteUID;
	
	function parse(obj,prefix)
	{
		// a dummy recursive parser just to print out the data
		var k, x, str = '{';//\n';
		for (k in obj) {
			x = obj[k];
			str += prefix + '' + k + ':';
			if (typeof x==='undefined' || x===null) str += 'null';
			else if (typeof x==='object') str += parse(x, prefix + '');
			else str += x;
			//str += '\n';
		}
		str += prefix + '' + '}';
		return str;
	}
	
	console.log(data.colorTable.byPalette);
	
	/*
	// Función AJAX para cambiar los valores
	// de las variables de sesión de los colores
	$.ajax({
		url: "inc/materia/plantillas-paletton/actualizar-colores.php",
		data: {
			PRINCIPAL:				data.colorTable.byPalette.pri[4],
			COMPLEMENTARIO:			data.colorTable.byPalette.pri[1],
			COMPLEMENTARIOHOVER:	data.colorTable.byPalette.pri[2],
			MEDIO:					data.colorTable.byPalette.sec1[0],
			SECUNDARIO:				data.colorTable.byPalette.sec2[0]
			
		},
		type: "POST",
		success: function( json ) {
			//$('#res2').html( json.html );
			$('iframe').attr( 'src', function ( i, val ) { return val; });
		},
		error: function( xhr, status, errorThrown ) {
			console.log( "Sorry, there was a problem!" );
			console.log( "Error: " + errorThrown );
			console.log( "Status: " + status );
			console.dir( xhr );
		},
		complete: function( xhr, status ) {
			console.log( "The request is complete!" );
		}
	}); // $.ajax({
	*/
	
	// Cambia los colores de los estilos de las estructuras
	$('.fondo-principal').css('background-color', data.colorTable.byPalette.pri[4]);
	$('.fondo-complementario').css('background-color', data.colorTable.byPalette.pri[1]);
	$('.fondo-complementario-hover').css('background-color', data.colorTable.byPalette.pri[2]);
	$('.fondo-medio').css('background-col', data.colorTable.byPalette.sec1[0])
	$('.fondo-secundario').css('background-color', data.colorTable.byPalette.sec2[0]);
	
	$('.borde-principal').css('border-color', data.colorTable.byPalette.pri[4]);
	$('.borde-complementario').css('border-color', data.colorTable.byPalette.pri[1]);
	$('.borde-complementario-hover').css('border-color', data.colorTable.byPalette.pri[2]);
	$('.borde-medio').css('border-color', data.colorTable.byPalette.sec1[0]);
	$('.borde-secundario').css('border-color', data.colorTable.byPalette.sec2[0]);
	
	$('.color-principal, .color-principal *').css('color', data.colorTable.byPalette.pri[4]);
	$('.color-complementario, .color-complementario *').css('color', data.colorTable.byPalette.pri[1]);
	$('.color-complementario-hover, .color-complementario-hover *').css('color', data.colorTable.byPalette.pri[2]);
	$('.color-medio, .color-medio *').css('color', data.colorTable.byPalette.sec1[0]);
	$('.color-secundario, .color-secundario *').css('color', data.colorTable.byPalette.sec2[0]);
	
	resultadoColor = '<h2>Paleta seleccionada</h2>';
	resultadoColor += '<div class="muestra-color" style="background: '+data.colorTable.byPalette.pri[4]+'"></div>';
	resultadoColor += '<div class="muestra-color" style="background: '+data.colorTable.byPalette.pri[1]+'"></div>';
	resultadoColor += '<div class="muestra-color" style="background: '+data.colorTable.byPalette.pri[2]+'"></div>';
	resultadoColor += '<div class="muestra-color" style="background: '+data.colorTable.byPalette.sec1[0]+'"></div>';
	resultadoColor += '<div class="muestra-color" style="background: '+data.colorTable.byPalette.sec2[0]+'"></div>';
	
	$('#resultado-color').html(resultadoColor);
	
	
	$('#idcolores').val(data.paletteUID);
	
	$('#primarioa').val(data.colorTable.byPalette.pri[0].replace('#',''));
	$('#primariob').val(data.colorTable.byPalette.pri[1].replace('#',''));
	$('#primarioc').val(data.colorTable.byPalette.pri[2].replace('#',''));
	$('#primariod').val(data.colorTable.byPalette.pri[3].replace('#',''));
	$('#primarioe').val(data.colorTable.byPalette.pri[4].replace('#',''));
	
	$('#secundario1a').val(data.colorTable.byPalette.sec1[0].replace('#',''));
	$('#secundario1b').val(data.colorTable.byPalette.sec1[1].replace('#',''));
	$('#secundario1c').val(data.colorTable.byPalette.sec1[2].replace('#',''));
	$('#secundario1d').val(data.colorTable.byPalette.sec1[3].replace('#',''));
	$('#secundario1e').val(data.colorTable.byPalette.sec1[4].replace('#',''));
	
	$('#secundario2a').val(data.colorTable.byPalette.sec2[0].replace('#',''));
	$('#secundario2b').val(data.colorTable.byPalette.sec2[1].replace('#',''));
	$('#secundario2c').val(data.colorTable.byPalette.sec2[2].replace('#',''));
	$('#secundario2d').val(data.colorTable.byPalette.sec2[3].replace('#',''));
	$('#secundario2e').val(data.colorTable.byPalette.sec2[4].replace('#',''));
	
	//html = parse(data,'');
	//res.innerHTML = html;

} // callback