// Establecer contraseña

$('#establecer-contrasena').click(function() {
	
	$('#ingresar-contrasena .error').html('');
	
	var contrasena1 = $('#ingresar-contrasena input[name="contrasena"]').val();
	var contrasena2 = $('#ingresar-contrasena input[name="confirmar-contrasena"]').val();
	var idUsuario = $('#ingresar-contrasena input[name="id-usuario"]').val();
	
	var error = false;
	var mensajeError = 'Error:<br>';
	
	if( contrasena1.length < 8 ) {
		error = true;
		mensajeError += 'La contraseña debe contener al menos 8 caracteres<br>';
	}
	
	if( contrasena1 !== contrasena2 ) {
		error = true;
		mensajeError += 'Las contraseñas no coinciden<br>';
	} // if( contrasena1.length < 8 ) { ... else if ...
	
	
	if( !error ) {
		
		$('#ingresar-contrasena input[name="contrasena"]').val('');
		$('#ingresar-contrasena input[name="confirmar-contrasena"]').val('');
		
		$.ajax({
            url: 'inc/usuarios/establecer-contrasena.php',
            data: {
                contrasena: contrasena1
            },
            type: 'post',
            success: function(datos){
                
                $('#ingresar-contrasena').html('<h2>' + datos + '</h2>').delay(2000).fadeOut(300);
                
            } // success: function(datos){
        }); // $.ajax({
        
	} else {
		$('#ingresar-contrasena .error').html( mensajeError );
	} // if( !error ) {
});