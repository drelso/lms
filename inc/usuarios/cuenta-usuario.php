<?php
// Cuenta de usuario autenticado

if(isset($usuario) && !empty($usuario)) {
?>
	
    <div class="main">
    	<?php
		
		foreach( $usuario->getTipo() as $tipo) {
			
			echo '<div class="contenedor-titulo-tipo"><h1 class="titulo-tipo-usuario">' . $tipo['nombre'] . '</h1></div>';
			
			include('inc/lista-contenidos.php');
			
		} // foreach( $usuario->getTipo() as $tipo) {
		?>
    </div> <!-- /main -->
    
<?php
} // if(isset($usuario) && !empty($usuario)) {
?>