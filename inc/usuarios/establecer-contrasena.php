<?php
// Establecer contraseña de usuario

session_start();

if(isset($_SESSION['usuario_registrado'])) {
	require_once( __DIR__ . '/../clases/usuario.class.php');
	
	$id = intval($_SESSION['usuario_registrado']);
	
	if($id > 0) {
		$usuario = new Usuario($id);
	} // if($id > 0) {
	
} // if(isset($_SESSION['usuario_registrado'])) {

// Si existe el objeto de clase usuario
// genera la estructura del perfil
//
if( isset($usuario) && !empty($usuario)
		&& isset($_POST['contrasena']) ) {
	
	require_once( __DIR__ . '/../db/bd.class.php' );
	$bd = new BD();
	
	
	$primerRegistro = $bd->query( "SELECT * FROM primer_registro WHERE id_usuario = " . $usuario->getID() );
	
	if( $primerRegistro == false ) {
		echo 'Hubo un error con la base de datos:' . $bd->error();
	} else {
	
		if( $primerRegistro->num_rows == 0 ) {
			
			$usuario->setContrasena( $_POST['contrasena'] );
			
			$registrarUsuario = $bd->query("INSERT INTO primer_registro (id_usuario,registrado) VALUES (" . $usuario->getID() . ",1)");
			
			if( $registrarUsuario == false ) {
				echo 'Hubo un error con la base de datos:' . $bd->error();
			} // if( $registrarUsuario == false ) {
		} // if( $primerRegistro->num_rows == 0 ) {
		
	} // if( $primerRegistro == false ) {
	
	echo 'Se estableció la contraseña del usuario correctamente';
	
} // if(isset($usuario) && !empty($usuario)) ...

?>