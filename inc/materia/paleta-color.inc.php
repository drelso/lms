<?php
// Materia Paso 2:
// Paleta de color
// Paletton.com

if( isset($grupo) && isset($usuario) ) { 
?>

	<h2>Paso 1: Paletas de color</h2>

    <p>Elija una paleta de color (presione el botón de "Apply" en la esquina superior derecha cuando haya terminado)</p>
    
    <button id="col-btn-less">Dé clic aquí para seleccionar su paleta de color</button>
    <h4 id="mostrar-instrucciones">Instrucciones para elegir su paleta de colores</h4>
    
    <div class="ayuda-colores">
    	<span id="cerrar-instrucciones">&times;</span>
    	<h3>¿Cómo elegir mi paleta de colores?</h3>
        
        <img src="inc/materia/plantillas-paletton/instrucciones-colores.jpg" width="416" height="468"/>
        
        <div class="instrucciones">
        	<h5>1. Elija su color principal</h5>
            <h6>Seleccione y mueva el círculo marcado en el diagrama para definir el color principal de su paleta.</h6>
            
            <h5>2. Defina los colores secundarios</h5>
            <h6>Seleccione y mueva uno de los círculos laterales para definir la distancia entre el color principal y los colores secundarios.</h6>
            
            <h5>3. Tonalidades</h5>
            <h6>Puede definir las tonalidades de su paleta de color moviendo el conjunto de círculos marcado, ya sea en grupo o seleccionando cada tonalidad de manera individual (esto se logra manteniendo presionada la tecla "Shift" en su teclado. Se sugiere no hacer movimientos en esta sección, ya que las tonalidades están ajustadas para generar el mayor contraste y legibilidad.</h6>
            
            <h5>4. Deshacer / Rehacer y Empezar de nuevo</h5>
            <h6>Utilice los botones de Undo y Redo para regresar un paso o presione el botón de Reset para volver a empezar de cero.</h6>
            
            <h5>5. Terminar</h5>
            <h6>Una vez que esté convencido con la paleta de colores resultante presione el botón de "Apply" para terminar.</h6>
        </div> <!-- /instrucciones -->
    </div> <!-- /ayuda-colores -->
    
    <div id="resultado-color"></div>
    
    <input type="hidden" id="idcolores" name="idcolores" value=""/>
    
    <input type="hidden" id="primarioa" name="primarioa" value=""/>
    <input type="hidden" id="primariob" name="primariob" value=""/>
    <input type="hidden" id="primarioc" name="primarioc" value=""/>
    <input type="hidden" id="primariod" name="primariod" value=""/>
    <input type="hidden" id="primarioe" name="primarioe" value=""/>
    
    <input type="hidden" id="secundario1a" name="secundario1a" value=""/>
    <input type="hidden" id="secundario1b" name="secundario1b" value=""/>
    <input type="hidden" id="secundario1c" name="secundario1c" value=""/>
    <input type="hidden" id="secundario1d" name="secundario1d" value=""/>
    <input type="hidden" id="secundario1e" name="secundario1e" value=""/>
    
    <input type="hidden" id="secundario2a" name="secundario2a" value=""/>
    <input type="hidden" id="secundario2b" name="secundario2b" value=""/>
    <input type="hidden" id="secundario2c" name="secundario2c" value=""/>
    <input type="hidden" id="secundario2d" name="secundario2d" value=""/>
    <input type="hidden" id="secundario2e" name="secundario2e" value=""/>
	    

	<script type="text/javascript" src="inc/js/paletton.js"></script>
	<script type="text/javascript" src="inc/js/paleta-colores.js"></script>


<?php
} // if( isset($grupo) && isset($usuario) ) { 
?>