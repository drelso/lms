<!doctype html>
<html>
<head>

<?php
if(isset($_GET['estructura']))
{ $estructuraNum = $_GET['estructura']; }
else { $estructuraNum = '1'; }
?>

<meta charset="UTF-8">
<title>Estructura <?= $estructuraNum; ?></title>

<link rel="stylesheet" href="estilos/estilos-<?php echo $estructuraNum; ?>.php"/>

</head>

<body>
	<header>
    	<nav class="primario">
    		<h1>
            	<img src="img/cerebro.png" width="40" height="30"/>
                INTELIGENCIAS MÚLTIPLES
            </h1>
        	
        </nav> <!-- /primario -->
        
        <nav class="secundario">
        	<a href="#introduccion">INTRODUCCIÓN</a>
        	<a href="#logico-matematica">LÓGICO-MATEMÁTICA</a>
        	<a href="#linguistico-verbal">LINGÜÍSTICO-VERBAL</a>
        	<a href="#corporal-kinestesica">CORPORAL-KINESTÉSICA</a>
        	<a href="#espacial-visual">ESPACIAL-VISUAL</a>
        	<a href="#musical">MUSICAL</a>
        	<a href="#interpersonal">INTERPERSONAL</a>
        	<a href="#intrapersonal">INTRAPERSONAL</a>
        	<a href="#naturalista">NATURALISTA</a>
        </nav> <!-- /secundario -->
    </header>
    
    <div class="main">
        <section id="introduccion">
            <h1>INTRODUCCIÓN</h1>
            
            <p>
            Las inteligencias múltiples son una teoría propuesta por Howard Gardner (Armstrong, 2009) que permite conocer a los individuos desde una perspectiva más amplia. Gardner propuso una clasificación para evaluar 8 dimensiones de inteligencia, que representan las capacidades de un persona, y son: i) lingüística, ii) lógico-matemática, iii) espacial, iv) corporal-kinestésica, v) musical, vi) interpersonal, vii) intrapersonal y viii) naturalista.
            
<span>El conocer las dimensiones propias representa un área de oportunidad para trabajar en aquellas que nos hagan falta.</span>
            </p>
        </section> <!-- /parte-1 -->
        
        
        <section id="logico-matematica">
        	<div class="contenido">
                <h1><span>Inteligencia</span> LÓGICO-MATEMÁTICA</h1>
                
                <div class="definicion">
                    <h2>Definición</h2>
                    <p>
                    Capacidad para usar los números de manera efectiva y razonar adecuadamente.
                    </p>
                </div> <!-- /definicion -->
                
                <div class="actividades">
                    <h3>Actividades asociadas</h3>
                    <p>
                    Científicos, matemáticos, contadores, ingenieros, analistas de sistemas, etc. Resolución de problemas de lógica y matemáticas. Cálculos numéricos, estadísticos y presupuestos.
                    </p>
                </div> <!-- /actividades -->
            </div> <!-- /contenido -->
        </section> <!-- /logico-matematica -->
        
        
        <section id="linguistico-verbal">
        	<div class="contenido">
                <h1><span>Inteligencia</span> LINGÜÍSTICO-VERBAL</h1>
                
                <div class="definicion">
                    <h2>Definición</h2>
                    <p>
                    Capacidad para usar palabras de una manera efectiva, de forma oral o escrita
                    </p>
                </div> <!-- /definicion -->
                
                <div class="actividades">
                    <h3>Actividades asociadas</h3>
                    <p>
                    Escritores, poetas, periodistas, oradores, etc. Redactar historias, leer, rimas, trabalenguas, facilidad para aprender idiomas.
                    </p>
                </div> <!-- /actividades -->
            </div> <!-- /contenido -->
        </section> <!-- /linguistico-verbal -->
        
        
        <section id="corporal-kinestesica">
        	<div class="contenido">
                <h1><span>Inteligencia</span> CORPORAL-KINESTÉSICA</h1>
                
                <div class="definicion">
                    <h2>Definición</h2>
                    <p>
                    Capacidad para usar todo el cuerpo para expresar ideas y sentimientos y facilidad para usar las manos para producir y transformar cosas
                    </p>
                </div> <!-- /definicion -->
                
                <div class="actividades">
                    <h3>Actividades asociadas</h3>
                    <p>
                    Atletas, bailarines, cirujanos, artesanos, etc. Actividades deportivas, danza, expresión corporal, actividades manuales, manejo de instrumentos.
                    </p>
                </div> <!-- /actividades -->
            </div> <!-- /contenido -->
        </section> <!-- /corporal-kinestesica -->
        
        
        <section id="espacial-visual">
        	<div class="contenido">
                <h1><span>Inteligencia</span> ESPACIAL-VISUAL</h1>
                
                <div class="definicion">
                    <h2>Definición</h2>
                    <p>
                    Habilidad para percibir de manera exacta el mundo visual – espacial y de ejecutar transformaciones sobre esas percepciones 
                    </p>
                </div> <!-- /definicion -->
                
                <div class="actividades">
                    <h3>Actividades asociadas</h3>
                    <p>
                    Pilotos, marinos, escultores, pintores, arquitectos, etc. Gráficos, esquemas, cuadros, mapas conceptuales y mentales, planos y croquis.
                    </p>
                </div> <!-- /actividades -->
            </div> <!-- /contenido -->
        </section> <!-- /espacial-visual -->
        
        
        <section id="musical">
        	<div class="contenido">
                <h1><span>Inteligencia</span> MUSICAL</h1>
                
                <div class="definicion">
                    <h2>Definición</h2>
                    <p>
                    Capacidad de percibir, discriminar, transformar y expresar las formas musicales
                    </p>
                </div> <!-- /definicion -->
                
                <div class="actividades">
                    <h3>Actividades asociadas</h3>
                    <p>
                    Compositores, directores de orquesta, críticos musicales, músicos, luthiers, oyentes sensibles, etc. Atraídos por sonidos y melodías; siguen el compás con el pie, ritmo.
                    </p>
                </div> <!-- /actividades -->
            </div> <!-- /contenido -->
        </section> <!-- /musical -->
        
        <section id="interpersonal">
        	<div class="contenido">
                <h1><span>Inteligencia</span> INTERPERSONAL</h1>
                
                <div class="definicion">
                    <h2>Definición</h2>
                    <p>
                    Capacidad de percibir y establecer distinciones entre los estados de ánimo, las intenciones, las motivaciones y los sentimientos de otras personas
                    </p>
                </div> <!-- /definicion -->
                
                <div class="actividades">
                    <h3>Actividades asociadas</h3>
                    <p>
                    Actores, políticos, buenos vendedores, docentes exitosos, etc. Trabajo en grupo, convincentes en sus negociaciones, entienden a sus contrapartes.
                    </p>
                </div> <!-- /actividades -->
            </div> <!-- /contenido -->
        </section> <!-- /interpersonal -->
        
        <section id="intrapersonal">
        	<div class="contenido">
                <h1><span>Inteligencia</span> INTRAPERSONAL</h1>
                
                <div class="definicion">
                    <h2>Definición</h2>
                    <p>
                    El conocimiento de sí mismo y la habilidad para adaptar las propias maneras de actuar a partir de ese conocimiento
                    </p>
                </div> <!-- /definicion -->
                
                <div class="actividades">
                    <h3>Actividades asociadas</h3>
                    <p>
                    Teólogos, filósofos, psicólogos, etc. Personas reflexivas, de razonamiento acertado. Suelen ser consejeros de sus pares.
                    </p>
                </div> <!-- /actividades -->
            </div> <!-- /contenido -->
        </section> <!-- /intrapersonal -->
        
        <section id="naturalista">
        	<div class="contenido">
                <h1><span>Inteligencia</span> NATURALISTA</h1>
                
                <div class="definicion">
                    <h2>Definición</h2>
                    <p>
                    Capacidad de distinguir, clasificar y utilizar elementos del medio ambiente, objetos, animales o plantas.  Percepción y reflexión sobre nuestro entorno (urbano o rural).
                    </p>
                </div> <!-- /definicion -->
                
                <div class="actividades">
                    <h3>Actividades asociadas</h3>
                    <p>
                    Gente de campo, botánicos, cazadores, ecologistas, paisajistas, etc. Personas que aman las plantas y los animales, les gusta investigar características el mundo natural y del hecho por el hombre.
                    </p>
                </div> <!-- /actividades -->
            </div> <!-- /contenido -->
        </section> <!-- /naturalista -->
        
        
    </div> <!-- /main -->
</body>
</html>
