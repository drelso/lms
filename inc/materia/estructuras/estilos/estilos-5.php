<?php
session_start();
header("Content-type: text/css");

$esquemaDeColor = array(
		'PRINCIPAL' => '000000',
		'COMPLEMENTARIO' => 'FFFFFF',
		'COMPLEMENTARIO-HOVER' => 'E5E5E5',
		'MEDIO' => '999999',
		'SECUNDARIO' => '666666'
	);

if(isset($_SESSION['PRINCIPAL']))
{ $esquemaDeColor['PRINCIPAL'] = $_SESSION['PRINCIPAL']; }

if(isset($_SESSION['COMPLEMENTARIO']))
{ $esquemaDeColor['COMPLEMENTARIO'] = $_SESSION['COMPLEMENTARIO']; }

if(isset($_SESSION['COMPLEMENTARIO-HOVER']))
{ $esquemaDeColor['COMPLEMENTARIO-HOVER'] = $_SESSION['COMPLEMENTARIO-HOVER']; }

if(isset($_SESSION['MEDIO']))
{ $esquemaDeColor['MEDIO'] = $_SESSION['MEDIO']; }

if(isset($_SESSION['SECUNDARIO']))
{ $esquemaDeColor['SECUNDARIO'] = $_SESSION['SECUNDARIO']; }


// Funcion obtenida de http://bavotasan.com/2011/convert-hex-color-to-rgb-using-php/
function hex2rgb($hex) {
	$hex = str_replace("#", "", $hex);
	
	if(strlen($hex) == 3) {
		$r = hexdec(substr($hex,0,1).substr($hex,0,1));
		$g = hexdec(substr($hex,1,1).substr($hex,1,1));
		$b = hexdec(substr($hex,2,1).substr($hex,2,1));
	} else {
		$r = hexdec(substr($hex,0,2));
		$g = hexdec(substr($hex,2,2));
		$b = hexdec(substr($hex,4,2));
	} // if(strlen($hex) == 3) { ... } else { ... }
	$rgb = array($r, $g, $b);
	return implode(",", $rgb); // returns the rgb values separated by commas
	//return $rgb; // returns an array with the rgb values
} // function hex2rgb($hex) {
	
$esquemaDeColorRGB = array();

$esquemaDeColorRGB['PRINCIPAL'] = hex2rgb($esquemaDeColor['PRINCIPAL']);
$esquemaDeColorRGB['COMPLEMENTARIO'] = hex2rgb($esquemaDeColor['COMPLEMENTARIO']);
$esquemaDeColorRGB['COMPLEMENTARIO-HOVER'] = hex2rgb($esquemaDeColor['COMPLEMENTARIO-HOVER']);
$esquemaDeColorRGB['MEDIO'] = hex2rgb($esquemaDeColor['MEDIO']);
$esquemaDeColorRGB['SECUNDARIO'] = hex2rgb($esquemaDeColor['SECUNDARIO']);
?>

			/*  CSS RESET  */
				  /**/
				  /**/
				  /**/
html, body, div, span, applet, object, iframe,
h1, h2, h3, h4, h5, h6, p, blockquote, pre,
a, abbr, acronym, address, big, cite, code,
del, dfn, em, img, ins, kbd, q, s, samp,
small, strike, strong, sub, sup, tt, var,
b, u, i, center,
dl, dt, dd, ol, ul, li,
fieldset, form, label, legend,
table, caption, tbody, tfoot, thead, tr, th, td,
article, aside, canvas, details, embed, 
figure, figcaption, footer, header, hgroup, 
menu, nav, output, ruby, section, summary,
time, mark, audio, video {
	margin: 0;
	padding: 0;
	border: 0;
	font-size: 100%;
	font: inherit;
	vertical-align: baseline;
}

/* HTML5 display-role reset for older browsers */
article, aside, details, figcaption, figure, 
footer, header, hgroup, menu, nav, section {
	display: block;
}

body { line-height: 1; }

ol, ul { list-style: none; }

blockquote, q { quotes: none; }

blockquote:before, blockquote:after,
q:before, q:after {
	content: '';
	content: none;
}

table {
	border-collapse: collapse;
	border-spacing: 0;
}

::selection {
    background-color: rgb(<?= $esquemaDeColorRGB['PRINCIPAL']; ?>);
    background-color: rgba(<?= $esquemaDeColorRGB['PRINCIPAL']; ?>,0.7);
    color: #<?= $esquemaDeColor['COMPLEMENTARIO']; ?>;
}

::-moz-selection {
    background-color: rgb(<?= $esquemaDeColorRGB['PRINCIPAL']; ?>);
    background-color: rgba(<?= $esquemaDeColorRGB['PRINCIPAL']; ?>,0.7);
    color: #<?= $esquemaDeColor['COMPLEMENTARIO']; ?>;
}

html, body {
    padding: 0;
    margin: 0;
    font-family: Gotham, "Helvetica Neue", Helvetica, Arial, sans-serif;
}

body { background-color: #<?= $esquemaDeColor['COMPLEMENTARIO']; ?>; }

* { color: #<?= $esquemaDeColor['SECUNDARIO']; ?>; }

header {
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    box-sizing: border-box;
    position: fixed;
    top: 0;
    left: 0;
    width: 200px;
    height: 100%;
    /*padding: 15px 25px;*/
    background-color: #<?= $esquemaDeColor['COMPLEMENTARIO-HOVER']; ?>;
    z-index: 200;
}

nav,
.main {
    display: block;
    position: relative;
    width: 100%;
}

nav {
	-webkit-box-sizing: border-box;
	-moz-box-sizing: border-box;
	box-sizing: border-box;
	padding: 15px 25px;
    border-right: 1px solid #<?= $esquemaDeColor['SECUNDARIO']; ?>;
}

nav a {
    display: inline-block;
    width: 100%;
    color: #<?= $esquemaDeColor['PRINCIPAL']; ?>;
    text-decoration: none;
    font-weight: bold;
    line-height: 130%;
    text-align: left;
}

nav.primario h1 {
	position: relative;
	color: #<?= $esquemaDeColor['PRINCIPAL']; ?>;
    line-height: 155%;
    font-size: 18px;
    padding: 3px 0;
}

nav.primario h1 img {
	position: absolute;
    bottom: 0;
    right: 0;
    -webkit-filter: invert(100%);
    filter: invert(100%);
}

nav.secundario a {
	position: relative;
	font-size: 12px;
    padding-bottom: 13px;
    margin-bottom: 13px;
}

nav.secundario a:after {
	content: '';
    display: block;
    position: absolute;
    bottom: 0;
    left: 0;
    width: 25px;
    height: 1px; 
    border-bottom: 1px solid #<?= $esquemaDeColor['SECUNDARIO']; ?>;
}

nav a:hover,
nav a:focus,
nav a:active {
	color: #<?= $esquemaDeColor['MEDIO']; ?>;
}

.main {
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    box-sizing: border-box;
    margin: -18px 0;
    padding: 0 0 0 200px;
}

section {
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    box-sizing: border-box;
    display: block;
    vertical-align: top;
    position: relative;
    width: 70%;
    min-height: 200px;
    margin: 20px auto;
    padding: 10px 10px 10px 110px;
    border: 1px solid #<?= $esquemaDeColor['PRINCIPAL']; ?>;
    /*background-image: url('../img/black-linen.png');*/
    overflow: hidden;
}

section:after {
	content: '';
    display: block;
    position: absolute;
    top: 5%;
    left: 10px;
    width: 80px;
    height: 90%;
    background-position: center;
    background-repeat: repeat;
    -webkit-background-size: 60px 19px;
    -moz-background-size: 60px 19px;
    /*background-size: 60px 19px;
    background-image: url('../img/texturas/subtle-zebra-3d.png');*/
    background-size: 30px 30px;
    background-image: url('../img/texturas/triangular.png');
    background-color: rgb(<?= $esquemaDeColorRGB['SECUNDARIO']; ?>);
    background-color: rgba(<?= $esquemaDeColorRGB['SECUNDARIO']; ?>,0.9);
    z-index: -1;
}

section h1 {
	-webkit-box-sizing: border-box;
	-moz-box-sizing: border-box;
	box-sizing: border-box;
    display: block;
	position: relative;
    width: 100%;
	font-size: 18px;
	font-weight: bold;
    line-height: 20px;
    clear: both;
    z-index: 10;
}

section h1 span {
	display: inline-block;
	width: 100%;
}

#introduccion {
	padding: 0;
    border: none;
    margin-bottom: 50px;
}

#introduccion:after { display: none; }

#introduccion h1 { margin: 30px 0 15px; }

#introduccion p {
	-webkit-box-sizing: border-box;
	-moz-box-sizing: border-box;
	box-sizing: border-box;
    position: relative;
    width: 100%;
    text-align: justify;
    line-height: 140%;
}

#introduccion span {
	-webkit-box-sizing: border-box;
	-moz-box-sizing: border-box;
	box-sizing: border-box;
	display: block;
    width: 100%;
    padding: 15px 10px;
    margin-top: 15px;
    color: #<?= $esquemaDeColor['COMPLEMENTARIO-HOVER']; ?>;
    background-color: #<?= $esquemaDeColor['PRINCIPAL']; ?>;
	font-size: 18px;
    font-weight: bold;
    text-align: left;
}

.definicion,
.actividades {
	-webkit-box-sizing: border-box;
	-moz-box-sizing: border-box;
	box-sizing: border-box;
	display: inline-block;
    position: relative;
    width: 50%;
    padding: 30px 50px 0 0;
    text-align: justify;
    font-size: 14px;
    line-height: 140%;
    vertical-align: top;
}

section h2,
section h3 {
	font-weight: bold;
    font-size: 16px;
    margin-bottom: 10px;
}

.actividades {
	width: 49%;
    margin-left: -4px;
}