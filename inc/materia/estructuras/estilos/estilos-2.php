<?php
session_start();
header("Content-type: text/css");

$esquemaDeColor = array(
		'PRINCIPAL' => '000000',
		'COMPLEMENTARIO' => 'FFFFFF',
		'COMPLEMENTARIO-HOVER' => 'E5E5E5',
		'MEDIO' => '999999',
		'SECUNDARIO' => '666666'
	);

if(isset($_SESSION['PRINCIPAL']))
{ $esquemaDeColor['PRINCIPAL'] = $_SESSION['PRINCIPAL']; }

if(isset($_SESSION['COMPLEMENTARIO']))
{ $esquemaDeColor['COMPLEMENTARIO'] = $_SESSION['COMPLEMENTARIO']; }

if(isset($_SESSION['COMPLEMENTARIO-HOVER']))
{ $esquemaDeColor['COMPLEMENTARIO-HOVER'] = $_SESSION['COMPLEMENTARIO-HOVER']; }

if(isset($_SESSION['MEDIO']))
{ $esquemaDeColor['MEDIO'] = $_SESSION['MEDIO']; }

if(isset($_SESSION['SECUNDARIO']))
{ $esquemaDeColor['SECUNDARIO'] = $_SESSION['SECUNDARIO']; }


// Funcion obtenida de http://bavotasan.com/2011/convert-hex-color-to-rgb-using-php/
function hex2rgb($hex) {
	$hex = str_replace("#", "", $hex);
	
	if(strlen($hex) == 3) {
		$r = hexdec(substr($hex,0,1).substr($hex,0,1));
		$g = hexdec(substr($hex,1,1).substr($hex,1,1));
		$b = hexdec(substr($hex,2,1).substr($hex,2,1));
	} else {
		$r = hexdec(substr($hex,0,2));
		$g = hexdec(substr($hex,2,2));
		$b = hexdec(substr($hex,4,2));
	} // if(strlen($hex) == 3) { ... } else { ... }
	$rgb = array($r, $g, $b);
	return implode(",", $rgb); // returns the rgb values separated by commas
	//return $rgb; // returns an array with the rgb values
} // function hex2rgb($hex) {
	
$esquemaDeColorRGB = array();

$esquemaDeColorRGB['PRINCIPAL'] = hex2rgb($esquemaDeColor['PRINCIPAL']);
$esquemaDeColorRGB['COMPLEMENTARIO'] = hex2rgb($esquemaDeColor['COMPLEMENTARIO']);
$esquemaDeColorRGB['COMPLEMENTARIO-HOVER'] = hex2rgb($esquemaDeColor['COMPLEMENTARIO-HOVER']);
$esquemaDeColorRGB['MEDIO'] = hex2rgb($esquemaDeColor['MEDIO']);
$esquemaDeColorRGB['SECUNDARIO'] = hex2rgb($esquemaDeColor['SECUNDARIO']);
?>

			/*  CSS RESET  */
				  /**/
				  /**/
				  /**/
html, body, div, span, applet, object, iframe,
h1, h2, h3, h4, h5, h6, p, blockquote, pre,
a, abbr, acronym, address, big, cite, code,
del, dfn, em, img, ins, kbd, q, s, samp,
small, strike, strong, sub, sup, tt, var,
b, u, i, center,
dl, dt, dd, ol, ul, li,
fieldset, form, label, legend,
table, caption, tbody, tfoot, thead, tr, th, td,
article, aside, canvas, details, embed, 
figure, figcaption, footer, header, hgroup, 
menu, nav, output, ruby, section, summary,
time, mark, audio, video {
	margin: 0;
	padding: 0;
	border: 0;
	font-size: 100%;
	font: inherit;
	vertical-align: baseline;
}

/* HTML5 display-role reset for older browsers */
article, aside, details, figcaption, figure, 
footer, header, hgroup, menu, nav, section {
	display: block;
}

body { line-height: 1; }

ol, ul { list-style: none; }

blockquote, q { quotes: none; }

blockquote:before, blockquote:after,
q:before, q:after {
	content: '';
	content: none;
}

table {
	border-collapse: collapse;
	border-spacing: 0;
}

::selection {
    background-color: rgb(<?= $esquemaDeColorRGB['PRINCIPAL']; ?>);
    background-color: rgba(<?= $esquemaDeColorRGB['PRINCIPAL']; ?>,0.7);
    color: #<?= $esquemaDeColor['COMPLEMENTARIO']; ?>;
}

::-moz-selection {
    background-color: rgb(<?= $esquemaDeColorRGB['PRINCIPAL']; ?>);
    background-color: rgba(<?= $esquemaDeColorRGB['PRINCIPAL']; ?>,0.7);
    color: #<?= $esquemaDeColor['COMPLEMENTARIO']; ?>;
}

html, body {
    padding: 0;
    margin: 0;
    font-family: Gotham, "Helvetica Neue", Helvetica, Arial, sans-serif;
}

body { background-color: #<?= $esquemaDeColor['COMPLEMENTARIO']; ?>; }

* { color: #<?= $esquemaDeColor['SECUNDARIO']; ?>; }

header {
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    box-sizing: border-box;
    position: fixed;
    top: 0;
    left: 0;
    width: 200px;
    height: 100%;
    /*padding: 15px 25px;*/
    background-color: #<?= $esquemaDeColor['PRINCIPAL']; ?>;
    z-index: 200;
}

nav,
.main {
    display: block;
    position: relative;
    width: 100%;
}

nav {
	-webkit-box-sizing: border-box;
	-moz-box-sizing: border-box;
	box-sizing: border-box;
	padding: 15px 25px;
}

nav a {
    display: inline-block;
    width: 100%;
    color: #<?= $esquemaDeColor['COMPLEMENTARIO']; ?>;
    text-decoration: none;
    font-weight: bold;
    line-height: 130%;
    text-align: left;
}

nav.primario { background: #<?= $esquemaDeColor['SECUNDARIO']; ?>; }

nav.primario h1 {
	position: relative;
	color: #<?= $esquemaDeColor['COMPLEMENTARIO']; ?>;
    line-height: 155%;
    font-size: 18px;
    padding: 3px 0;
}

nav.primario h1 img {
	position: absolute;
    bottom: 0;
    right: 0;
}

nav.secundario a {
	font-size: 12px;
    padding-bottom: 8px;
    border-bottom: 1px dotted #<?= $esquemaDeColor['SECUNDARIO']; ?>;
    margin-bottom: 8px;
}

nav a:hover,
nav a:focus,
nav a:active {
	color: #<?= $esquemaDeColor['COMPLEMENTARIO-HOVER']; ?>;
}

.main {
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    box-sizing: border-box;
    margin: -18px 0;
    padding: 0 0 0 200px;
}

section {
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    box-sizing: border-box;
    display: inline-block;
    vertical-align: top;
    position: relative;
    width: 100%;
    min-height: 280px;
    padding: 30px 30px 50px;
	background-color: rgba(<?= $esquemaDeColorRGB['SECUNDARIO']; ?>,0.6);
    /*background-image: url('../img/black-linen.png');*/
    overflow: hidden;
}

section:nth-child(2n) {
	background-color: rgba(<?= $esquemaDeColorRGB['MEDIO']; ?>,0.6);
}

#introduccion {
	background-image: url('../img/black-linen.png');
	background-color: rgba(<?= $esquemaDeColorRGB['PRINCIPAL']; ?>,0.7);
}

#introduccion p {
	-webkit-box-sizing: border-box;
	-moz-box-sizing: border-box;
	box-sizing: border-box;
    position: relative;
    padding-right: 55%;
}

#introduccion span {
	-webkit-box-sizing: border-box;
	-moz-box-sizing: border-box;
	box-sizing: border-box;
	display: block;
    position: absolute;
    top: 0;
    left: 50%;
    width: 49%;
    padding-right: 50px;
    color: #<?= $esquemaDeColor['COMPLEMENTARIO-HOVER']; ?>;
	font-size: 18px;
    font-weight: bold;
    text-align: left;
}

section:after {
	content: '';
    display: block;
    position: absolute;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
    background-position: center;
    background-repeat: no-repeat;
    -webkit-background-size: cover;
    -moz-background-size: cover;
    background-size: cover;
    z-index: -1;
}

section#logico-matematica:after {
    background-image: url('../img/matematica2.jpg');
}
    
section#linguistico-verbal:after {
    background-image: url('../img/lenguaje2.png');
}
    
section#corporal-kinestesica:after {
    background-image: url('../img/cinestetica.png');
}
    
section#espacial-visual:after {
    background-image: url('../img/espacial2.jpg');
}
    
section#musical:after {
    background-image: url('../img/musical3.jpg');
}
    
section#interpersonal:after {
    background-image: url('../img/interpersonal2.jpg');
}
    
section#intrapersonal:after {
    background-image: url('../img/intrapersonal.jpg');
}
    
section#naturalista:after {
    background-image: url('../img/naturalista3.jpg');
}

section * { color: #<?= $esquemaDeColor['COMPLEMENTARIO']; ?>; }

section h1 {
	-webkit-box-sizing: border-box;
	-moz-box-sizing: border-box;
	box-sizing: border-box;
    display: inline-block;
	position: relative;
    width: auto;
    margin: 40px 0 20px 50%;
	font-size: 18px;
	font-weight: bold;
    line-height: 20px;
    color: #<?= $esquemaDeColor['COMPLEMENTARIO']; ?>;
    clear: both;
    z-index: 10;
}

section h1:before,
section h1:after {
	content: '';
    position: absolute;
    top: 9px;
    width: 10000px;
    height: 1px;
    border-top: 1px dashed #<?= $esquemaDeColor['COMPLEMENTARIO']; ?>;
    z-index: 1;
}

section h1:before { right: 101%; }
section h1:after { left: 101%; }

.definicion,
.actividades {
	-webkit-box-sizing: border-box;
	-moz-box-sizing: border-box;
	box-sizing: border-box;
	display: inline-block;
    position: relative;
    width: 50%;
    padding: 100px 50px 100px 0;
    vertical-align: middle;
}

section h2,
section h3 {
	font-weight: bold;
    font-size: 16px;
    margin-bottom: 10px;
}

.actividades {
	width: 49%;
    margin-left: -4px;
}