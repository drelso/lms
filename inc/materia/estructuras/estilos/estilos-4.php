<?php
session_start();
header("Content-type: text/css");

$esquemaDeColor = array(
		'PRINCIPAL' => '000000',
		'COMPLEMENTARIO' => 'FFFFFF',
		'COMPLEMENTARIO-HOVER' => 'E5E5E5',
		'MEDIO' => '999999',
		'SECUNDARIO' => '666666'
	);

if(isset($_SESSION['PRINCIPAL']))
{ $esquemaDeColor['PRINCIPAL'] = $_SESSION['PRINCIPAL']; }

if(isset($_SESSION['COMPLEMENTARIO']))
{ $esquemaDeColor['COMPLEMENTARIO'] = $_SESSION['COMPLEMENTARIO']; }

if(isset($_SESSION['COMPLEMENTARIO-HOVER']))
{ $esquemaDeColor['COMPLEMENTARIO-HOVER'] = $_SESSION['COMPLEMENTARIO-HOVER']; }

if(isset($_SESSION['MEDIO']))
{ $esquemaDeColor['MEDIO'] = $_SESSION['MEDIO']; }

if(isset($_SESSION['SECUNDARIO']))
{ $esquemaDeColor['SECUNDARIO'] = $_SESSION['SECUNDARIO']; }


// Funcion obtenida de http://bavotasan.com/2011/convert-hex-color-to-rgb-using-php/
function hex2rgb($hex) {
	$hex = str_replace("#", "", $hex);
	
	if(strlen($hex) == 3) {
		$r = hexdec(substr($hex,0,1).substr($hex,0,1));
		$g = hexdec(substr($hex,1,1).substr($hex,1,1));
		$b = hexdec(substr($hex,2,1).substr($hex,2,1));
	} else {
		$r = hexdec(substr($hex,0,2));
		$g = hexdec(substr($hex,2,2));
		$b = hexdec(substr($hex,4,2));
	} // if(strlen($hex) == 3) { ... } else { ... }
	$rgb = array($r, $g, $b);
	return implode(",", $rgb); // returns the rgb values separated by commas
	//return $rgb; // returns an array with the rgb values
} // function hex2rgb($hex) {
	
$esquemaDeColorRGB = array();

$esquemaDeColorRGB['PRINCIPAL'] = hex2rgb($esquemaDeColor['PRINCIPAL']);
$esquemaDeColorRGB['COMPLEMENTARIO'] = hex2rgb($esquemaDeColor['COMPLEMENTARIO']);
$esquemaDeColorRGB['COMPLEMENTARIO-HOVER'] = hex2rgb($esquemaDeColor['COMPLEMENTARIO-HOVER']);
$esquemaDeColorRGB['MEDIO'] = hex2rgb($esquemaDeColor['MEDIO']);
$esquemaDeColorRGB['SECUNDARIO'] = hex2rgb($esquemaDeColor['SECUNDARIO']);
?>

			/*  CSS RESET  */
				  /**/
				  /**/
				  /**/
html, body, div, span, applet, object, iframe,
h1, h2, h3, h4, h5, h6, p, blockquote, pre,
a, abbr, acronym, address, big, cite, code,
del, dfn, em, img, ins, kbd, q, s, samp,
small, strike, strong, sub, sup, tt, var,
b, u, i, center,
dl, dt, dd, ol, ul, li,
fieldset, form, label, legend,
table, caption, tbody, tfoot, thead, tr, th, td,
article, aside, canvas, details, embed, 
figure, figcaption, footer, header, hgroup, 
menu, nav, output, ruby, section, summary,
time, mark, audio, video {
	margin: 0;
	padding: 0;
	border: 0;
	font-size: 100%;
	font: inherit;
	vertical-align: baseline;
}

/* HTML5 display-role reset for older browsers */
article, aside, details, figcaption, figure, 
footer, header, hgroup, menu, nav, section {
	display: block;
}

body { line-height: 1; }

ol, ul { list-style: none; }

blockquote, q { quotes: none; }

blockquote:before, blockquote:after,
q:before, q:after {
	content: '';
	content: none;
}

table {
	border-collapse: collapse;
	border-spacing: 0;
}

::selection {
    background-color: rgb(<?= $esquemaDeColorRGB['PRINCIPAL']; ?>);
    background-color: rgba(<?= $esquemaDeColorRGB['PRINCIPAL']; ?>,0.7);
    color: #<?= $esquemaDeColor['COMPLEMENTARIO']; ?>;
}

::-moz-selection {
    background-color: rgb(<?= $esquemaDeColorRGB['PRINCIPAL']; ?>);
    background-color: rgba(<?= $esquemaDeColorRGB['PRINCIPAL']; ?>,0.7);
    color: #<?= $esquemaDeColor['COMPLEMENTARIO']; ?>;
}

html, body {
    padding: 0;
    margin: 0;
    font-family: Gotham, "Helvetica Neue", Helvetica, Arial, sans-serif;
}

body { background-color: #<?= $esquemaDeColor['PRINCIPAL']; ?>; }

* { color: #<?= $esquemaDeColor['PRINCIPAL']; ?>; }

header {
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    box-sizing: border-box;
    position: fixed;
    bottom: 0;
    left: 0;
    width: 100%;
    height: 75px;
    padding: 0 30px;
    background-color: #<?= $esquemaDeColor['COMPLEMENTARIO']; ?>;
    border-top: 2px solid #<?= $esquemaDeColor['MEDIO']; ?>;
    z-index: 200;
    -webkit-box-shadow: 1px 2px 14px 0px rgba(50, 50, 50, 0.54);
    -moz-box-shadow:    1px 2px 14px 0px rgba(50, 50, 50, 0.54);
    box-shadow:         1px 2px 14px 0px rgba(50, 50, 50, 0.54);
}

nav,
.main {
    display: block;
    position: relative;
    width: 100%;
    text-align: justify;
}

nav { height: 30px; }

nav.secundario:after,
.main:after {
    content: "";
    display: inline-block;
    width: 100%;
}

nav a {
    display: inline-block;
    color: #<?= $esquemaDeColor['SECUNDARIO']; ?>;
    text-decoration: none;
    font-weight: bold;
    font-size: 13px;
    line-height: 30px;
}

nav a:hover,
nav a:focus,
nav a:active {
	color: #<?= $esquemaDeColor['MEDIO']; ?>;
}

nav.primario {
	-webkit-box-sizing: border-box;
	-moz-box-sizing: border-box;
	box-sizing: border-box;
	position: relative;
    width: 100%;
    height: auto;
    text-align: center;
}

nav.primario h1 {
	display: inline-block;
    position: relative;
    width: auto;
    margin: 0 auto;
    padding: 15px 40px 8px 0;
	color: #<?= $esquemaDeColor['MEDIO']; ?>;
    text-align: right;
}

nav.primario img {
	position: absolute;
    top: 10px;
    right: 0;
    width: 32px;
    height: 24px;
    -webkit-filter: invert(100%);
    filter: invert(100%);
}

nav.primario a {
	padding: 8px;
	background: #<?= $esquemaDeColor['PRINCIPAL']; ?>;
}

nav.secundario a { font-size: 11px; }

.main {
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    box-sizing: border-box;
    margin-bottom: 60px;
    padding: 30px 30px 0;
}

section {
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    box-sizing: border-box;
    display: inline-block;
    position: relative;
    vertical-align: top;
    width: 100%;
    margin-bottom: 50px;
    padding: 30px;
    background-color: rgb(<?= $esquemaDeColorRGB['COMPLEMENTARIO-HOVER']; ?>);
    background-color: rgba(<?= $esquemaDeColorRGB['COMPLEMENTARIO-HOVER']; ?>,0.9);
    -webkit-border-radius: 8px;
    -moz-border-radius: 8px;
    border-radius: 8px;
}

section:after {
	content: '';
	display: block;
    position: absolute;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
    background-position: center;
    background-repeat: no-repeat;
    -webkit-background-size: cover;
    -moz-background-size: cover;
    background-size: cover;
    -webkit-border-radius: 8px;
    -moz-border-radius: 8px;
    border-radius: 8px;
    z-index: -1;
}

section#logico-matematica:after {
    background-image: url('../img/matematica2.jpg');
}
    
section#linguistico-verbal:after {
    background-image: url('../img/lenguaje2.png');
}
    
section#corporal-kinestesica:after {
    background-image: url('../img/cinestetica.png');
}
    
section#espacial-visual:after {
    background-image: url('../img/espacial2.jpg');
}
    
section#musical:after {
    background-image: url('../img/musical3.jpg');
}
    
section#interpersonal:after {
    background-image: url('../img/interpersonal2.jpg');
}
    
section#intrapersonal:after {
    background-image: url('../img/intrapersonal.jpg');
}
    
section#naturalista:after {
    background-image: url('../img/naturalista3.jpg');
}

#introduccion {
	background: none;
    border: 1px dashed #<?= $esquemaDeColor['SECUNDARIO']; ?>;
}

#introduccion * { color: #<?= $esquemaDeColor['COMPLEMENTARIO']; ?>; }

#introduccion h1,
.contenido h1 {
	font-weight: bold;
    margin-bottom: 20px;
}

.contenido h2,
.contenido h3 {
	padding: 5px 0;
    border-top: 1px dashed #<?= $esquemaDeColor['MEDIO']; ?>;
    border-bottom: 1px dashed #<?= $esquemaDeColor['MEDIO']; ?>;
    margin-bottom: 10px;
}

.definicion p { margin-bottom: 25px; }

ul.submenu {
	-webkit-box-sizing: border-box;
	-moz-box-sizing: border-box;
	box-sizing: border-box;
	position: absolute;
    top: 60%;
    left: 0;
    width: 100%;
    height: 40%;
    border-top: 1px dashed #<?= $esquemaDeColor['COMPLEMENTARIO-HOVER']; ?>;
	padding: 15px 20px 15px 0;
    overflow: scroll;
}

ul.submenu li {
	position: relative;
	text-align: left;
    padding-bottom: 15px;
    margin-bottom: 15px;
    cursor: pointer;
}

ul.submenu li:after {
	content: '';
    display: block;
    position: absolute;
    bottom: 0;
    left: 0;
    width: 30px;
    height: 1px;
    border-top: 1px dashed #<?= $esquemaDeColor['COMPLEMENTARIO-HOVER']; ?>;
}

ul.submenu li:hover,
ul.submenu li:focus,
ul.submenu li:active {
	color: #<?= $esquemaDeColor['COMPLEMENTARIO-HOVER']; ?>;
}