<?php
session_start();
header("Content-type: text/css");

$esquemaDeColor = array(
		'PRINCIPAL' => '000000',
		'COMPLEMENTARIO' => 'FFFFFF',
		'COMPLEMENTARIO-HOVER' => 'E5E5E5',
		'MEDIO' => '999999',
		'SECUNDARIO' => '666666'
	);

if(isset($_SESSION['PRINCIPAL']))
{ $esquemaDeColor['PRINCIPAL'] = $_SESSION['PRINCIPAL']; }

if(isset($_SESSION['COMPLEMENTARIO']))
{ $esquemaDeColor['COMPLEMENTARIO'] = $_SESSION['COMPLEMENTARIO']; }

if(isset($_SESSION['COMPLEMENTARIO-HOVER']))
{ $esquemaDeColor['COMPLEMENTARIO-HOVER'] = $_SESSION['COMPLEMENTARIO-HOVER']; }

if(isset($_SESSION['MEDIO']))
{ $esquemaDeColor['MEDIO'] = $_SESSION['MEDIO']; }

if(isset($_SESSION['SECUNDARIO']))
{ $esquemaDeColor['SECUNDARIO'] = $_SESSION['SECUNDARIO']; }


// Funcion obtenida de http://bavotasan.com/2011/convert-hex-color-to-rgb-using-php/
function hex2rgb($hex) {
	$hex = str_replace("#", "", $hex);
	
	if(strlen($hex) == 3) {
		$r = hexdec(substr($hex,0,1).substr($hex,0,1));
		$g = hexdec(substr($hex,1,1).substr($hex,1,1));
		$b = hexdec(substr($hex,2,1).substr($hex,2,1));
	} else {
		$r = hexdec(substr($hex,0,2));
		$g = hexdec(substr($hex,2,2));
		$b = hexdec(substr($hex,4,2));
	} // if(strlen($hex) == 3) { ... } else { ... }
	$rgb = array($r, $g, $b);
	return implode(",", $rgb); // returns the rgb values separated by commas
	//return $rgb; // returns an array with the rgb values
} // function hex2rgb($hex) {
	
$esquemaDeColorRGB = array();

$esquemaDeColorRGB['PRINCIPAL'] = hex2rgb($esquemaDeColor['PRINCIPAL']);
$esquemaDeColorRGB['COMPLEMENTARIO'] = hex2rgb($esquemaDeColor['COMPLEMENTARIO']);
$esquemaDeColorRGB['COMPLEMENTARIO-HOVER'] = hex2rgb($esquemaDeColor['COMPLEMENTARIO-HOVER']);
$esquemaDeColorRGB['MEDIO'] = hex2rgb($esquemaDeColor['MEDIO']);
$esquemaDeColorRGB['SECUNDARIO'] = hex2rgb($esquemaDeColor['SECUNDARIO']);
?>


			/*  CSS RESET  */
				  /**/
				  /**/
				  /**/
html, body, div, span, applet, object, iframe,
h1, h2, h3, h4, h5, h6, p, blockquote, pre,
a, abbr, acronym, address, big, cite, code,
del, dfn, em, img, ins, kbd, q, s, samp,
small, strike, strong, sub, sup, tt, var,
b, u, i, center,
dl, dt, dd, ol, ul, li,
fieldset, form, label, legend,
table, caption, tbody, tfoot, thead, tr, th, td,
article, aside, canvas, details, embed, 
figure, figcaption, footer, header, hgroup, 
menu, nav, output, ruby, section, summary,
time, mark, audio, video {
	margin: 0;
	padding: 0;
	border: 0;
	font-size: 100%;
	font: inherit;
	vertical-align: baseline;
}

/* HTML5 display-role reset for older browsers */
article, aside, details, figcaption, figure, 
footer, header, hgroup, menu, nav, section {
	display: block;
}

body { line-height: 1; }

ol, ul { list-style: none; }

blockquote, q { quotes: none; }

blockquote:before, blockquote:after,
q:before, q:after {
	content: '';
	content: none;
}

table {
	border-collapse: collapse;
	border-spacing: 0;
}

::selection {
    background-color: rgb(<?= $esquemaDeColorRGB['PRINCIPAL']; ?>);
    background-color: rgba(<?= $esquemaDeColorRGB['PRINCIPAL']; ?>,0.7);
    color: #<?= $esquemaDeColor['COMPLEMENTARIO']; ?>;
}

::-moz-selection {
    background-color: rgb(<?= $esquemaDeColorRGB['PRINCIPAL']; ?>);
    background-color: rgba(<?= $esquemaDeColorRGB['PRINCIPAL']; ?>,0.7);
    color: #<?= $esquemaDeColor['COMPLEMENTARIO']; ?>;
}

html, body {
    padding: 0;
    margin: 0;
    font-family: Gotham, "Helvetica Neue", Helvetica, Arial, sans-serif;
}

/* Hace todo el contenido más chico */
body {
    -moz-transform: scale(0.6, 0.6); /* Moz-browsers */
    zoom: 0.6; /* Other non-webkit browsers */
    zoom: 60%; /* Webkit browsers */
}


body { background-color: #<?= $esquemaDeColor['COMPLEMENTARIO']; ?>; }

* { color: #<?= $esquemaDeColor['SECUNDARIO']; ?>; }

header {
    position: fixed;
    top: 0;
    left: 0;
    width: 100%;
    height: auto;
    z-index: 200;
}

nav,
.main {
    display: block;
    position: relative;
    width: 100%;
    text-align: justify;
}

nav {
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    box-sizing: border-box;
    padding: 10px 25px;
}

nav:after,
.main:after {
    content: "";
    display: inline-block;
    width: 100%;
}

nav a {
    display: inline-block;
    color: #<?= $esquemaDeColor['COMPLEMENTARIO']; ?>;
    text-decoration: none;
    font-weight: bold;
    line-height: 15px;
}

nav a:hover,
nav a:focus,
nav a:active {
	color: #<?= $esquemaDeColor['COMPLEMENTARIO-HOVER']; ?>;
}

nav.primario {
	height: 56px;
    background-color: #<?= $esquemaDeColor['PRINCIPAL']; ?>;
}

nav.primario h1 {
	line-height: 36px;
	vertical-align: top;
	color: #<?= $esquemaDeColor['COMPLEMENTARIO-HOVER']; ?>;
    font-size: 18px;
    letter-spacing: 1px;
}

nav.primario h1 img { margin-right: 8px; }

nav.primario img { display: inline-block; }

nav.secundario {
	height: 38px;
	background-color: #<?= $esquemaDeColor['SECUNDARIO']; ?>;
}

nav.secundario a { font-size: 11px; }

.main {
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    box-sizing: border-box;
    margin-top: 114px;
    padding: 30px 30px 0;
    text-align: justify;
}

section {
    display: inline-block;
    position: relative;
    width: 45%;
    /*vertical-align: top;
    padding-top: 65%;*/
    margin-bottom: 95px;
    text-align: left;
    vertical-align: top;
}

#introduccion {
	-webkit-box-sizing: border-box;
	-moz-box-sizing: border-box;
	box-sizing: border-box;
    width: 100%;
	background: #<?= $esquemaDeColor['COMPLEMENTARIO-HOVER']; ?>;
    padding: 60px;
}

#introduccion p {
    color: #<?= $esquemaDeColor['PRINCIPAL']; ?>;
}

#introduccion span {
	display: inline-block;
    width: 100%;
    margin-top: 20px;
    color: #<?= $esquemaDeColor['PRINCIPAL']; ?>;
}

.contenido {
	-webkit-box-sizing: border-box;
	-moz-box-sizing: border-box;
	box-sizing: border-box;
    position: relative;
    width: 100%;
	/*position: absolute;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;*/
    padding-right: 52%;
}

section h1 {
	width: 100%;
    color: #<?= $esquemaDeColor['PRINCIPAL']; ?>;
	font-weight: bold;
    padding-bottom: 20px;
    border-bottom: 1px solid #<?= $esquemaDeColor['PRINCIPAL']; ?>;
    margin-bottom: 20px;
}

section h1 span {
	display: inline-block;
    width: 100%;
    font-weight: normal;
    font-size: 16px;
}

section h2,
section h3 {
	display: inline-block;
	width: auto;
	padding: 8px;
	color: #<?= $esquemaDeColor['COMPLEMENTARIO']; ?>;
	background: #<?= $esquemaDeColor['SECUNDARIO']; ?>;
    margin-bottom: 10px;
    font-weight: bold;
}

section p {
	margin-bottom: 15px;
    line-height: 130%;
}

section:after {
	content: '';
    position: absolute;
    top: 0;
    right: 0;
    width: 48%;
    height: 100%;
    background-position: center;
    background-repeat: no-repeat;
    -webkit-background-size: cover;
    -moz-background-size: cover;
    background-size: cover;
}
    
section#logico-matematica:after {
    background-image: url('../img/matematica2.jpg');
}
    
section#linguistico-verbal:after {
    background-image: url('../img/lenguaje2.png');
}
    
section#corporal-kinestesica:after {
    background-image: url('../img/cinestetica.png');
}
    
section#espacial-visual:after {
    background-image: url('../img/espacial2.jpg');
}
    
section#musical:after {
    background-image: url('../img/musical3.jpg');
}
    
section#interpersonal:after {
    background-image: url('../img/interpersonal2.jpg');
}
    
section#intrapersonal:after {
    background-image: url('../img/intrapersonal.jpg');
}
    
section#naturalista:after {
    background-image: url('../img/naturalista3.jpg');
}