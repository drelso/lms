<?php
session_start();
header("Content-type: text/css");

$esquemaDeColor = array(
		'PRINCIPAL' => '000000',
		'COMPLEMENTARIO' => 'FFFFFF',
		'COMPLEMENTARIO-HOVER' => 'E5E5E5',
		'MEDIO' => '999999',
		'SECUNDARIO' => '666666'
	);

if(isset($_SESSION['PRINCIPAL']))
{ $esquemaDeColor['PRINCIPAL'] = $_SESSION['PRINCIPAL']; }

if(isset($_SESSION['COMPLEMENTARIO']))
{ $esquemaDeColor['COMPLEMENTARIO'] = $_SESSION['COMPLEMENTARIO']; }

if(isset($_SESSION['COMPLEMENTARIO-HOVER']))
{ $esquemaDeColor['COMPLEMENTARIO-HOVER'] = $_SESSION['COMPLEMENTARIO-HOVER']; }

if(isset($_SESSION['MEDIO']))
{ $esquemaDeColor['MEDIO'] = $_SESSION['MEDIO']; }

if(isset($_SESSION['SECUNDARIO']))
{ $esquemaDeColor['SECUNDARIO'] = $_SESSION['SECUNDARIO']; }


// Funcion obtenida de http://bavotasan.com/2011/convert-hex-color-to-rgb-using-php/
function hex2rgb($hex) {
	$hex = str_replace("#", "", $hex);
	
	if(strlen($hex) == 3) {
		$r = hexdec(substr($hex,0,1).substr($hex,0,1));
		$g = hexdec(substr($hex,1,1).substr($hex,1,1));
		$b = hexdec(substr($hex,2,1).substr($hex,2,1));
	} else {
		$r = hexdec(substr($hex,0,2));
		$g = hexdec(substr($hex,2,2));
		$b = hexdec(substr($hex,4,2));
	} // if(strlen($hex) == 3) { ... } else { ... }
	$rgb = array($r, $g, $b);
	return implode(",", $rgb); // returns the rgb values separated by commas
	//return $rgb; // returns an array with the rgb values
} // function hex2rgb($hex) {
	
$esquemaDeColorRGB = array();

$esquemaDeColorRGB['PRINCIPAL'] = hex2rgb($esquemaDeColor['PRINCIPAL']);
$esquemaDeColorRGB['COMPLEMENTARIO'] = hex2rgb($esquemaDeColor['COMPLEMENTARIO']);
$esquemaDeColorRGB['COMPLEMENTARIO-HOVER'] = hex2rgb($esquemaDeColor['COMPLEMENTARIO-HOVER']);
$esquemaDeColorRGB['MEDIO'] = hex2rgb($esquemaDeColor['MEDIO']);
$esquemaDeColorRGB['SECUNDARIO'] = hex2rgb($esquemaDeColor['SECUNDARIO']);
?>

			/*  CSS RESET  */
				  /**/
				  /**/
				  /**/
html, body, div, span, applet, object, iframe,
h1, h2, h3, h4, h5, h6, p, blockquote, pre,
a, abbr, acronym, address, big, cite, code,
del, dfn, em, img, ins, kbd, q, s, samp,
small, strike, strong, sub, sup, tt, var,
b, u, i, center,
dl, dt, dd, ol, ul, li,
fieldset, form, label, legend,
table, caption, tbody, tfoot, thead, tr, th, td,
article, aside, canvas, details, embed, 
figure, figcaption, footer, header, hgroup, 
menu, nav, output, ruby, section, summary,
time, mark, audio, video {
	margin: 0;
	padding: 0;
	border: 0;
	font-size: 100%;
	font: inherit;
	vertical-align: baseline;
}

/* HTML5 display-role reset for older browsers */
article, aside, details, figcaption, figure, 
footer, header, hgroup, menu, nav, section {
	display: block;
}

body { line-height: 1; }

ol, ul { list-style: none; }

blockquote, q { quotes: none; }

blockquote:before, blockquote:after,
q:before, q:after {
	content: '';
	content: none;
}

table {
	border-collapse: collapse;
	border-spacing: 0;
}

::selection {
    background-color: rgb(<?= $esquemaDeColorRGB['PRINCIPAL']; ?>);
    background-color: rgba(<?= $esquemaDeColorRGB['PRINCIPAL']; ?>,0.7);
    color: #<?= $esquemaDeColor['COMPLEMENTARIO']; ?>;
}

::-moz-selection {
    background-color: rgb(<?= $esquemaDeColorRGB['PRINCIPAL']; ?>);
    background-color: rgba(<?= $esquemaDeColorRGB['PRINCIPAL']; ?>,0.7);
    color: #<?= $esquemaDeColor['COMPLEMENTARIO']; ?>;
}

html, body {
    padding: 0;
    margin: 0;
    font-family: Gotham, "Helvetica Neue", Helvetica, Arial, sans-serif;
}

body { background-color: #<?= $esquemaDeColor['COMPLEMENTARIO-HOVER']; ?>; }

* { color: #<?= $esquemaDeColor['COMPLEMENTARIO']; ?>; }

header {
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    box-sizing: border-box;
    position: fixed;
    top: 0;
    left: 0;
    width: 100%;
    height: 75px;
    padding: 8px 25px;
    background-color: #<?= $esquemaDeColor['SECUNDARIO']; ?>;
    border-bottom: 2px solid #<?= $esquemaDeColor['MEDIO']; ?>;
    z-index: 200;
    -webkit-box-shadow: 1px 2px 14px 0px rgba(50, 50, 50, 0.54);
    -moz-box-shadow:    1px 2px 14px 0px rgba(50, 50, 50, 0.54);
    box-shadow:         1px 2px 14px 0px rgba(50, 50, 50, 0.54);
}

nav,
.main {
    display: block;
    position: relative;
    width: 100%;
    text-align: justify;
}

nav { height: 30px; }

nav:after,
.main:after {
    content: "";
    display: inline-block;
    width: 100%;
}

nav a {
    display: inline-block;
    color: #<?= $esquemaDeColor['COMPLEMENTARIO']; ?>;
    text-decoration: none;
    font-weight: bold;
    font-size: 13px;
    line-height: 30px;
}

nav a:hover,
nav a:focus,
nav a:active {
	color: #<?= $esquemaDeColor['COMPLEMENTARIO-HOVER']; ?>;
}

nav.primario h1 {
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    box-sizing: border-box;
    position: relative;
    padding-left: 40px;
	line-height: 24px;
}

nav.primario img {
	position: absolute;
    top: 0;
    left: 0;
	width: 32px;
    height: 24px;
    line-height: 24px;
}

nav.secundario {
	border-top: 1px solid #<?= $esquemaDeColor['COMPLEMENTARIO-HOVER']; ?>;
}

nav.secundario a { font-size: 11px; }

.main {
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    box-sizing: border-box;
    margin-top: 60px;
    padding: 30px 30px 0;
}

section {
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    box-sizing: border-box;
    display: inline-block;
    position: relative;
    vertical-align: top;
    width: 47%;
    margin-bottom: 50px;
    padding-top: 47%;
    /*padding: 15px 20px;*/
    background-color: rgba(<?= $esquemaDeColorRGB['PRINCIPAL']; ?>, 0.7);
    border: 1px solid #<?= $esquemaDeColor['SECUNDARIO']; ?>;
    text-align: center;
}

section:after {
	content: '';
    display: block;
    position: absolute;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
    background-position: center;
    background-repeat: no-repeat;
    -webkit-background-size: cover;
    -moz-background-size: cover;
    background-size: cover;
    z-index: -1;
}

section#logico-matematica:after {
    background-image: url('../img/matematica2.jpg');
}
    
section#linguistico-verbal:after {
    background-image: url('../img/lenguaje2.png');
}
    
section#corporal-kinestesica:after {
    background-image: url('../img/cinestetica.png');
}
    
section#espacial-visual:after {
    background-image: url('../img/espacial2.jpg');
}
    
section#musical:after {
    background-image: url('../img/musical3.jpg');
}
    
section#interpersonal:after {
    background-image: url('../img/interpersonal2.jpg');
}
    
section#intrapersonal:after {
    background-image: url('../img/intrapersonal.jpg');
}
    
section#naturalista:after {
    background-image: url('../img/naturalista3.jpg');
}

#introduccion {
	position: relative;
    width: 100%;
	padding: 0;
    margin-top: 30px;
    background: none;
    border: none;
    text-align: left;
}

#introduccion * { color: #<?= $esquemaDeColor['SECUNDARIO']; ?>; }

#introduccion h1 {
	display: inline-block;
    width: auto;
    padding-bottom: 3px;
    border-bottom: 1px solid #<?= $esquemaDeColor['SECUNDARIO']; ?>;
    margin-bottom: 5px;
    clear: both;
}

#introduccion span {
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    box-sizing: border-box;
	display: inline-block;
    width: 100%;
    margin-top: 15px;
    padding: 15px;
    text-align: center;
    background: #<?= $esquemaDeColor['SECUNDARIO']; ?>;
    color: #<?= $esquemaDeColor['COMPLEMENTARIO']; ?>;
    font-size: 18px;
}

.contenido {
	position: absolute;
    top: 15px;
    right: 20px;
    left: 20px;
    bottom: 15px;
}

section h1 {
	font-weight: bold;
    margin: 20px 0 80px;
}

section h1 span,
section h2,
section h3 {
	display: inline-block;
    width: 100%;
    line-height: 140%;
    font-weight: normal;
}

section h2,
section h3 {
    text-decoration: underline;
}

section p { margin: 10px 0 25px; }