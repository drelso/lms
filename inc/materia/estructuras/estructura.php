<?php
if( isset( $estructuraNum ) ) {
	
	echo '<link rel="stylesheet" href="' . BASEDIR . '/inc/materia/estructuras/estilos/estilos-estructuras.css"/>';
	
	if( $estructuraNum == 1 ) {
	?>
		<div class="estructura" id="estructura-<?= $estructuraNum; ?>">
			<nav class="fondo-principal borde-secundario">
				<h1 class="color-complementario">Logo</h1>
				
				<ul class="menu-estructura color-complementario-hover">
					<li>Opción 1</li>
					<li>Opción 2</li>
					<li>Opción 3</li>
					<li>Opción 4</li>
					<li>Opción 5</li>
				</ul> <!-- /menu-estructura -->
			</nav>
			
			<div class="main-estructura">
				<section class="fondo-secundario color-complementario">
					<h2>Encabezado</h2>
					——————————————
					——————————————
					——————————————
					——————————————
					——————————————
					——————————————
					——————————————
					——————————————
				</section>
				
				<section class="fondo-secundario color-complementario">
					<h2>Encabezado</h2>
					——————————————
					——————————————
					——————————————
					——————————————
					——————————————
					——————————————
					——————————————
					——————————————
				</section>
				
				<section class="fondo-secundario color-complementario">
					<h2>Encabezado</h2>
					——————————————
					——————————————
					——————————————
					——————————————
					——————————————
					——————————————
					——————————————
					——————————————
				</section>
				
				<section class="fondo-secundario color-complementario">
					<h2>Encabezado</h2>
					——————————————
					——————————————
					——————————————
					——————————————
					——————————————
					——————————————
					——————————————
					——————————————
				</section>
			</div> <!-- /main-estructura -->
			
			<footer></footer>
		</div> <!-- /estructura -->
	
	<?php
	} // if( $estructuraNum == 1 ) {
	
	
	if( $estructuraNum == 2 ) {
	?>
		<div class="estructura" id="estructura-<?= $estructuraNum; ?>">
			<nav class="fondo-complementario borde-secundario color-principal">
				<h1>Logo</h1>
				
				<ul class="menu-estructura">
					<li>Opción 1</li>
					<li>Opción 2</li>
					<li>Opción 3</li>
					<li>Opción 4</li>
					<li>Opción 5</li>
				</ul> <!-- /menu-estructura -->
			</nav>
			
			<div class="main-estructura fondo-principal color-complementario">
				<section>
					<h2>Encabezado</h2>
					——————————————
					——————————————
					——————————————
					——————————————
					——————————————
					——————————————
					——————————————
					——————————————
				</section>
				
				<section>
					<h2>Encabezado</h2>
					——————————————
					——————————————
					——————————————
					——————————————
					——————————————
					——————————————
					——————————————
					——————————————
				</section>
				
				<section>
					<h2>Encabezado</h2>
					——————————————
					——————————————
					——————————————
					——————————————
					——————————————
					——————————————
					——————————————
					——————————————
				</section>
				
				<section>
					<h2>Encabezado</h2>
					——————————————
					——————————————
					——————————————
					——————————————
					——————————————
					——————————————
					——————————————
					——————————————
				</section>
			</div> <!-- /main-estructura -->
			
			<footer></footer>
		</div> <!-- /estructura -->
	
	<?php
	} // if( $estructuraNum == 2 ) {
	
	
	if( $estructuraNum == 3 ) {
	?>
		<div class="estructura fondo-complementario" id="estructura-<?= $estructuraNum; ?>">
			<nav class="borde-complementario-hover">
				<h1 class="color-principal">Logo</h1>
				
				<ul class="menu-estructura color-secundario">
					<li>Opción 1</li>
					<li>Opción 2</li>
					<li>Opción 3</li>
					<li>Opción 4</li>
					<li>Opción 5</li>
				</ul> <!-- /menu-estructura -->
			</nav>
			
			<div class="main-estructura color-medio">
				<section class="">
					<h2>Encabezado</h2>
					——————————————
					——————————————
					——————————————
					——————————————
					——————————————
					——————————————
					——————————————
					——————————————
				</section>
				
				<section class="">
					<h2>Encabezado</h2>
					——————————————
					——————————————
					——————————————
					——————————————
					——————————————
					——————————————
					——————————————
					——————————————
				</section>
				
				<section class="">
					<h2>Encabezado</h2>
					——————————————
					——————————————
					——————————————
					——————————————
					——————————————
					——————————————
					——————————————
					——————————————
				</section>
				
				<section class="">
					<h2>Encabezado</h2>
					——————————————
					——————————————
					——————————————
					——————————————
					——————————————
					——————————————
					——————————————
					——————————————
				</section>
			</div> <!-- /main-estructura -->
			
			<footer></footer>
		</div> <!-- /estructura -->
	
	<?php
	} // if( $estructuraNum == 3 ) {
	
	
	if( $estructuraNum == 4 ) {
	?>
		<div class="estructura fondo-secundario color-complementario" id="estructura-<?= $estructuraNum; ?>">
			<nav class="color-complementario-hover">
				<h1 class="borde-complementario-hover">Logo</h1>
				
				<ul class="menu-estructura">
					<li>Opción 1</li>
					<li>Opción 2</li>
					<li>Opción 3</li>
					<li>Opción 4</li>
					<li>Opción 5</li>
				</ul> <!-- /menu-estructura -->
			</nav>
			
			<div class="main-estructura">
				<section class="">
					<h2>Encabezado</h2>
					——————————————
					——————————————
					——————————————
					——————————————
					——————————————
					——————————————
					——————————————
					——————————————
				</section>
				
				<section class="">
					<h2>Encabezado</h2>
					——————————————
					——————————————
					——————————————
					——————————————
					——————————————
					——————————————
					——————————————
					——————————————
				</section>
				
				<section class="">
					<h2>Encabezado</h2>
					——————————————
					——————————————
					——————————————
					——————————————
					——————————————
					——————————————
					——————————————
					——————————————
				</section>
				
				<section class="">
					<h2>Encabezado</h2>
					——————————————
					——————————————
					——————————————
					——————————————
					——————————————
					——————————————
					——————————————
					——————————————
				</section>
			</div> <!-- /main-estructura -->
			
			<footer></footer>
		</div> <!-- /estructura -->
	
	<?php
	} // if( $estructuraNum == 4 ) {
	
} // if( isset( $estructuraNum ) ) {
?>