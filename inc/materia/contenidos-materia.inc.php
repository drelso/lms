<?php
// Formulario de administración de materias

if( isset( $grupo ) && isset( $materia ) ) {
	
	$dirUploads = BASEDIR . '/uploads/';
	
		
	if( !isset( $bd ) ) {
		require_once( __DIR__ . '/../db/bd.class.php');
		$bd = new BD();
	} // if( !isset($bd) ) {
		
		require_once( __DIR__ . '/../clases/tema.class.php');
		require_once( __DIR__ . '/../clases/evaluacion.class.php'); ?>

    	<div id="temas">
            
            <?php
			$temasMateria = $materia->getTemas();
			
			$contPaso = 2;
			
			foreach( $temasMateria as $tema ) { ?>
				
                <?php
				$contenidos = Tema::getContenidos( $tema->getID() );
				?>
				
				<section class="contenido-tema paso-materia" id="paso-<?= $contPaso; ?>" data-id="<?= $tema->getID(); ?>">
					
					<h2 class="titulo-tema" data-id="<?= $tema->getID(); ?>"><?= $tema->getNombre(); ?></h2>
					
					<div class="contenidos" data-id="<?= $contPaso; ?>">
		                    	
						<?php
						foreach( $contenidos as $contenido ) {
							
							$tipoContenido = $contenido->getTipoDeContenido();
							
							// El tipo de contenido 5 es para evaluaciones
							if( $tipoContenido->getID() != 5 ) { ?>
							
								<div class="contenido seleccionable" data-id="<?= $contenido->getID(); ?>">
	                            	<?php
									//echo selectContenidos( $tipoContenido->getID() );
									
									$recomendado = 0.0;
									
									if( isset($recomendaciones) ) {
										if( $recomendaciones != false ) {
											switch( $tipoContenido->getID() ) {
												case 1: $recomendado = $recomendaciones['narrativo']; break;
												case 3: $recomendado = $recomendaciones['video']; break;
												case 4: $recomendado = $recomendaciones['audio']; break;
												case 6: $recomendado = $recomendaciones['presentacion']; break;
											} // switch( $tipoContenido->getID() ) {
										} // if( $recomendaciones != false ) {
									} // if( isset($recomendaciones) ) {
									
									$textoRecomendacion = '';
									
									if( $recomendado != 0.0 ) {
										$textoRecomendacion = 'Recomendado ' . round( ($recomendado * 100), 2) . '%';
									} // if( $recomendado != 0.0 ) {
									
									?>
									
	                				<h3><?= $contenido->getNombre(); ?></h3>
	                				<h4>Tipo: <?= $tipoContenido->getNombre(); ?> <span><?= $textoRecomendacion; ?></span></h4>
	                                
	                                <div class="info-contenido">
		                                <?php
		                                $informacion = $contenido->getInformacion();
		                                
		                                if( !empty( $informacion ) ) {
		                                	echo '<p>' . $contenido->getInformacion() . '</p>';
		                                } // if( !empty( $contenido->getInformacion() ) ) {
		                                
		                                switch( $tipoContenido->getID() ) {
		                                	
		                                	case 2:
		                                		// Tipo de contenido: Imagen
		                                		$nombreArchivo = $contenido->getArchivo();
		                                		
		                                		if( !empty( $nombreArchivo ) ) {
		                                			
		                                			$rutaImagen =  BASEDIR  . '/uploads/' . $nombreArchivo;
		                                			
		                                			if( existeUrl($rutaImagen) ) {
		                                				echo '<img src="' . $rutaImagen . '"/>';
		                                			} // if( file_exists ($rutaImagen) ) {
		                                		} // if( !empty( $nombreArchivo ) ) {
		                                		// Caso para contenido de tipo imagen
		                                		
		                                		break;
		                                	
		                                	case 3:
		                                		// Tipo de contenido: Video
		                                		$iframeVideo = $contenido->getIFrame();
		                                		
		                                		if( !empty($iframeVideo) ) {
		                                			echo $iframeVideo;
		                                		} else {
			                                		$nombreArchivo = $contenido->getArchivo();
			                                		
			                                		if( !empty( $nombreArchivo ) ) {
			                                			
			                                			$rutaVideo =  BASEDIR  . '/uploads/' . $nombreArchivo;
			                                			
			                                			echo '<video width="320" height="240" controls><source src="' . $rutaVideo . '" type="video/mp4">Tu navegador no soporta los videos HTML5.</video>';
			                                			
			                                		} // if( !empty( $nombreArchivo ) ) {
		                                		} // if( !empty($iframeVideo) ) { ... else ...
		                                		break;
		                                	
		                                	case 4:
		                                		// Tipo de contenido: Audio
		                                		$nombreArchivo = $contenido->getArchivo();
		                                		
		                                		if( !empty( $nombreArchivo ) ) {
		                                			
		                                			$rutaAudio =  BASEDIR  . '/uploads/' . $nombreArchivo;
		                                			
		                                			echo '<audio controls><source src="' . $rutaAudio . '" type="audio/mpeg">Tu navegador no soporta los videos HTML5.</audio>';
		                                			
		                                		} // if( !empty( $nombreArchivo ) ) {
		                                		break;
		                                	
		                                	case 1:
		                                	case 6:
		                                		// Tipo de contenido: Texto y Presentación
		                                		$nombreArchivo = $contenido->getArchivo();
		                                		
		                                		if( !empty( $nombreArchivo ) ) {
		                                			
		                                			$rutaPDF =  BASEDIR  . '/uploads/' . $nombreArchivo;
		                                			
		                                			echo '<embed src="' . $rutaPDF . '#view=FitH" width="600" height="500" alt="pdf" pluginspage="http://www.adobe.com/products/acrobat/readstep2.html">';
		                                			
		                                		} // if( !empty( $nombreArchivo ) ) {
		                                		break;
		                                } // switch( $tipoContenido->getID() ) {
		                                
		                                ?>
	                                </div> <!-- /info-contenido -->
	                			</div> <!-- /contenido -->
                			
							<?php
							} // if( $tipoContenido->getID() != 5 ) {
							
						} // foreach( $contenidos as $contenido ) { ?>
                        
                    </div> <!-- /contenidos -->
                    
					<h3 class="error" id="error-paso-<?= $contPaso; ?>"></h3>
					
                    <button class="siguiente-paso" data-id="<?= $contPaso; ?>" data-tipo="contenido">Continuar</button>
                    
				</section> <!-- /contenido-tema-<?= $tema->getID(); ?> -->
				
				<?php
				$contPaso++;
				
				$tieneEvaluacion = false;
				
				foreach( $contenidos as $contenido ) {
					$tipoContenido = $contenido->getTipoDeContenido();
                    if( $tipoContenido->getID() == 5 ) { $tieneEvaluacion = true; }
	            } // foreach( $contenidos as $contenido ) {
				
				
				if( $tieneEvaluacion ) {
				?>
					<section class="evaluacion-tema paso-materia" id="paso-<?= $contPaso; ?>" data-id="<?= $tema->getID(); ?>">
						<h2 class="titulo-tema" data-id="<?= $tema->getID(); ?>"><?= $tema->getNombre(); ?> - Evaluación</h2>
						
						
						<div class="contenido-evaluacion">
						
							<?php
							foreach( $contenidos as $contenido ) {
									
								$tipoContenido = $contenido->getTipoDeContenido();
									
								// Revisa si el contenido es una
								// evaluación (la evaluación recibe el
								// ID 5)
								//
			                    if( $tipoContenido->getID() == 5 ) {
									
									$evaluacion = new Evaluacion( $contenido->getID() );
									
									$reactivos = $evaluacion->getReactivos();
									?>
			                    	<div class="preguntas">
			                        	<?php foreach( $reactivos as $reactivo ) { ?>
			                                <div class="pregunta" data-idevaluacion="<?= $reactivo->getIdEvaluacion(); ?>" data-orden="<?= $reactivo->getOrden(); ?>">
			                                	
			                                	<div class="info-pregunta">
				                                    <h3><?= $reactivo->getPregunta(); ?></h3>
				                                    
				                                    <?php
				                                    $imagenPregunta = $reactivo->getImagenPregunta();
	                                                if( !empty($imagenPregunta) ) {
	                                                	echo '<img src="' . $dirUploads . $reactivo->getImagenPregunta() . '"/>';
	                                                } // if( !empty($respuesta[1]) ) {
	                                                ?>
			                                    </div> <!-- /info-pregunta -->
			                                    
			                                    <div class="respuestas">
			                                    
													<?php
			                                        $respuestas = $reactivo->getRespuestas();
													$correctas = $reactivo->getCorrectas();
													
													$indiceRespuesta = 0;
			                                        
			                                        $numRespuesta = 0;
			                                        
			                                        foreach( $respuestas as $respuesta ) {
														
			                                        	if( !empty($respuesta[0]) || !empty($respuesta[1]) ) { ?>
			                                                <label>
			                                                	<input type="checkbox" name="checkbox-respuesta" value="<?= $numRespuesta; ?>"/><?= $respuesta[0]; ?>
			                                                	
			                                                	<?php
			                                                	if( !empty($respuesta[1]) ) {
			                                                		echo '<img src="' . $dirUploads . $respuesta[1] . '"/>';
			                                                	} // if( !empty($respuesta[1]) ) {
			                                                	?>
			                                                </label>
			                                        		<?php
			                                        		$numRespuesta++;
														} // if( !empty($respuesta[0]) || !empty($respuesta[1]) ) {
														
														$indiceRespuesta++;
														
			                                        } // foreach( $respuestas as $respuesta ) {
													?>
			                                    </div> <!-- /respuestas -->
			                                    
			                                </div> <!-- /pregunta -->
			                            <?php } // foreach( $reactivos as $reactivo ) {  ?>
			                        </div> <!-- /preguntas -->
								<?php
								} // if( $tipoContenido->getID() == 5 ) { ... else ...
							} // foreach( $contenidos as $contenido ) { ?>
							
							<h3 class="error" id="error-paso-<?= $contPaso; ?>"></h3>
							
							<button class="enviar-evaluacion" data-id="<?= $contPaso; ?>">Enviar evaluación</button>
						
						</div> <!-- /contenido-evaluacion -->
						
						<div class="resultados-evaluacion"  data-id="<?= $contPaso; ?>">
	                    	<h2></h2>
	                    	<button class="siguiente-paso" data-id="<?= $contPaso; ?>" data-tipo="evaluacion">Continuar</button>
	                    </div> <!-- /resultados-evaluacion -->
	                    
					</section> <!-- /paso-<?php $contPaso; ?> -->
					
					<?php
					$contPaso++;
				} // if( $tieneEvaluacion ) {
			} // foreach( $temasMateria as $tema ) { 
			
			
			require_once( __DIR__ . '/cuestionario.inc.php' );
			?>
            
            
            <section class="paso-materia" id="agradecimiento">
            	<h2>Muchas gracias por tu participación</h2>
            </section> <!-- /agradecimiento -->
            
            
        </div> <!-- /temas -->
        
    <?php include_once( __DIR__ . '/../shadowbox.php' ); ?>
    
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
    
	<script type="text/javascript" src="inc/js/jquery-1.11.3.min.js"></script>
    
	<script type="text/javascript" src="inc/js/jquery-ui.min.js"></script>
<?php
} else { echo 'No tienes permisos'; } // if( isset( $nombreMateria ) && isset( $idMateria ) ) {

// Función que pregunta por cURL si la URL ingresada
// existe, lo que permite definir si un archivo existe
function existeUrl( $url ) {
	$ch = curl_init($url);    
	
	curl_setopt($ch, CURLOPT_NOBODY, true);
	curl_exec($ch);
	
	$code = curl_getinfo($ch, CURLINFO_HTTP_CODE);

	if($code == 200) {
		$status = true;
	} else {
		$status = false;
	} // if($code == 200) {
	
	curl_close($ch);
	return $status;
} // function existeUrl( $url ) {

?>