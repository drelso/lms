<?php
// Cuestionario cualitativo de la plataforma

if( isset($contPaso) ) {
?>
	<section class="evaluacion-tema cuestionario-final paso-materia" id="paso-<?= $contPaso; ?>">
		<h2 class="titulo-tema">Cuestionario de opinión</h2>
		
		<div class="preguntas">
			<div class="pregunta opcion-multiple" id="retro-1">
				<h3>¿Mejora tu experiencia de aprendizaje elegir la estructura y la paleta de colores de tu interfaz de usuario?</h3>

				<div class="respuestas">
					<label class=""><input type="radio" name="cuestionario-respuesta-1" value="3">Mucho</label>

					<label class=""><input type="radio" name="cuestionario-respuesta-1" value="2">Algo</label>

					<label class=""><input type="radio" name="cuestionario-respuesta-1" value="1">Nada</label>
				</div> <!-- /respuestas -->

			</div> <!-- /pregunta -->
			
			
			<div class="pregunta opcion-multiple" id="retro-2">
				<h3>¿Mejora tu humor y tu disposición de aprendizaje la paleta de colores que elegiste?</h3>

				<div class="respuestas">
					<label class=""><input type="radio" name="cuestionario-respuesta-2" value="3">Mucho</label>

					<label class=""><input type="radio" name="cuestionario-respuesta-2" value="2">Algo</label>

					<label class=""><input type="radio" name="cuestionario-respuesta-2" value="1">Nada</label>
				</div> <!-- /respuestas -->

			</div> <!-- /pregunta -->
			
			
			<div class="pregunta opcion-multiple" id="retro-3">
				<h3>¿La estructura seleccionada ayuda en la navegación de contenidos?</h3>

				<div class="respuestas">
					<label class=""><input type="radio" name="cuestionario-respuesta-3" value="3">Mucho</label>

					<label class=""><input type="radio" name="cuestionario-respuesta-3" value="2">Algo</label>

					<label class=""><input type="radio" name="cuestionario-respuesta-3" value="1">Nada</label>
				</div> <!-- /respuestas -->

			</div> <!-- /pregunta -->
			
			
			<div class="pregunta opcion-multiple" id="retro-4">
				<h3>¿La selección de recursos de aprendizaje ayuda a mejorar el proceso de comprensión del material?</h3>

				<div class="respuestas">
					<label class=""><input type="radio" name="cuestionario-respuesta-4" value="3">Mucho</label>

					<label class=""><input type="radio" name="cuestionario-respuesta-4" value="2">Algo</label>

					<label class=""><input type="radio" name="cuestionario-respuesta-4" value="1">Nada</label>
				</div> <!-- /respuestas -->

			</div> <!-- /pregunta -->
			
			
			<div class="pregunta opcion-multiple" id="retro-5">
				<h3>¿Resultaron útiles las recomendaciones al elegir un tipo de recurso de aprendizaje?</h3>

				<div class="respuestas">
					<label class=""><input type="radio" name="cuestionario-respuesta-5" value="3">Mucho</label>

					<label class=""><input type="radio" name="cuestionario-respuesta-5" value="2">Algo</label>

					<label class=""><input type="radio" name="cuestionario-respuesta-5" value="1">Nada</label>
				</div> <!-- /respuestas -->

			</div> <!-- /pregunta -->
			
			
			<div class="pregunta opcion-multiple" id="retro-6">
				<h3>¿Consideras que los resultados de las evaluaciones reflejan tu comprensión del material?</h3>

				<div class="respuestas">
					<label class=""><input type="radio" name="cuestionario-respuesta-6" value="3">Mucho</label>

					<label class=""><input type="radio" name="cuestionario-respuesta-6" value="2">Algo</label>

					<label class=""><input type="radio" name="cuestionario-respuesta-6" value="1">Nada</label>
				</div> <!-- /respuestas -->

			</div> <!-- /pregunta -->
			
			
			<div class="pregunta" id="retro-7">
				<h3>¿Qué tipo de recurso de aprendizaje adicional sugerirías?</h3>

				<div class="respuestas">
					<textarea name="cuestionario-respuesta-7" maxlength="1000"></textarea>
				</div> <!-- /respuestas -->

			</div> <!-- /pregunta -->
			
			
			<div class="pregunta" id="retro-8">
				<h3>¿Qué sugieres para mejorar el ambiente de aprendizaje?</h3>

				<div class="respuestas">
					<textarea name="cuestionario-respuesta-8" maxlength="1000"></textarea>
				</div> <!-- /respuestas -->

			</div> <!-- /pregunta -->
			
			
			<div class="pregunta" id="retro-9">
				<h3>¿Te pareció que el sistema es útil en el proceso de aprendizaje?</h3>

				<div class="respuestas">
					<textarea name="cuestionario-respuesta-9" maxlength="1000"></textarea>
				</div> <!-- /respuestas -->

			</div> <!-- /pregunta -->
			
			
		</div> <!-- /preguntas -->
		
		
		<h3 class="error" id="error-finalizar"></h3>
		
        <button id="boton-finalizar" data-id="<?= $contPaso; ?>">Finalizar</button>
        
	</section>
<?php
} // if( isset($contPaso) ) {

?>