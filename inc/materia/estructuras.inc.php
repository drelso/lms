<?php
// Estructuras gráficas para los recursos de aprendizaje

if( isset($grupo) && isset($usuario) ) { ?>
	
	<h2>Paso 2: Estructuras gráficas</h2>

	<p>Seleccione la estructura que más le agrade</p>
	
	<div class="contenedor-estructuras">
		<label>
			<input type="radio" name="estructura" value="1"/><span>Estructura 1</span>
			
			<?php
			$estructuraNum = 1;
			include( __DIR__ . '/estructuras/estructura.php' );
			?>
			
		</label>
		
		
		<label>
			<input type="radio" name="estructura" value="2"/><span>Estructura 2</span>
			
			<?php
			$estructuraNum = 2;
			include( __DIR__ . '/estructuras/estructura.php' );
			?>
			
		</label>
		
		
		<label>
			<input type="radio" name="estructura" value="3"/><span>Estructura 3</span>
			
			<?php
			$estructuraNum = 3;
			include( __DIR__ . '/estructuras/estructura.php' );
			?>
			
		</label>
		
		
		<label>
			<input type="radio" name="estructura" value="4"/><span>Estructura 4</span>
			
			<?php
			$estructuraNum = 4;
			include( __DIR__ . '/estructuras/estructura.php' );
			?>
			
		</label>
		
	</div> <!-- /contenedor-estructuras -->
<?php
} // if( isset($grupo) && isset($usuario) ) { 
?>