<?php
// Checa si la sesión ya existe
if(session_id() == '' || !isset($_SESSION)) {
    session_start();
}

require_once('config/config.php');
?>

<!doctype html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Aprendizaje adaptativo</title>
    
    <script src="inc/js/jquery-1.11.3.min.js"></script>
    
    <link rel="stylesheet" id="hoja-de-estilos" href="inc/css/estilos.css"/>
    
</head>

<body>

<?php

$id = 0;
$usuario;

// Si el usuario está registrado
// instancia un objeto de clase usuario
//
if(isset($_SESSION['usuario_registrado'])) {
	require_once('inc/clases/usuario.class.php');
	
	$id = intval($_SESSION['usuario_registrado']);
	
	if($id > 0) {
		$usuario = new Usuario($id);
	} // if($id > 0) {
	
} // if(isset($_SESSION['usuario_registrado'])) {

// Si existe el objeto de clase usuario
// genera la estructura del perfil
//
if(isset($usuario) && !empty($usuario)) {
	
	require_once('inc/db/bd.class.php');
	$bd = new BD();
	
	$primerRegistro = $bd->query( "SELECT * FROM primer_registro WHERE id_usuario = " . $usuario->getID() );
	
	if( $primerRegistro == false ) {
		echo 'Hubo un error con la base de datos:' . $bd->error();
	} else {
	
		if( $primerRegistro->num_rows == 0 ) { ?>
		
			<div id="ingresar-contrasena">
				<h2>Por favor establezca su contraseña:</h2>
				
				<div class="formulario-ingresar-contrasena">
					
					<h3 class="error"></h3>
					
					<input type="password" name="contrasena" placeholder="Contraseña"/>
					<input type="password" name="confirmar-contrasena" placeholder="Confirmar contraseña"/>
					
					<button id="establecer-contrasena">Establecer contraseña</button>
				</div> <!-- /formulario-ingresar-contrasena -->
			</div> <!-- /establecer-contrasena -->
			
			<script src="inc/js/jquery-1.11.3.min.js"></script>
			<script src="inc/js/establecer-contrasena.js"></script>
			
		<?php
		} // if( $primerRegistro->num_rows == 0 ) {
		
	} // if( $primerRegistro == false ) { ... else ...
	?>
	
	<header id="encabezado-general">
	    <h1>Aprendizaje adaptativo</h1>
	    
	    <nav class="primario">
	        <h3><?= $usuario->getNombre(); ?></h3>
	        <h5><?= $usuario->getMatricula(); ?></h5>
	        
	        <!-- <a href="#">Perfil</a> -->
	        <a href="<?= BASEDIR; ?>?logout=1">Cerrar sesión</a>
	    </nav> <!-- /primario -->
	    
	    <nav class="secundario">
	    </nav> <!-- /secundario -->
	</header>
<?php
} // if(isset($usuario) && !empty($usuario)) {
?>