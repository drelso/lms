<?php
// Clase Reactivo

class Reactivo {
	private $idEvaluacion;
	private $pregunta;
	private $imagen;
	private $respuestas = array();
	private $correctas = array();
	private $orden;
	private $bd;
	
	function __construct($idEvaluacion, $orden) {
		require_once( __DIR__ . '/../db/bd.class.php' );
		$this->bd = new BD();
		
		$this->idEvaluacion = intval($idEvaluacion);
		$this->orden = intval($orden);
		
		// Consulta a tabla de evaluaciones
		$resultados = $this->bd->query("SELECT * FROM evaluaciones WHERE id_contenido = " . $this->idEvaluacion . " AND orden = " . $orden );
		
		if($resultados == false) {
			
			echo 'Hubo un error con la base de datos:' . $this->bd->error();
			
		} else {
			
			foreach($resultados as $resultado) {
		
				$this->pregunta		=	$resultado['pregunta'];
				$this->imagen		=	$resultado['imagenPregunta'];
				
				for( $i = 1; $i <= 8; $i++ ) {
					array_push(
						$this->respuestas,
						array(
							$resultado['respuesta_' . $i ],
							$resultado['imagenRespuesta_' . $i ]
						)
					);
				} // for( $i = 0; $i <= 8; $i++ ) {
				
				// Genera un arreglo de enteros que corresponden
				// a los índices de las respuestas correctas
				$this->correctas	=	array_map('intval', explode( ",", $resultado['correctas'] ) );
				
				break;
			} // foreach($resultados as $resultado) {
			
		} // if($resultados == false) { ... else ...
	} // function __construct($id) {
	
	
	public function getIdEvaluacion() { return $this->idEvaluacion; }
	
	
	public function getPregunta() { return $this->pregunta; }
	
	
	public function setPregunta( $pregunta ) {
		$this->bd->query("UPDATE evaluaciones SET pregunta = " . $this->bd->escapar( $pregunta ) . " WHERE id_contenido = " . $this->idEvaluacion . " AND orden = " . $this->orden );
		
		$this->pregunta = $pregunta;
	} // public function setPregunta( $pregunta ) {
		
	
	public function getImagenPregunta() { return $this->imagen; }
	
	
	public function setImagenPregunta( $imagen ) {
		$this->bd->query("UPDATE evaluaciones SET imagenPregunta = " . $this->bd->escapar( $imagen ) . " WHERE id_contenido = " . $this->idEvaluacion . " AND orden = " . $this->orden );
		
		$this->imagen = $imagen;
	} // public function setPregunta( $pregunta ) {
	
	
	public function getRespuestas() { return $this->respuestas; }
	
	
	public function getRespuestasCorrectas() {
		
		$respuestas = array();
		
		foreach( $this->correctas as $correcta ) {
			array_push( $respuestas[$correcta] );
		} // foreach( $this->correctas as $correcta ) {
		
		return $respuestas;
		
	} // public function getRespuestasCorrectas() {
	
	
	public function setRespuestas( $respuestas ) {
		
		if( count( $respuestas ) > 0 && count( $respuestas ) <= 8 ) {
			
			$cont = 1;
			
			foreach( $respuestas as $respuesta ) {
				$this->bd->query("UPDATE evaluaciones SET respuesta_" . $cont . " = " . $this->bd->escapar( $respuesta ) . " WHERE id_contenido = " . $this->idEvaluacion . " AND orden = " . $this->orden );
				
				$cont++;
			} // foreach( $respuestas as $respuesta ) {
			
			$this->respuestas = $respuestas;
		} // if( count( $respuestas ) > 0 && count( $respuestas ) <= 8 ) {
	} // public function setRespuestas( $respuestas ) {
	
	
	public function getCorrectas() { return $this->correctas; }
	
	
	public function setCorrectas( $correctas ) {
		
		if( count( $correctas ) > 0 && count( $correctas ) <= 8 ) {
			
			$stringCorrectas = '';
			
			$primera = true;
			
			foreach( $correctas as $correcta ) {
				
				if( $primera ) {
					$primera = false;
				} else {
					$stringCorrectas .= ',';
				} // if( $primera ) { ... else ...
				
				$stringCorrectas .= intval( $correcta );
				
			} // foreach( $respuestas as $respuesta ) {
			
			$this->bd->query("UPDATE evaluaciones SET correctas = " . $stringCorrectas . " WHERE id_contenido = " . $this->idEvaluacion . " AND orden = " . $this->orden );
			
			$this->correctas = $correctas;
			
		} // if( count( $correctas ) > 0 && count( $correctas ) <= 8 ) {
		
	} // public function setCorrectas( $correctas ) {
	
	
	public function getOrden() { return $this->orden; }
	
	
	public function setOrden( $orden ) {
		$this->bd->query("UPDATE evaluaciones SET orden = " . intval( $orden ) . " WHERE id_contenido = " . $this->idEvaluacion . " AND orden = " . $this->orden );
		
		$this->orden = $orden;
	} // public setOrden( $orden ) {
	
	
	// $reactivo es un arreglo con la siguiente estructura:
	// $reactivo = array(
	//					'pregunta'		=> String,
	//					'respuestas'	=> array(String),
	//					'correctas'		=> array(Int)
	//				)
	//
	public static function agregarReactivo( $id, $reactivo ) {
		
		require_once( __DIR__ . '/../db/bd.class.php' );
		$bd = new BD();
		
		$respuestas = array();
		
		for( $i = 0; $i < 8; $i++ ) {
			//$respuestas[$i] = ( $reactivo['respuestas'][$i] == '' ) ? 'NULL' : $bd->escapar( $reactivo['respuestas'][$i] );
			
			$respuesta[$i] = ( !empty($reactivo['respuestas'][$i]) ) ? $reactivo['respuestas'][$i] : array("NULL","NULL");
		} // for( $i = 1; $i <= 8; $i++ ) {
			
		$respuestasString = implode(',',$respuestas);
		
		$correctasString = '';
		
		$primeraCorrecta = true;
		
		foreach( $reactivo['correctas'] as $correcta ) {
			if( $primeraCorrecta ) {
				$primeraCorrecta = false;
			} else {
				$correctasString .= ',';
			} // if( $primeraCorrecta ) { ... else ...
			
			$correctaTemp = intval( $correcta );
			
			if( $correctaTemp >= 0 && $correctaTemp < 8 ) {
				$correctasString .= $correctaTemp;
			} // if( $correctaTemp >= 0 && $correctaTemp < 8 ) {
			
		} // foreach( $reactivo['correctas'] as $correcta ) {
		
		
		$queryInsert = "INSERT INTO evaluaciones (
										id_contenido,
										
										pregunta,
										imagenPregunta,
										correctas,
										orden,
										
										respuesta_1,
										imagenRespuesta_1,
										respuesta_2,
										imagenRespuesta_2,
										respuesta_3,
										imagenRespuesta_3,
										respuesta_4,
										imagenRespuesta_4,
										respuesta_5,
										imagenRespuesta_5,
										respuesta_6,
										imagenRespuesta_6,
										respuesta_7,
										imagenRespuesta_7,
										respuesta_8,
										imagenRespuesta_8
								) VALUES (" .
										intval( $id ) . "," .
										
										$bd->escapar( $reactivo['pregunta'] ) . "," .
										$bd->escapar( $reactivo['imagen'] ) . "," .
										$bd->escapar( $correctasString ) . "," .
										$bd->escapar( $reactivo['orden'] );
										
		for( $i = 0; $i < 8; $i++ ) {
			if( !empty($reactivo['respuestas'][$i]) ) {
			$queryInsert .= "," . $bd->escapar( $reactivo['respuestas'][$i][0] );
			$queryInsert .= "," . $bd->escapar( $reactivo['respuestas'][$i][1] );
			} else {
				$queryInsert .= ",NULL,NULL";
			} // if( !empty($reactivo['respuestas'][$i]) ) {
		} // for( $i = 1; $i <= 8; $i++ ) {
		
		$queryInsert .= ")";
		
		//echo 'QUERY>>> ' . $queryInsert;
		
		// DESCOMENTAR
		//$bd->query("INSERT INTO evaluaciones (id_contenido,pregunta,respuesta_1,respuesta_2,respuesta_3,respuesta_4,respuesta_5,respuesta_6,respuesta_7,respuesta_8,correctas,orden) VALUES (" . intval( $id ) . "," . $bd->escapar( $reactivo['pregunta'] ) . "," . $respuestasString . "," . $bd->escapar( $correctasString ) . "," . $bd->escapar( $reactivo['orden'] ) . ")");
		
		$bd->query( $queryInsert );
		
		
		// Corregir creación de objeto Reactivo
		//
		// return new Reactivo( $bd->idInsertado() );
		
		
		//array_push($this->reactivos, $reactivo);
		
	} // public function agregarReactivo( $reactivo ) {
	
	
} // class Reactivo {
?>