<?php
// Clase Materia

class Materia {
	private $id;
	private $nombre;
	private $temas = array();
	private $departamento;
	private $bd;
	
	function __construct($id) {
		require_once( __DIR__ . '/../db/bd.class.php' );
		$this->bd = new BD();
		
		$this->id = $this->bd->escapar( $id );
		
		require_once('tema.class.php');
		
		
		// Consulta a tabla de tema
		$idTemas = $this->bd->query("SELECT id FROM tema WHERE id_materia = " . $this->id);
		
		if( $idTemas == false ) {
			echo 'Hubo un error con la base de datos:' . $this->bd->error();
		} else {
			if( $idTemas->num_rows > 0 ) {
				foreach( $idTemas as $idTema ) {
					$tema = new Tema( $idTema['id'] );
					
					array_push( $this->temas, $tema );
				} // foreach( $idTemas as $idTema ) {
			} // if( $idTemas->num_rows > 0 ) {
		} // 
		
		//$this->tema = new Tema( $this->id );
		
		// Consulta a tabla de materia
		$resultados = $this->bd->query("SELECT * FROM materia WHERE id = " . $this->id);
		
		if( $resultados == false ) {
			echo 'Hubo un error con la base de datos:' . $this->bd->error();
		} else {
			foreach( $resultados as $resultado ) {
				$this->nombre	=	$resultado['nombre'];
				
				if( isset( $resultado['id_departamento'] ) ) {
					require_once('departamento.class.php');
					
					$this->departamento = new Departamento( $resultado['id_departamento'] );
				} // if( isset( $resultado['id_departamento'] ) ) {
				
				break;
			} // foreach($resultados as $resultado) {
		} // if($resultados == false) { ... else ...
	} // function __construct($id) {
	
	
	public function getID() { return $this->id; }
	
	
	public function setID( $id ) {
		
		$nuevoID = $this->bd->escapar( $id );
		
		$existeID = $this->bd->query("SELECT * FROM materia WHERE id = " . $nuevoID );
		
		if( $existeID == false ) {
			echo 'Hubo un error con la base de datos:' . $this->bd->error();
		} else {
		
			if( $existeID->num_rows == 0 ) {
				
				$this->bd->query('UPDATE materia SET id = ' . $nuevoID . ' WHERE id = ' . $this->id);
				$this->id = $nuevoID;
				
			} else {
				
				echo 'El ID ingresado ya existe';
				
			} // if( $existeID->num_rows == 0 ) {
		} // if( $existeID == false ) { ... else ...
		
	} // public function setID( $id ) {
	
	
	public function getNombre() { return $this->nombre; }
	
	
	public function setNombre( $nombre ) {
		$this->bd->query('UPDATE materia SET nombre = ' . $this->bd->escapar($nombre) . ' WHERE id = ' . $this->id);
		$this->nombre = $nombre;
	} // public function setNombre($nombre) {
	
	
	public function getTemas() { return $this->temas; }
	
	
	public function getDepartamento() { return $this->departamento; }
	
	
	public function setDepartamento( $idDepartamento ) {
		
		$idDepartamento = $this->bd->escapar($idDepartamento );
		
		$existeDepto = $this->bd->query("SELECT * FROM departamento WHERE id = " . $idDepartamento );
		
		if( $existeDepto == false ) {
			echo 'Hubo un error con la base de datos:' . $this->bd->error();
		} else {
		
			if( $existeDepto->num_rows !== 0 ) {
				
				$this->bd->query('UPDATE materia SET id_departamento = ' . $idDepartamento . ' WHERE id = ' . $this->id);
				$this->departamento = $idDepartamento;
				
			} else {
				
				echo 'El departamento ingresado no existe';
				
			} // if( $existeDepto->num_rows == 0 ) {
		} // if( $existeDepto == false ) { ... else ...
		
	} // public function setDepartamento( $idDepartamento ) {
	
	
	public function borrarContenido() {
		
		$temasMateria = $this->bd->query("SELECT * FROM tema WHERE id_materia = " . $this->id );
		
		echo 'TEMAS: ' . $temasMateria->num_rows . '<br>';
		
		if( $temasMateria == false ) {
			echo 'Hubo un error con la base de datos:' . $this->bd->error();
		} else {
			
			if( $temasMateria->num_rows != 0 ) {
				
				foreach( $temasMateria as $tema ) {
					
					$idTema = $tema['id'];
					
					echo 'TEMA ' . $idTema . '<br>';
					
					$contenidos = $this->bd->query("SELECT id FROM contenidos WHERE id_tema = ". $idTema );
					
					if( $contenidos == false ) {
						echo 'Hubo un error con la base de datos:' . $this->bd->error();
					} elseif( $contenidos->num_rows != 0 ) {
						
						foreach( $contenidos as $contenido ) {
							
							$borrarEvaluacion = $this->bd->query("DELETE FROM evaluaciones WHERE id_contenido = " . $contenido['id'] );
							
							if( $borrarEvaluacion == false ) {
								echo 'Hubo un error con la base de datos:' . $this->bd->error();
							} // if( $borrarEvaluacion == false ) {
							
						} // foreach( $contenidos as $contenido ) {
						
					} // if( $contenidos == false ) {
					
					$borrarContenidos = $this->bd->query("DELETE FROM contenidos WHERE id_tema = " . $idTema );
		
					if( $borrarContenidos == false ) {
						echo 'Hubo un error con la base de datos:' . $this->bd->error();
					} else {
						
						$borrarTema = $this->bd->query("DELETE FROM tema WHERE id = " . $idTema );
						
					} // if( $borrarContenidos == false ) { ... else ...
					
				} // foreach( $resultados as $resultado ) {
				
			} // if( $temasMateria->num_rows == 0 ) {
			
		} // if( $temasMateria == false ) { ... else ...
		
		
		
	} // public function borrarContenido() {
	
} // class Materia {
?>