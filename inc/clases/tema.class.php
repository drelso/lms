<?php
// Clase Tema

class Tema {
	private $id;
	private $idMateria;
	private $nombre;
	private $orden;
	private $bd;
	
	function __construct($id) {
		require_once( __DIR__ . '/../db/bd.class.php' );
		$this->bd = new BD();
		
		$this->id = intval($id);
		
		// Consulta a tabla de temas
		$resultados = $this->bd->query("SELECT * FROM tema WHERE id = " . $this->id);
		
		if($resultados == false) {
			
			echo 'Hubo un error con la base de datos:' . $this->bd->error();
			
		} else {
			
			foreach($resultados as $resultado) {
				$this->idMateria	=	$resultado['id_materia'];
				$this->nombre		=	$resultado['nombre'];
				$this->orden		=	$resultado['orden'];
				break;
			} // foreach($resultados as $resultado) {
			
		} // if($resultados == false) { ... else ...
	} // function __construct($id) {
	
	
	public function getID() { return $this->id; }
	
	
	public function getIdMateria() { return $this->idMateria; }
	
	
	public function setIdMateria( $idMateria ) {
		$this->bd->query('UPDATE tema SET id_materia = ' . $this->bd->escapar($idMateria) . ' WHERE id = ' . $this->id);
		$this->idMateria = $idMateria;
	} // public function setIdMateria( $idMateria ) {
		
	
	public function getOrden() { return $this->orden; }
	
	
	public function setOrden( $orden ) {
		$this->bd->query('UPDATE tema SET orden = ' . intval($orden) . ' WHERE id = ' . $this->id);
		$this->orden = $orden;
	} // public function setOrden( $orden ) {
	
	
	public function getNombre() { return $this->nombre; }
	
	
	public function setNombre( $nombre ) {
		$this->bd->query('UPDATE tema SET nombre = ' . $this->bd->escapar($nombre) . ' WHERE id = ' . $this->id);
		$this->nombre = $nombre;
	} // public function setNombre($nombre) {
	
	
	public static function agregarTema( $idMateria, $orden, $nombre ) {
		
		require_once( __DIR__ . '/../db/bd.class.php' );
		$bd = new BD();
		
		// Consulta a tabla de temas
		$resultados = $bd->query("SELECT id FROM tema WHERE id_materia = " . $bd->escapar( $idMateria ) . " AND nombre = " . $bd->escapar( $nombre ) );
		
		if($resultados == false) {
			
			echo 'Hubo un error con la base de datos:' . $bd->error();
				
		} elseif( $resultados->num_rows == 0 ) {
			$bd->query('INSERT INTO tema (id_materia,nombre,orden) VALUES	(' . $bd->escapar($idMateria) . ',' . $bd->escapar( $nombre ) . ',' . intval( $orden ) . ');');
			
			return new Tema( $bd->idInsertado() );
		} else {
			foreach( $resultados as $resultado ) {
				
				$tema = new Tema( $resultado['id'] );
				
				$tema->setIdMateria( $idMateria );
				$tema->setOrden( $orden );
				$tema->setNombre( $nombre );
				
				return $tema;
				
			} // foreach( $resultados as $resultado ) {
		} // if($resultados == false) { ... elseif ... else ...
		
	} // public static function agregarTema() {
	
	
	public static function getContenidos( $idTema ) {
		
		require_once( __DIR__ . '/../db/bd.class.php' );
		$bd = new BD();
		
		// Consulta a tabla de contenidos
		$resultados = $bd->query("SELECT * FROM contenidos WHERE id_tema = " . $bd->escapar( $idTema ) );
		
		
		if($resultados == false) {
			
			echo 'Hubo un error con la base de datos:' . $bd->error();
				
		} elseif( $resultados->num_rows > 0 ) {
			$contenidos = array();
		
			require_once( 'contenido.class.php' );
			
			foreach( $resultados as $resultado ) {
				$contenido = new Contenido( $resultado['id'] );
				
				array_push( $contenidos, $contenido );
			} // foreach( $resultados as $resultado ) {
			
			return $contenidos;
			
		} // if($resultados == false) { ... elseif ...
		
		return array();
		
	} // public static function getContenidos( $idTema ) {
	
	
} // class Tema {


?>