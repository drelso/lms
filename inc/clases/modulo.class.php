<?php
// Clase Módulo

class Modulo {
	private $id;
	private $idMateria;
	private $orden;
	private $nombre;
	private $bd;
	
	function __construct($id) {
		require_once( __DIR__ . '/../db/bd.class.php' );
		$this->bd = new BD();
		
		$this->id = intval($id);
		
		// Consulta a tabla de módulos
		$resultados = $this->bd->query("SELECT * FROM modulos WHERE id = " . $this->id);
		
		if($resultados == false) {
			
			echo 'Hubo un error con la base de datos:' . $this->bd->error();
			
		} else {
			
			foreach($resultados as $resultado) {
				$this->idMateria	=	$resultado['id_materia'];
				$this->orden		=	$resultado['orden'];
				$this->nombre		=	$resultado['nombre'];
				break;
			} // foreach($resultados as $resultado) {
			
		} // if($resultados == false) { ... else ...
	} // function __construct($id) {
	
	
	public function getID() { return $this->id; }
	
	
	public function getIdMateria() { return $this->idMateria; }
	
	
	public function setIdMateria( $idMateria ) {
		$this->bd->query('UPDATE modulos SET id_materia = ' . $this->bd->escapar($idMateria) . ' WHERE id = ' . $this->id);
		$this->idMateria = $idMateria;
	} // public function setIdMateria( $idMateria ) {
		
	
	public function getOrden() { return $this->orden; }
	
	
	public function setOrden( $orden ) {
		$this->bd->query('UPDATE modulos SET orden = ' . intval($orden) . ' WHERE id = ' . $this->id);
		$this->orden = $orden;
	} // public function setOrden( $orden ) {
	
	
	public function getNombre() { return $this->nombre; }
	
	
	public function setNombre( $nombre ) {
		$this->bd->query('UPDATE modulos SET nombre = ' . $this->bd->escapar($nombre) . ' WHERE id = ' . $this->id);
		$this->nombre = $nombre;
	} // public function setNombre($nombre) {
	
	
	public static function agregarModulo( $idMateria, $orden, $nombre ) {
		
		require_once( __DIR__ . '/../db/bd.class.php' );
		$bd = new BD();
		
		// Consulta a tabla de módulos
		$resultados = $bd->query("SELECT id FROM modulos WHERE id_materia = " . $bd->escapar( $idMateria ) . " AND nombre = " . $bd->escapar( $nombre ) );
		
		if($resultados == false) {
			
			echo 'Hubo un error con la base de datos:' . $bd->error();
				
		} elseif( $resultados->num_rows == 0 ) {
			$bd->query('INSERT INTO modulos (id_materia,orden,nombre) VALUES	(' . $bd->escapar($idMateria) . ','. intval( $orden ) . ',' . $bd->escapar( $nombre ) . ');');
			
			return new Modulo( $bd->idInsertado() );
		} else {
			foreach( $resultados as $resultado ) {
				
				$modulo = new Modulo( $resultado['id'] );
				
				$modulo->setIdMateria( $idMateria );
				$modulo->setOrden( $orden );
				$modulo->setNombre( $nombre );
				
				return $modulo;
				
			} // foreach( $resultados as $resultado ) {
		} // if($resultados == false) { ... elseif ... else ...
		
	} // public static function agregarModulo() {
	
} // class Contenido {
?>