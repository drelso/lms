<?php
// Clase Contenido

class Contenido {
	protected $id;
	protected $idTema;
	protected $nombre;
	protected $informacion;
	protected $tipoDeAprendizaje;
	protected $tipoDeContenido;
	private $archivo;
	private $iframe;
	protected $orden;
	protected $bd;
	
	function __construct($id) {
		require_once( __DIR__ . '/../db/bd.class.php' );
		$this->bd = new BD();
		
		$this->id = intval($id);
		
		// Consulta a tabla de contenidos
		$resultados = $this->bd->query("SELECT * FROM contenidos WHERE id = " . $this->id);
		
		if($resultados == false) {
			
			echo 'Hubo un error con la base de datos:' . $this->bd->error();
			
		} else {
			
			require_once('modulo.class.php');
			require_once('tiposcontenido.class.php');
			
			foreach($resultados as $resultado) {
		
				$this->idTema				=	$resultado['id_tema'];
				$this->nombre				=	$resultado['nombre'];
				$this->informacion			=	$resultado['informacion'];
				$this->tipoDeAprendizaje	=	$resultado['tipo_de_aprendizaje'];
				$this->tipoDeContenido		=	new TipoDeContenido($resultado['tipo_de_contenido']);
				$this->archivo 				=	$resultado['archivo'];
				$this->iframe 				=	$resultado['iframe'];
				$this->orden				=	$resultado['orden'];
				break;
			} // foreach($resultados as $resultado) {
			
		} // if($resultados == false) { ... else ...
	} // function __construct($id) {
	
	
	public function getID() { return $this->id; }
	
	
	public function getIdTema() { return $this->idTema; }
	
	
	public function setIdTema( $idTema ) {
		$this->bd->query('UPDATE contenidos SET id_tema = ' . intval( $idTema ) . ' WHERE id = ' . $this->id);
		$this->idTema = $idTema;
	} // public function setIdTema( $idTema ) {
	
	
	public function getNombre() { return $this->nombre; }
	
	
	public function setNombre( $nombre ) {
		$this->bd->query('UPDATE contenidos SET nombre = ' . $this->bd->escapar( $nombre ) . ' WHERE id = ' . $this->id);
		$this->nombre = $nombre;
	} // public function setNombre( $nombre ) {
	
	
	public function getInformacion() { return $this->informacion; }
	
	
	public function setInformacion($informacion) {
		$this->bd->query('UPDATE contenidos SET informacion = ' . $this->bd->escapar($informacion) . ' WHERE id = ' . $this->id);
		$this->informacion = $informacion;
	} // public function setInformacion($informacion) {
	
	
	public function getTipoDeAprendizaje() { return $this->tipoDeAprendizaje; }
	
	
	public function setTipoDeAprendizaje($tipoDeAprendizaje) {
		$this->bd->query('UPDATE contenidos SET tipo_de_aprendizaje = ' . $this->bd->escapar($tipoDeAprendizaje) . ' WHERE id = ' . $this->id);
		$this->tipoDeAprendizaje = $tipoDeAprendizaje;
	} // public function setTipoDeAprendizaje($tipoDeAprendizaje) {
	
	
	public function getTipoDeContenido() { return $this->tipoDeContenido; }
	
	
	public function setTipoDeContenido($idTipoDeContenido) {
		$this->bd->query('UPDATE contenidos SET tipo_de_contenido = ' . $this->bd->escapar($idTipoDeContenido) . ' WHERE id = ' . $this->id);
		$this->tipoDeContenido		=	new TipoDeContenido($tipoDeAprendizaje);
	} // public function setTipoDeContenido($tipoDeContenido) {
	
	
	public function getArchivo() { return $this->archivo; }
	
	
	public function setArchivo( $archivo ) {
		$this->bd->query('UPDATE contenidos SET archivo = ' . $this->bd->escapar( $archivo ) . ' WHERE id = ' . $this->id);
		$this->archivo = $archivo;
	} // public function setArchivo( $archivo ) {
		
	
	public function getIFrame() { return $this->iframe; }
	
	
	public function setIFrame( $iframe ) {
		$this->bd->query('UPDATE contenidos SET iframe = ' . $this->bd->escapar( $iframe ) . ' WHERE id = ' . $this->id);
		$this->iframe = $iframe;
	} // public function setIFrame( $iframe ) {
	
	
	public function getOrden() { return $this->orden; }
	
	
	public function setOrden( $orden ) {
		$this->bd->query('UPDATE contenidos SET orden = ' . intval( $orden ) . ' WHERE id = ' . $this->id);
		$this->orden = $orden;
	} // public function setOrden( $orden ) {
	
	
	public static function agregarContenido( $idTema, $nombre, $informacion, $tipoDeAprendizaje, $tipoDeContenido, $orden, $archivo = NULL, $iframe = NULL ) {
		
		require_once( __DIR__ . '/../db/bd.class.php' );
		$bd = new BD();
		
		// Consulta a tabla de contenidos
		$resultados = $bd->query("SELECT id,id_tema FROM contenidos WHERE id_tema = " . $idTema . " AND nombre = " . $bd->escapar( $nombre ) );
		
		if($resultados == false) {
			
			echo 'Hubo un error con la base de datos:' . $bd->error();
				
		} elseif( $resultados->num_rows == 0 ) {
			
			$nombreArchivo = ( isset( $archivo ) ) ? $bd->escapar( $archivo ) : 'NULL';
			$stringIframe = ( isset( $iframe ) ) ? $bd->escapar( $iframe ) : 'NULL';
			
			$bd->query('INSERT INTO contenidos (id_tema,nombre,informacion,tipo_de_aprendizaje,tipo_de_contenido,orden,archivo,iframe) VALUES	(' . intval( $idTema ) . ',' . $bd->escapar( $nombre ) . ',' . $bd->escapar( $informacion ) . ',' . $bd->escapar( $tipoDeAprendizaje ) . ',' . $bd->escapar( $tipoDeContenido ) . ',' . intval( $orden ) . ',' . $nombreArchivo . ',' . $stringIframe . ');');
			
			return new Contenido( $bd->idInsertado() );
		} else {
			foreach( $resultados as $resultado ) {
				
				$contenido = new Contenido( $resultado['id'] );
				
				$contenido->setIdTema( $idTema );
				$contenido->setNombre( $nombre );
				$contenido->setInformacion( $informacion );
				$contenido->setTipoDeAprendizaje( $tipoDeAprendizaje );
				$contenido->setTipoDeContenido( $tipoDeContenido );
				$contenido->setOrden( $orden );
				
				return $contenido;
				
			} // foreach( $resultados as $resultado ) {
		} // if($resultados == false) { ... elseif ... else ...
	} // public static function agregarContenido( $modulo, $nombre, ...
	
	
} // class Contenido {
?>