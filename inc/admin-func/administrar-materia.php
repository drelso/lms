<?php
// Formulario de administración de materias
error_reporting(E_ALL);
ini_set('display_errors', 1);
if( isset( $nombreMateria ) && isset( $idMateria ) ) {
	
	if( !isset( $bd ) ) {
		require_once( __DIR__ . '/../db/bd.class.php');
		$bd = new BD();
	} // if( !isset($bd) ) {
	
	$dirUploads = BASEDIR . '/uploads/';
	
	// Select de tipos de contenido
	//
	$tiposDeContenido = '';
	$tipoEvaluacion = '';
	
	$resultados = $bd->query("SELECT * FROM tipo_contenido" );
	
	if( $resultados == false ) {
		echo 'Hubo un error con la base de datos:' . $this->bd->error();
	} else {
		
		if( $resultados->num_rows > 0 ) {
			
			// Formulario para modificar el nombre de la materia
			$tiposDeContenido	= '<select name="tipo-contenido">';
			$tipoEvaluacion		= '<select name="tipo-contenido">';
			
			$tiposDeContenido .= '<option value="0">Elija el tipo de contenido</option>';
			
			foreach( $resultados as $resultado ) {
				if( $resultado['nombre'] !== 'Evaluación' ) {
					
					$tiposDeContenido .= '<option value="' . $resultado['id'] . '">' . $resultado['nombre'] . '</option>';
					
				} else {
					$tipoEvaluacion .= '<option value="' . $resultado['id'] . '">' . $resultado['nombre'] . '</option>';
				} //if( $resultado['id'] !== 5 ...
			} // foreach($resultados as $resultado) {
			
			$tiposDeContenido	.= '</select> <!-- /tipo-contenido -->';
			$tipoEvaluacion		.= '</select> <!-- /tipo-contenido -->';
			
		} // if( $resultados->num_rows > 0 ) {
	} // if( $resultados == false ) {
	//
	// /Select de tipos de contenido
	
	
	// Select de tipos de contenido
	//
	function selectContenidos( $seleccionado = NULL ) {
		$tiposDeContenido = '';
		$tipoEvaluacion = '';
		
		require_once( __DIR__ . '/../db/bd.class.php');
		$bd = new BD();
		
		$resultados = $bd->query("SELECT * FROM tipo_contenido" );
		
		if( $resultados == false ) {
			echo 'Hubo un error con la base de datos:' . $this->bd->error();
		} else {
			
			if( $resultados->num_rows > 0 ) {
				
				// Formulario para modificar el nombre de la materia
				$tiposDeContenido	= '<select name="tipo-contenido">';
				$tipoEvaluacion		= '<select name="tipo-contenido">';
				
				$tiposDeContenido .= '<option value="0">Elija el tipo de contenido</option>';
				
				foreach( $resultados as $resultado ) {
					if( $resultado['nombre'] !== 'Evaluación' ) {
						
						$selected = ( $seleccionado == $resultado['id'] ) ? 'selected' : '';
						
						$tiposDeContenido .= '<option value="' . $resultado['id'] . '" ' . $selected . '>' . $resultado['nombre'] . '</option>';
						
					} else {
						$tipoEvaluacion .= '<option value="' . $resultado['id'] . '">' . $resultado['nombre'] . '</option>';
					} //if( $resultado['id'] !== 5 ...
				} // foreach($resultados as $resultado) {
				
				$tiposDeContenido	.= '</select> <!-- /tipo-contenido -->';
				$tipoEvaluacion		.= '</select> <!-- /tipo-contenido -->';
				
			} // if( $resultados->num_rows > 0 ) {
		} // if( $resultados == false ) {
		
		if( $seleccionado == 5 ) {
			return $tipoEvaluacion;
		} else {
			return $tiposDeContenido;
		} // if( $seleccionado == 5 ) {
		
	} // function selectContenidos( $seleccionado = NULL ) {
	
	
	// Select de departamentos
	//
	$departamentos = '';
	
	$resultados = $bd->query("SELECT * FROM departamento" );
	
	if( $resultados == false ) {
		echo 'Hubo un error con la base de datos:' . $this->bd->error();
	} else {
		
		if( $resultados->num_rows > 0 ) {
			
			// Formulario para asignar el departamento a la materia
			$departamentos = '<select name="departamento">';
			
			$departamentos .= '<option value="0">Elija un departamento</option>';
			
			foreach( $resultados as $resultado ) {
				if( $resultado['id'] !== '' ) {
					
					$seleccionado = ( $departamentoMateria == $resultado['id'] ) ? 'selected' : '';
					
					$departamentos .= '<option value="' . $resultado['id'] . '" ' . $seleccionado . '>' . $resultado['nombre'] . '</option>';
					
				} //if( $resultado['id'] !== '' ) {
			} // foreach($resultados as $resultado) {
			
			$departamentos .= '</select> <!-- /departamento -->';
			
		} // if( $resultados->num_rows > 0 ) {
	} // if( $resultados == false ) {
	
	
?>


    <div class="oculto">
        <div id="campo-agregar-tema">
            <fieldset class="tema">
                <input type="text" name="tema[]" placeholder="Tema" />
                
                <div class="contenidos"></div>
                
                <a class="agregar-contenido">Agregar contenido</a>
            	<a class="agregar-evaluacion">Agregar evaluación</a>
            </fieldset>
        </div> <!-- /campo-agregar-tema -->
        
        
        <div id="campo-agregar-contenido">
            <div class="contenido">
                <?php // $tiposDeContenido; ?>
                <?= selectContenidos(); ?>
                
                <input type="text" name="nombre-contenido" placeholder="Nombre del contenido"/>
                
                <input type="file" name="contenido" />
                
                <input type="text" name="iframe-contenido" placeholder="IFrame de contenido"/>
                
                <textarea name="contenido-texto" placeholder="Contenido en texto"></textarea>
                <a class="eliminar-contenido">&times;</a>
            </div> <!-- /contenido -->
        </div> <!-- /campo-agregar-contenido -->
        
        
        <div id="campo-agregar-evaluacion">
            <div class="contenido">
				<?php // $tipoEvaluacion; ?>
            	<?= selectContenidos(5); ?>
                
                <input type="text" name="nombre-contenido" placeholder="Nombre de la evaluación"/>
                
                <div class="preguntas">
                </div> <!-- /preguntas -->
                
                <a class="agregar-pregunta">Agregar pregunta</a>
                
                <a class="eliminar-contenido">&times;</a>
            </div> <!-- /contenido -->
        </div> <!-- /campo-agregar-evaluacion -->
        
        
        <div id="campo-agregar-pregunta">
            <div class="pregunta">
                
                <input type="text" name="pregunta" placeholder="Pregunta" />
                
                <input type="file" name="imagen-pregunta" />
                
                <div class="respuestas">
                </div> <!-- /respuestas -->
                
                <a class="agregar-respuesta">Agregar respuesta</a>
                
                <a class="eliminar-pregunta">&times;</a>
            </div> <!-- /pregunta -->
        </div> <!-- /campo-agregar-pregunta -->
        
        
        <div id="campo-agregar-respuesta">
            <fieldset class="respuesta">
            
                <input type="text" name="respuesta" placeholder="Respuesta" />
                
                <input type="file" name="imagen-respuesta" />
                
                <label><input type="checkbox" name="opcion-correcta" value="Opción correcta" />Opción correcta</label>
                
                <a class="eliminar-respuesta">&times;</a>
                
            </fieldset>
        </div> <!-- /campo-agregar-respuesta -->
    </div> <!-- /oculto -->


    <form action="actualizar-materia.php" method="post" id="agregar-tema" enctype="multipart/form-data">

        <input type="text" name="nombre-materia" value="<?= $nombreMateria; ?>" placeholder="Nombre de la materia" />
        
        <?= $departamentos; ?>

    	<div id="temas">
        
        	
            
            
            <?php
			require_once( __DIR__ . '/../clases/materia.class.php');
			
			$materia = new Materia( $idMateria );
			
			$temasMateria = $materia->getTemas();
			
			foreach( $temasMateria as $tema ) { ?>
			
                <fieldset class="tema">
                    <input type="text" name="tema[]" placeholder="Tema" value="<?= $tema->getNombre(); ?>">
                    
                    <div class="contenidos">
                    	
                        <?php
						require_once( __DIR__ . '/../clases/tema.class.php');
						
						$contenidos = Tema::getContenidos( $tema->getID() );
						
						foreach( $contenidos as $contenido ) { ?>
							
							<div class="contenido">
                            	<?php
								$tipoContenido = $contenido->getTipoDeContenido();
								echo selectContenidos( $tipoContenido->getID() ); ?>
								
                				<input type="text" name="nombre-contenido" placeholder="Nombre del contenido" value="<?= htmlentities( $contenido->getNombre() ); ?>">
                                
                                <?php
								// Revisa si el contenido es una
								// evaluación (la evaluación recibe el
								// ID 5
								//
                                if( $tipoContenido->getID() == 5 ) {
									require_once( __DIR__ . '/../clases/evaluacion.class.php');
									
									$evaluacion = new Evaluacion( $contenido->getID() );
									
									$reactivos = $evaluacion->getReactivos();
									?>
                                	<div class="preguntas">
                                    	<?php foreach( $reactivos as $reactivo ) { ?>
                                            <div class="pregunta">
                                            
                                                <input type="text" name="pregunta" placeholder="Pregunta" value="<?= htmlentities( $reactivo->getPregunta() ); ?>">
                                                
                                                <?php
                                                $imagenPregunta = $reactivo->getImagenPregunta();
                                                if( !empty($imagenPregunta) ) {
                                                	echo '<h4>Archivo actual:</h4>';
                                                	echo '<img src="' . $dirUploads . $reactivo->getImagenPregunta() . '"/>';
                                                	echo '<input type="hidden" name="imagen-actual" value="' . $reactivo->getImagenPregunta() . '"/>';
                                                } // if( !empty($respuesta[1]) ) {
                                                ?>
                                                
                								<input type="file" name="imagen-pregunta" />
                                                
                                                <div class="respuestas">
                                                
													<?php
                                                    $respuestas = $reactivo->getRespuestas();
													$correctas = $reactivo->getCorrectas();
													
													$indiceRespuesta = 0;
                                                    
                                                    foreach( $respuestas as $respuesta ) {
														
                                                    	if( !empty($respuesta[0]) || !empty($respuesta[1]) ) {
															
															$correcta = ( in_array($indiceRespuesta, $correctas ) ) ? 'checked' : '';
															?>
                                                            <fieldset class="respuesta">
                                                                <input type="text" name="respuesta" placeholder="Respuesta" value="<?= htmlentities( $respuesta[0] ); ?>">
                                                                
                                                                <?php
                                                                if( !empty($respuesta[1]) ) {
                                                                	echo '<h4>Archivo actual:</h4>';
                                                                	echo '<img src="' . $dirUploads . $respuesta[1] . '"/>';
                                                                	echo '<input type="hidden" name="imagen-respuesta-actual" value="' . $respuesta[1] . '"/>';
                                                                } // if( !empty($respuesta[1]) ) {
                                                                ?>
                                                                
												                <input type="file" name="imagen-respuesta" />
												                
                                                                <label><input type="checkbox" name="opcion-correcta" value="Opción correcta" <?= $correcta; ?>>Opción correcta</label>
                                                                <a class="eliminar-respuesta">×</a>
                                                            </fieldset> <!-- /respuesta -->
                                                    	<?php
														} // if( !empty($respuesta) ) {
														
														$indiceRespuesta++;
														
                                                    } // foreach( $respuestas as $respuesta ) {
													?>
                                                </div> <!-- /respuestas -->
                                                
                                                <a class="agregar-respuesta">Agregar respuesta</a>
                                                
                                                <a class="eliminar-pregunta">×</a>
                                                
                                            </div> <!-- /pregunta -->
                                        <?php } // foreach( $reactivos as $reactivo ) {  ?>
                                    </div> <!-- /preguntas -->
                                    
									<a class="agregar-pregunta">Agregar pregunta</a>
									
								<?php } else { ?>
									
									<h4>Archivo actual: <?= $contenido->getArchivo(); ?></h4>
									
									<input type="hidden" name="contenido-actual" value="<?= htmlentities( $contenido->getArchivo() ); ?>"/>
									
                                    <input type="file" name="contenido">
                                    
					                <input type="text" name="iframe-contenido" placeholder="IFrame de contenido" value="<?= htmlentities( $contenido->getIFrame() ); ?>"/>
					                
                                    <textarea name="contenido-texto" placeholder="Contenido en texto"><?= htmlentities( $contenido->getInformacion() ); ?></textarea>
                				<?php } // if( $tipoContenido->getID() == 5 ) { ... else ... ?>
                                <a class="eliminar-contenido">×</a>
                                
                			</div> <!-- /contenido -->
                			
						<?php } // foreach( $contenidos as $contenido ) { ?>
                        
                    </div> <!-- /contenidos -->
                    
                    <a class="agregar-contenido">Agregar contenido</a>
                    <a class="agregar-evaluacion">Agregar evaluación</a>
				</fieldset>
				
			<?php } // foreach( $temasMateria as $tema ) { ?>
            
            
            
        	<?php /*
            <fieldset class="tema">
                <input type="text" name="tema[]" placeholder="Tema" />
                
                <div class="contenidos"></div>
                
            	<a class="agregar-contenido">Agregar contenido</a>
            	<a class="agregar-evaluacion">Agregar evaluación</a>
            </fieldset>
		*/ ?>
        </div> <!-- /temas -->
        <a id="agregar-otro-tema">Agregar tema</a>
		
        <input type="submit" value="Actualizar" />
		<input type="hidden" name="id" value="<?= $idMateria; ?>" />
        <input type="hidden" name="agregar-tema-enviado" value="true" />
        <input type="hidden" name="MAX_FILE_SIZE" value="10000000" />
    </form> <!-- /agregar-tema -->
    
    <?php include_once( __DIR__ . '/../shadowbox.php' ); ?>
    
    <style>
	.contenido, .activo-evaluacion { display: block; background: gray; margin-bottom: 20px; }
	
	.sortable-placeholder { background: blue; margin-bottom: 20px; }
	
	.oculto { display: none; }
	</style>
    
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
    
	<script type="text/javascript" src="inc/js/jquery-1.11.3.min.js"></script>
    
	<script type="text/javascript" src="inc/js/jquery-ui.min.js"></script>
  	
	<!--<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>-->
  
    <script type="text/javascript" src="inc/js/temas.js"></script>
<?php
} else { echo 'No tienes permisos'; } // if( isset( $nombreMateria ) && isset( $idMateria ) ) {
?>