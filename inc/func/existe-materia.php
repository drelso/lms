<?php

error_reporting(E_ALL);
ini_set('display_errors', 1);


if( isset( $_POST['nombre'] ) ) {
	
	require_once('../db/bd.class.php');
	$bd = new BD();
	
	$whereIdDepto = ( $_POST['idDepartamento'] == '0') ? 'IS NULL' : '= ' . $bd->escapar( $_POST['idDepartamento'] );

	// Consulta a tabla de materia
	$existeMateria = $bd->query("SELECT * FROM materia WHERE nombre = " . $bd->escapar( $_POST['nombre'] ) . " AND id_departamento " . $whereIdDepto );
	
	if( $existeMateria == false ) {
		echo 'Hubo un error con la base de datos:' . $bd->error();
	} else {
		
		if( $existeMateria->num_rows == 0 ) {
			
			echo '0';
			
		} else {
			
			echo '1';
			
		} // if( $existeMateria->num_rows == 0 ) {
		
	} // if($existeMateria == false) { ... else ...
	
} // if( isset( $_POST['nombre'] ) ) {

?>