<?php
// Algoritmo de recomendación de recursos
// de aprendizaje a partir de los datos
// obtenidos en los estilos de aprendizaje


function recomendarRecurso( $arregloEstilos ) {
	if( !empty($arregloEstilos) ) {
		
		$ponderacion = array();
		
		$ponderacion['activo']['video']				= 1.0;
		$ponderacion['activo']['audio']				= 1.0;
		$ponderacion['activo']['narrativo']			= 0.0;
		$ponderacion['activo']['presentacion']		= 0.0;
		
		$ponderacion['reflexivo']['video']			= 0.0;
		$ponderacion['reflexivo']['audio']			= 0.0;
		$ponderacion['reflexivo']['narrativo']		= 1.0;
		$ponderacion['reflexivo']['presentacion']	= 1.0;
		
		$ponderacion['sensitivo']['video']			= 1.0;
		$ponderacion['sensitivo']['audio']			= 1.0;
		$ponderacion['sensitivo']['narrativo']		= 0.0;
		$ponderacion['sensitivo']['presentacion']	= 0.0;
		
		$ponderacion['intuitivo']['video']			= 0.0;
		$ponderacion['intuitivo']['audio']			= 0.0;
		$ponderacion['intuitivo']['narrativo']		= 1.0;
		$ponderacion['intuitivo']['presentacion']	= 1.0;
		
		$ponderacion['secuencial']['video']			= 1.0;
		$ponderacion['secuencial']['audio']			= 1.0;
		$ponderacion['secuencial']['narrativo']		= 0.0;
		$ponderacion['secuencial']['presentacion']	= 0.0;
		
		$ponderacion['global']['video']				= 0.0;
		$ponderacion['global']['audio']				= 0.0;
		$ponderacion['global']['narrativo']			= 1.0;
		$ponderacion['global']['presentacion']		= 1.0;
		
		$ponderacion['visual']['video']				= 1.0;
		$ponderacion['visual']['audio']				= 0.0;
		$ponderacion['visual']['narrativo']			= 0.0;
		$ponderacion['visual']['presentacion']		= 1.0;
		
		$ponderacion['verbal']['video']				= 0.0;
		$ponderacion['verbal']['audio']				= 1.0;
		$ponderacion['verbal']['narrativo']			= 1.0;
		$ponderacion['verbal']['presentacion']		= 0.0;
		
		
		$acumulado = array();
		
		$acumulado['video']			= 0;
		$acumulado['audio']			= 0;
		$acumulado['narrativo']		= 0;
		$acumulado['presentacion']	= 0;
		
		
		$acumulado['video'] += floatval( $arregloEstilos['activo'] ) * $ponderacion['activo']['video'];
		$acumulado['video'] += floatval( $arregloEstilos['reflexivo'] ) * $ponderacion['reflexivo']['video'];
		$acumulado['video'] += floatval( $arregloEstilos['sensitivo'] ) * $ponderacion['sensitivo']['video'];
		$acumulado['video'] += floatval( $arregloEstilos['intuitivo'] ) * $ponderacion['intuitivo']['video'];
		$acumulado['video'] += floatval( $arregloEstilos['secuencial'] ) * $ponderacion['secuencial']['video'];
		$acumulado['video'] += floatval( $arregloEstilos['global'] ) * $ponderacion['global']['video'];
		$acumulado['video'] += floatval( $arregloEstilos['visual'] ) * $ponderacion['visual']['video'];
		$acumulado['video'] += floatval( $arregloEstilos['verbal'] ) * $ponderacion['verbal']['video'];
		
		
		$acumulado['audio'] += floatval( $arregloEstilos['activo'] ) * $ponderacion['activo']['audio'];
		$acumulado['audio'] += floatval( $arregloEstilos['reflexivo'] ) * $ponderacion['reflexivo']['audio'];
		$acumulado['audio'] += floatval( $arregloEstilos['sensitivo'] ) * $ponderacion['sensitivo']['audio'];
		$acumulado['audio'] += floatval( $arregloEstilos['intuitivo'] ) * $ponderacion['intuitivo']['audio'];
		$acumulado['audio'] += floatval( $arregloEstilos['secuencial'] ) * $ponderacion['secuencial']['audio'];
		$acumulado['audio'] += floatval( $arregloEstilos['global'] ) * $ponderacion['global']['audio'];
		$acumulado['audio'] += floatval( $arregloEstilos['visual'] ) * $ponderacion['visual']['audio'];
		$acumulado['audio'] += floatval( $arregloEstilos['verbal'] ) * $ponderacion['verbal']['audio'];
		
		
		$acumulado['narrativo'] += floatval( $arregloEstilos['activo'] ) * $ponderacion['activo']['narrativo'];
		$acumulado['narrativo'] += floatval( $arregloEstilos['reflexivo'] ) * $ponderacion['reflexivo']['narrativo'];
		$acumulado['narrativo'] += floatval( $arregloEstilos['sensitivo'] ) * $ponderacion['sensitivo']['narrativo'];
		$acumulado['narrativo'] += floatval( $arregloEstilos['intuitivo'] ) * $ponderacion['intuitivo']['narrativo'];
		$acumulado['narrativo'] += floatval( $arregloEstilos['secuencial'] ) * $ponderacion['secuencial']['narrativo'];
		$acumulado['narrativo'] += floatval( $arregloEstilos['global'] ) * $ponderacion['global']['narrativo'];
		$acumulado['narrativo'] += floatval( $arregloEstilos['visual'] ) * $ponderacion['visual']['narrativo'];
		$acumulado['narrativo'] += floatval( $arregloEstilos['verbal'] ) * $ponderacion['verbal']['narrativo'];
		
		
		$acumulado['presentacion'] += floatval( $arregloEstilos['activo'] ) * $ponderacion['activo']['presentacion'];
		$acumulado['presentacion'] += floatval( $arregloEstilos['reflexivo'] ) * $ponderacion['reflexivo']['presentacion'];
		$acumulado['presentacion'] += floatval( $arregloEstilos['sensitivo'] ) * $ponderacion['sensitivo']['presentacion'];
		$acumulado['presentacion'] += floatval( $arregloEstilos['intuitivo'] ) * $ponderacion['intuitivo']['presentacion'];
		$acumulado['presentacion'] += floatval( $arregloEstilos['secuencial'] ) * $ponderacion['secuencial']['presentacion'];
		$acumulado['presentacion'] += floatval( $arregloEstilos['global'] ) * $ponderacion['global']['presentacion'];
		$acumulado['presentacion'] += floatval( $arregloEstilos['visual'] ) * $ponderacion['visual']['presentacion'];
		$acumulado['presentacion'] += floatval( $arregloEstilos['verbal'] ) * $ponderacion['verbal']['presentacion'];
		
		
		// Valor total obtenido
		$valorTotal = $acumulado['video'] + $acumulado['audio'] + $acumulado['narrativo'] + $acumulado['presentacion'];
		
		/*
		echo 'Ac Video ' . $acumulado['video'] . '<br>';
		echo 'Ac Audio ' . $acumulado['audio'] . '<br>';
		echo 'Ac Narrativo ' . $acumulado['narrativo'] . '<br>';
		echo 'Ac Presentacion ' . $acumulado['presentacion'] . '<br>';
		
		echo 'Valor total> ' . $valorTotal;
		*/
		
		
		// Arreglo de recomendaciones
		// Cada campo es un valor porcentual
		// de recomendación del recurso indicado
		$recomendacion = array();
		
		$recomendacion['video']			= $acumulado['video'] / $valorTotal;
		$recomendacion['audio']			= $acumulado['audio'] / $valorTotal;
		$recomendacion['narrativo']		= $acumulado['narrativo'] / $valorTotal;
		$recomendacion['presentacion']	= $acumulado['presentacion'] / $valorTotal;
		
		return $recomendacion;
		
	} else {
		return false;
	} // if( !empty($estilosDeAprendizaje) ) {
} // function recomendarRecurso( $arregloEstilos ) {

?>