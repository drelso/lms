<?php
// Función para imprimir la tabla de usuarios del grupo


function tablaUsuariosGrupo($idGrupo) {
	
	require_once( __DIR__ . '/../db/bd.class.php' );
	$bd = new BD();
	
	// Consulta a join de tabla de grupos de usuario y usuario
	$usuarios = $bd->query("SELECT *
								FROM usuario_grupo a, usuarios b
								WHERE a.id_usuario = b.id
								AND a.id_grupo = " . $idGrupo);
	
	$output = '<table>';
	$output .= '<thead>';
	$output .= '<tr>';
	$output .= '<th>Matrícula</th>';
	$output .= '<th>Nombre</th>';
	$output .= '<th>Correo</th>';
	$output .= '</tr>';
	$output .= '</thead>';
	
	$output .= '<tbody>';
	
	foreach($usuarios as $usuario) {
		
		$output .= '<tr>';
		
		$output .= '<td>' . $usuario['matricula'] . '</td>';
		$output .= '<td>' . $usuario['nombre'] . '</td>';
		$output .= '<td>' . $usuario['correo'] . '</td>';
		$output .= '<td><a class="eliminar-usuario-grupo">&times;</a></td>';
		
		$output .= '</tr>';
		
	} // foreach($usuarios as $usuario) {
	
	$output .= '</tbody>';
	$output .= '</table>';
	
	return $output;
} // function tablaUsuariosGrupo($idGrupo) {
?>