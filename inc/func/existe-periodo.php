<?php

error_reporting(E_ALL);
ini_set('display_errors', 1);

if( isset( $_POST['nombre'] )
	&& isset( $_POST['descripcion'] ) ) {
	
	require_once('../db/bd.class.php');
	$bd = new BD();
	
	// Consulta a tabla de periodos
	$existePeriodo = $bd->query("SELECT * FROM periodos WHERE nombre = " . $bd->escapar( $_POST['nombre'] ) . " AND descripcion = " . $bd->escapar( $_POST['descripcion'] ) );
	
	if( $existePeriodo == false ) {
		echo 'Hubo un error con la base de datos:' . $bd->error();
	} else {
		
		if( $existePeriodo->num_rows == 0 ) {
			
			echo '0';
			
		} else {
			
			echo '1';
			
		} // if( $existePeriodo->num_rows == 0 ) {
		
	} // if($existePeriodo == false) { ... else ...
	
} // if( isset( $_POST['nombre'] ) ) {

?>