<?php

error_reporting(E_ALL);
ini_set('display_errors', 1);


if( isset( $_POST['clave'] ) ) {
	
	require_once('../db/bd.class.php');
	$bd = new BD();
	
	// Consulta a tabla de departamento
	$existeDepartamento = $bd->query("SELECT * FROM departamento WHERE id = " . $bd->escapar( $_POST['clave'] ) . " AND nombre = " . $bd->escapar( $_POST['nombre'] ) . " AND director = " . $bd->escapar( $_POST['director'] ) );
	
	if( $existeDepartamento == false ) {
		echo 'Hubo un error con la base de datos:' . $bd->error();
	} else {
		
		if( $existeDepartamento->num_rows == 0 ) {
			
			echo '0';
			
		} else {
			
			echo '1';
			
		} // if( $existeDepartamento->num_rows == 0 ) {
		
	} // if($existeDepartamento == false) { ... else ...
	
} // if( isset( $_POST['clave'] ) ) {

?>