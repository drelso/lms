<?php

// Subir archivo por AJAX


error_reporting(E_ALL);
ini_set('display_errors', 1);

$tmpName = $_FILES['csv']['tmp_name'];
$nombre = $_FILES['csv']['name'];

$extension = pathinfo($nombre, PATHINFO_EXTENSION);

require_once( __DIR__ . '/../db/bd.class.php' );
$bd = new BD();


echo 'Archivo ' . $extension . '<br><br>';

if( $extension == 'csv' ) {
    
    $resultado = file_get_contents_utf8($tmpName);
    
    $output = '<table>';
    $output .= '<thead>';
    $output .= '<tr>';
    
    $output .= '<th>Matrícula</th>';
    
    $output .= '<th>Nombre</th>';
    $output .= '<th>Correo</th>';
    
    $output .= '<th>Activo - Reflexivo</th>';
    $output .= '<th>Sensitivo - Intuitivo</th>';
    $output .= '<th>Visual - Verbal</th>';
    $output .= '<th>Secuencial - Global</th>';
    
    $output .= '<th>Verbal</th>';
    $output .= '<th>Lógico matemático</th>';
    $output .= '<th>Espacial</th>';
    $output .= '<th>Musical</th>';
    $output .= '<th>Kinestésico</th>';
    $output .= '<th>Intrapersonal</th>';
    $output .= '<th>Interpersonal</th>';
    $output .= '<th>Naturalista</th>';
    
    $output .= '<th>Nuevo usuario</th>';
    $output .= '<th>Eliminar</th>';
    
    $output .= '<th></th>';
    $output .= '</tr>';
    $output .= '</thead>';
    $output .= '<tbody>';
    
    foreach( $resultado as $registro ) {
        
        if( !empty( $registro[0] ) ) {
        
            $output .= '<tr>';
            
            $numColumna = 1;
            
            $nuevoEstudiante = true;
            
            $nombreEstudiante = '';
            
            foreach( $registro as $columna ) {
                
                if( $numColumna == 1 ) {
                    
                    $existeEstudiante = $bd->query("SELECT * FROM usuarios WHERE matricula = " . $bd->escapar( $columna ) );
                    
                    
                    if( $existeEstudiante == false ) {
                        echo 'Hubo un error con la base de datos:' . $bd->error();
                    } else {
                        
                        if( $existeEstudiante->num_rows > 0 ) {
                            
                            $nuevoEstudiante = false;
                            
                            foreach( $existeEstudiante as $estudiante ) {
                                
                                $output .= '<td><input name="matricula" placeholder="Matrícula" value="' . $estudiante['matricula'] . '" disabled/></td>';
                                $output .= '<td><input name="nombre" placeholder="Nombre" value="' . $estudiante['nombre'] . '" disabled/></td>';
                                $output .= '<td><input name="correo" placeholder="Correo" value="' . $estudiante['correo'] . '" disabled/></td>';
                                
                                $output .= '<td class="usuario-nuevo">No</td>';
                                $output .= '<td><span class="eliminar-usuario-grupo">&times;</span></td>';
                                
                                $output .= '<td class="error-datos-csv"></td>';
                                
                                break;
                                
                            } // foreach( $existeEstudiante as $estudiante ) {
                        } // if( count( $existeEstudiante ) > 0 ) {
                        
                    } // if( $existeEstudiante == false ) { ... else ...
                    
                } // if( $numColumna == 1 ) { ... else ...
                
                $mensajeError = '';
                
                if( $nuevoEstudiante ) {
                    
                    switch( $numColumna ) {
                        
                        case 1:
                            // Matrícula
                            if( strlen( $columna ) > 10 ) {
                                $mensajeError .= 'La matrícula debe contener 10 caracteres o menos<br>';
                            } // if( strlen( $columna ) > 10 ) {
                            
                            if( empty( $columna ) ) {
                                $mensajeError .= 'Debe ingresar una matrícula para el usuario<br>';
                            } // if( strlen( $columna ) > 10 ) {
                            
                            $output .= '<td><input name="matricula" placeholder="Matrícula" value="' . $columna . '"/></td>';
                            break;
                        
                        case 2:
                            // Nombre
                            $nombreEstudiante = $columna;
                            
                            if( strlen( $nombreEstudiante ) > 255 ) {
                                $mensajeError .= 'El nombre debe contener 255 caracteres o menos<br>';
                            } // if( strlen( $nombreEstudiante ) > 255 ) {
                            
                            if( empty( $nombreEstudiante ) ) {
                                $mensajeError .= 'Debe ingresar un nombre para el usuario<br>';
                            } // if( empty( $nombreEstudiante ) ) {
                            
                            break;
                            
                        case 3:
                            // Apellido paterno
                            $nombreEstudiante .= $columna;
                            
                            if( strlen( $nombreEstudiante ) > 255 ) {
                                $mensajeError .= 'El nombre debe contener 255 caracteres o menos<br>';
                            } // if( strlen( $nombreEstudiante ) > 255 ) {
                            
                            if( empty( $nombreEstudiante ) ) {
                                $mensajeError .= 'Debe ingresar un nombre para el usuario<br>';
                            } // if( empty( $nombreEstudiante ) ) {
                            
                            break;
                            
                        case 4:
                            // Apellido materno
                            $nombreEstudiante .= $columna;
                            
                            if( strlen( $nombreEstudiante ) > 255 ) {
                                $mensajeError .= 'El nombre debe contener 255 caracteres o menos<br>';
                            } // if( strlen( $nombreEstudiante ) > 255 ) {
                            
                            if( empty( $nombreEstudiante ) ) {
                                $mensajeError .= 'Debe ingresar un nombre para el usuario<br>';
                            } // if( empty( $nombreEstudiante ) ) {
                            
                            
                            $output .= '<td><input name="nombre" placeholder="Nombre" value="' . $nombreEstudiante . '"/></td>';
                            
                            break;
                        
                        case 5:
                            // Correo
                            if( strlen( $columna ) > 255 ) {
                                $mensajeError .= 'El correo debe contener 255 caracteres o menos<br>';
                            } // if( strlen( $columna ) > 10 ) {
                            
                            if( empty( $columna ) ) {
                                $mensajeError .= 'Debe ingresar un correo para el usuario<br>';
                            } // if( empty( $columna ) ) {
                            
                            
                            $output .= '<td><input name="correo" placeholder="Correo" value="' . $columna . '"/></td>';
                            break;
                            
                        case 6:
                            // Activo - Reflexivo
                            $output .= '<td><input name="activo-reflexivo" placeholder="Activo - Reflexivo" value="' . $columna . '"/></td>';
                            break;
                            
                        case 7:
                            // Sensitivo - Intuitivo
                            $output .= '<td><input name="sensitivo-intuitivo" placeholder="Sensitivo - Intuitivo" value="' . $columna . '"/></td>';
                            break;
                            
                        case 8:
                            // Visual - Verbal
                            $output .= '<td><input name="visual-verbal" placeholder="Visual - Verbal" value="' . $columna . '"/></td>';
                            break;
                            
                        case 9:
                            // Secuencial - Global
                            $output .= '<td><input name="secuencial-global" placeholder="Secuencial - Global" value="' . $columna . '"/></td>';
                            break;
                            
                        case 10:
                            // Verbal
                            $output .= '<td><input name="verbal" placeholder="Verbal" value="' . $columna . '"/></td>';
                            break;
                            
                        case 11:
                            // Lógico matemático
                            $output .= '<td><input name="logico-matematico" placeholder="Lógico matemático" value="' . $columna . '"/></td>';
                            break;
                            
                        case 12:
                            // Espacial
                            $output .= '<td><input name="espacial" placeholder="Espacial" value="' . $columna . '"/></td>';
                            break;
                            
                        case 13:
                            // Musical
                            $output .= '<td><input name="musical" placeholder="Musical" value="' . $columna . '"/></td>';
                            break;
                            
                        case 14:
                            // Kinestésico
                            $output .= '<td><input name="kinestesico" placeholder="Kinestésico" value="' . $columna . '"/></td>';
                            break;
                            
                        case 15:
                            // Intrapersonal
                            $output .= '<td><input name="intrapersonal" placeholder="Intrapersonal" value="' . $columna . '"/></td>';
                            break;
                            
                        case 16:
                            // Interpersonal
                            $output .= '<td><input name="interpersonal" placeholder="Interpersonal" value="' . $columna . '"/></td>';
                            break;
                            
                        case 17:
                            // Naturalista
                            $output .= '<td><input name="naturalista" placeholder="Naturalista" value="' . $columna . '"/></td>';
                            break;
                            
                    } // switch( $numColumna ) {
                    
                   // $output .= '<td>' . $columna . '</td>';
                } // if( $nuevoEstudiante ) {
                
                $numColumna++;
                
            } // foreach( $registro as $columna ) {
            
            if( $nuevoEstudiante ) {
                $output .= '<td class="usuario-nuevo">Sí</td>';
                $output .= '<td class="eliminar-usuario-grupo">&times;</td>';
                $output .= '<td  class="error-datos-csv"></td>';
            } // if( $nuevoEstudiante ) {
            
            $output .= '</tr>';
            
        } // if( !empty( $registro[0] ) ) {
        
    } // foreach( $resultado as $registro ) {
    
    $output .= '</tbody>';
    
    echo $output;
    
} else {
    echo 'Error: el archivo debe ser .csv';
} // if( $extension == 'csv' ) {


function file_get_contents_utf8($fn) {
    
    $contenido = file_get_contents($fn);
    $datos = mb_convert_encoding($contenido, 'UTF-8',
          mb_detect_encoding($contenido, 'UTF-8, ISO-8859-1', true));
    
    $lineas = explode(PHP_EOL, $datos);
    $arreglo = array();
    
    foreach ($lineas as $linea) {
        $arreglo[] = str_getcsv($linea);
    } // foreach ($lineas as $linea) {
    
    return $arreglo;
    
} // function file_get_contents_utf8($fn) {
    
?>