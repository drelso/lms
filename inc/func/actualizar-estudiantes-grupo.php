<?php
// Actualiza los alumnos del grupo

ini_set("display_errors", "1");
error_reporting(E_ALL);

if( isset($_POST['idGrupo']) && isset($_POST['estudiantes']) ) {
	
	require_once( __DIR__ . '/../db/bd.class.php' );
	$bd = new BD();
	
	require_once( __DIR__ . '/../clases/administrador.class.php' );
	$administrador = new Administrador(3);
	
	require_once( __DIR__ . '/../clases/grupo.class.php' );
	$grupo = new Grupo( $_POST['idGrupo'] );
	
	$idsEstudiantes = array();
	
	$estudiantes = json_decode( $_POST['estudiantes'] );
	
	foreach( $estudiantes as $estudiante ) {
		
		$existeEstudiante = $bd->query("SELECT id FROM usuarios WHERE matricula = " . $bd->escapar( $estudiante->matricula ) );
		
		if( $existeEstudiante == false ) {
            echo 'Hubo un error con la base de datos:' . $bd->error();
        } else {
            
            if( $existeEstudiante->num_rows > 0 ) {
                
                foreach( $existeEstudiante as $estudianteExistente ) {
                    
                    array_push($idsEstudiantes, $estudianteExistente['id']);
                    
                    break;
                    
                } // foreach( $existeEstudiante as $estudianteExistente ) {
            } else {
            	
            	$activo = calcularValorEstilo( $estudiante->actRef, false );
            	$reflexivo = calcularValorEstilo( $estudiante->actRef, true );
            	
            	$sensitivo = calcularValorEstilo( $estudiante->sensInt, false );
            	$intuitivo = calcularValorEstilo( $estudiante->sensInt, true );
            	
            	$visual = calcularValorEstilo( $estudiante->visVerb, false );
            	$verbal = calcularValorEstilo( $estudiante->visVerb, true );
            	
            	$secuencial = calcularValorEstilo( $estudiante->secGlob, false );
            	$global = calcularValorEstilo( $estudiante->secGlob, true );
            	
            	$intVerbal 	= $estudiante->verbal; 
				$logMat 	= $estudiante->logMat;
				$espacial 	= $estudiante->espacial;
				$musical 	= $estudiante->musical;
				$kines 		= $estudiante->kines;
				$intrap 	= $estudiante->intrap;
				$interp 	= $estudiante->interp;
				$natur 		= $estudiante->natur;
            	
            	
            	
            	$nuevoEstudiante = $administrador->agregarUsuario($estudiante->nombre, $estudiante->correo, 'temp', $estudiante->matricula, '', 0, array(3));
            	
            	if( !empty( $nuevoEstudiante ) ) {
            		
            		// Inserta los valores de estilos de aprendizaje del estudiante
            		$estilosDeAprendizaje = $bd->query("INSERT INTO estilos_de_aprendizaje (id_usuario, activo, reflexivo, sensitivo, intuitivo, visual, verbal, secuencial, global) VALUES (" . $bd->escapar(  $nuevoEstudiante->getID() ) . ", " . $activo . ", " . $reflexivo . ", " . $sensitivo . ", " . $intuitivo . ", " . $visual . ", " . $verbal . ", " . $secuencial . ", " . $global . ")" );
            		
            		
					if( $estilosDeAprendizaje == false ) {
			            echo 'Hubo un error con la base de datos:' . $bd->error();
			        } // if( $estilosDeAprendizaje == false ) {
			        
            		// Inserta los valores de inteligencias múltiples del estudiante
            		$inteligenciasMultiples = $bd->query("INSERT INTO inteligencias_multiples (id_usuario, verbal, logico_matematica, espacial, musical, kinestesica, intrapersonal, interpersonal, naturalista) VALUES (" . $bd->escapar(  $nuevoEstudiante->getID() ) . ", " . $intVerbal . ", " . $logMat . ", " . $espacial . ", " . $musical . ", " . $kines . ", " . $intrap . ", " . $interp . ", " . $natur . ")" );
            		
					if( $inteligenciasMultiples == false ) {
			            echo 'Hubo un error con la base de datos:' . $bd->error();
			        } // if( $estilosDeAprendizaje == false ) {
			        
            		array_push($idsEstudiantes, $nuevoEstudiante->getID());
            	} // if( !empty( $nuevoEstudiante ) ) {
            	
            } // if( $existeEstudiante->num_rows > 0 ) { ... else ...
            
            
        } // if( $existeEstudiante == false ) { ... else ...
		
	} // foreach( $data as $estudiante ) {
	
	$grupo->setEstudiantes($idsEstudiantes);
	
	
	require_once( __DIR__ . '/../func/tabla-usuarios-grupo.func.php' );
	
	echo tablaUsuariosGrupo($_POST['idGrupo']);
	
	echo '<h3 class="mensaje-exito">Grupo actualizado exitosamente</h3>';
	
} else {
	header('Location: ' . $_SERVER['HTTP_REFERER']);
} // if( !empty( $data ) ) { ... else ...


function calcularValorEstilo( $valor, $positivo = true ) {
	
	$resultado = floatval( $valor ) / 11;
	
	if( $positivo ) {
		if( $resultado < 0 ) { $resultado = 0; }
	} else {
		if( $resultado > 0 ) { $resultado = 0; }
		else { $resultado *= -1; }
	} // if( $positivo ) { ... else ...
	
	return $resultado;
	
} // function calcularValorEstilo() {

?>