<?php
header("Content-type: text/css");

$principal 				= ( isset($_GET['p']) ) ? '#'.$_GET['p'] : "#000";
$complementario 		= ( isset($_GET['c']) ) ? '#'.$_GET['c'] : "#FFF";
$complementarioHover	= ( isset($_GET['ch']) ) ? '#'.$_GET['ch'] : "#f2f2f2";
$medio 					= ( isset($_GET['m']) ) ? '#'.$_GET['m'] : "#8c8c8c";
$secundario 			= ( isset($_GET['s']) ) ? '#'.$_GET['s'] : "#4d4d4d";


?>


/*
Estilos del módulo de aprendizaje adaptativo
*/



			/*  CSS RESET  */
				  /**/
				  /**/
				  /**/
html, body, div, span, applet, object, iframe,
h1, h2, h3, h4, h5, h6, p, blockquote, pre,
a, abbr, acronym, address, big, cite, code,
del, dfn, em, img, ins, kbd, q, s, samp,
small, strike, strong, sub, sup, tt, var,
b, u, i, center,
dl, dt, dd, ol, ul, li,
fieldset, form, label, legend,
table, caption, tbody, tfoot, thead, tr, th, td,
article, aside, canvas, details, embed, 
figure, figcaption, footer, header, hgroup, 
menu, nav, output, ruby, section, summary,
time, mark, audio, video {
	margin: 0;
	padding: 0;
	border: 0;
	font-size: 100%;
	font: inherit;
	vertical-align: baseline;
}

a {
	text-decoration:none;
}


a:hover,
a:focus,
a:active, 
input:hover,
input:focus,
input:active{
	outline:none;
}

/* HTML5 display-role reset for older browsers */
article, aside, details, figcaption, figure, 
footer, header, hgroup, menu, nav, section {
	display: block;
}

body { line-height: 1; }

ol, ul { list-style: none; }

blockquote, q { quotes: none; }

blockquote:before, blockquote:after,
q:before, q:after {
	content: '';
	content: none;
}

table {
	border-collapse: collapse;
	border-spacing: 0;
}

::selection {
	background-color: <?= $secundario; ?>;
	color: <?= $complementario; ?>;
}

::-moz-selection {
	background-color: <?= $secundario; ?>;
	color: <?= $complementario; ?>;
}


										/* Tipografía y textos */
html * {
	font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
	font-weight: normal;
	font-style: normal;
	font-variant: normal;
	color: <?= $secundario; ?>;
}

h1,
h2,
h3,
h4,
h5,
h6 {
	font-weight: bold;
	line-height: 120%;
}

h1 {
	font-size: 20px;
	text-transform: uppercase;
}

h2 {
	font-size: 18px;
	text-transform: uppercase;
}

h3 { font-size: 16px; }

h4 { font-size: 14px; }

h5 { font-size: 12px; }

h6 { font-size: 10px; }

p {
	font-size: 12px;
	line-height: 20px;
}


										/* Generales */
body {
	/*padding: 30px;*/
}

form:after {
	visibility: hidden;
	display: block;
	content: "";
	clear: both;
	height: 0;
}

* html form             { zoom: 1; } /* IE6 */
*:first-child+html form { zoom: 1; } /* IE7 */

input,
select,
textarea {
	position: relative;
	width: 100%;
	margin-bottom: 10px;
	padding: 10px 0;
	outline: none;
	border: none;
	border-bottom: 2px solid <?= $secundario; ?>;
}

textarea {
	height: 50px;
	border-right: 2px solid <?= $secundario; ?>;
	resize: vertical;
}

input[type="checkbox"],
input[type="radio"] {
	display: inline-block;
	width: auto;
	margin: 0 10px 0 0;
}

label {
	-webkit-box-sizing: border-box;
	-moz-box-sizing: border-box;
	box-sizing: border-box;
	display: block;
	width: 100%;
	padding: 7px;
	cursor: pointer;
}

label:hover {
	background-color: <?= $medio; ?>;
	color: <?= $complementario; ?>;
}

input[type="submit"],
form button {
	float: right;
	width: 60px;
	border: none;
	cursor: pointer;
}

input[type="submit"]:hover,
form button:hover {
	background-color: <?= $secundario; ?>;
	color: <?= $complementario; ?>;
}

.inactivo { cursor: not-allowed !important; }


.error {
	display: inline-block;
	padding: 0 3px;
	background-color: #800000;
	line-height: 26px;
	color: <?= $complementario; ?>;
	clear: both;
}

										/* Afters para justificar */
.panel-admin .formulario-agregar-usuario:after,
#resultado-color:after,
#menu-materia:after,
.contenidos:after,
.preguntas:after {
	content: '';
	display: inline-block;
	width: 100%;
	height: 0;
}



										/* Encabezado */
.encabezado-materia {
	-webkit-box-sizing: border-box;
	-moz-box-sizing: border-box;
	box-sizing: border-box;
	position: relative;
	width: 100%;
	padding: 0 0 0 30px;
	background-color: <?= $principal; ?>;
	text-align: justify;
	white-space: nowrap;
}

.encabezado-materia h1,
#menu-materia,
#menu-materia li {
	-webkit-box-sizing: border-box;
	-moz-box-sizing: border-box;
	box-sizing: border-box;
	display: inline-block;
	position: relative;
	width: auto;
	vertical-align: middle;
	white-space: normal;
}

.encabezado-materia h1 {
	width: 38%;
	height: 50px;
	line-height: 50px;
	color: <?= $complementario; ?>;
	padding-right: 20px;
	font-size: 18px;
	text-align: left;
}

#menu-materia {
	width: 61%;
	height: 50px;
	text-align: justify;
	padding-left: 20px;
	font-size: 0;
}

#menu-materia li {
	/*max-width: 15%;*/
	width: 24%;
	height: 100%;
	text-align: center;
	color: <?= $complementarioHover; ?>;
	font-size: 12px;
	line-height: 120%;
	padding: 10px 0;
}

#menu-materia li hr {
	/*display: inline-block;*/
	display: none;
    margin: 2px 0;
    width: 100%;
    border-left: none;
    border-right: none;
}

#menu-materia .visitado { background-color: <?= $secundario; ?>; }

#menu-materia .activo { background-color: #800000; }


										/* Principal */
.main {
	-webkit-box-sizing: border-box;
	-moz-box-sizing: border-box;
	box-sizing: border-box;
	position: relative;
	width: 100%;
	padding: 15px 30px;
}


										/* Lista grupos */
.grupos {
	padding: 30px 0;
}

.grupos li a {
	display: block;
	padding: 15px 3px;
	border-bottom: 2px solid <?= $secundario; ?>;
}

.grupos li h4 { font-weight: normal; }

.grupos li a:hover {
	background-color: <?= $secundario; ?>;
	border-color: #800000;
}

.grupos li a:hover * { color: <?= $complementario; ?>; }


										/* Pasos materia */
.paso-materia { display: none; }

#paso-1 { display: block; }

#encabezado-general {
	display: none;
}

.titulo-tema {
    font-size: 22px;
    letter-spacing: 2px;
    margin: 20px 0 40px;
}

#agradecimiento h2 {
	margin: 50px 0;
	text-align: center;
	font-size: 26px;
	letter-spacing: 2px;
	color: <?= $secundario; ?>;
}


										/* Contenidos */

.contenidos,
.preguntas {
	text-align: justify;
}

.contenido,
.pregunta {
	-webkit-box-sizing: border-box;
	-moz-box-sizing: border-box;
	box-sizing: border-box;
	display: inline-block;
	position: relative;
	width: 48%;
	background-color: <?= $secundario; ?>;
	padding: 20px;
	margin-bottom: 30px;
	vertical-align: top;
	cursor: pointer;
}

.seleccionable:hover { background-color: <?= $medio; ?>; }

.contenido *,
.pregunta * { color: <?= $complementario; ?>; }

.info-contenido { display: none; }

.contenido p {
	font-size: 14px;
	padding: 30px 0 15px;
}

.contenido iframe,
.contenido embed,
.contenido img,
.contenido audio,
.contenido video {
	-webkit-box-sizing: border-box;
	-moz-box-sizing: border-box;
	box-sizing: border-box;
	position: relative;
	width: 100%;
	height: 500px;
	border: 2px solid <?= $complementario; ?>;
}

.contenido audio {
	height: 30px;
	border: none;
}

.contenido img { height: auto; }

#boton-finalizar,
.siguiente-paso,
.enviar-evaluacion {
	display: block;
	color: <?= $complementario; ?>;
	font-size: 17px;
	padding: 16px 31px;
	margin: 30px 0;
	text-decoration: none;
	background-color: <?= $secundario; ?>;
	border: none;
	outline: none;
	cursor: pointer;
}

#boton-finalizar:hover,
.siguiente-paso:hover,
.enviar-evaluacion:hover { background-color: #800000; }


										/* Evaluación */
.pregunta h3 { margin-bottom: 20px; }

.respuestas label {
	border-bottom: 2px solid <?= $secundario; ?>;
	text-align: left;
}

.pregunta img {
	display: block;
	position: relative;
	width: 100%;
	height: auto;
	margin: 20px 0 30px;
}

label.respuesta-seleccionada,
label.respuesta-seleccionada:hover {
	-webkit-box-sizing: border-box;
	-moz-box-sizing: border-box;
	box-sizing: border-box;
	background: <?= $complementarioHover; ?>;
	color: <?= $principal; ?>;
}

.respuestas textarea {
	background-color: transparent;
	border-color: <?= $complementario; ?>;
	font-size: 14px;
}

.resultados-evaluacion { display: none; }

.resultados-evaluacion h2 span {
	font-weight: bold;
	font-size: 30px;
	color: <?= $secundario; ?>;
}