<?php

echo 'Subida de archivo';

var_dump($_FILES['archivo']);

//if they DID upload a file...
if($_FILES['archivo']['name'])
{
	//if no errors...
	if(!$_FILES['archivo']['error'])
	{
		$valid_file = true;

		//now is the time to modify the future file name and validate the file
		$new_file_name = strtolower($_FILES['archivo']['tmp_name']); //rename file
		if($_FILES['archivo']['size'] > (1024000)) //can't be larger than 1 MB
		{
			$valid_file = false;
			$message = 'Oops!  Your file\'s size is to large.';

			echo $message;
		}
		
		//if the file has passed the test
		if($valid_file)
		{
			//move it to where we want it to be
			move_uploaded_file($_FILES['archivo']['tmp_name'], 'uploads/'.$_FILES['archivo']['name']);
			$message = 'Congratulations!  Your file was accepted.';

			echo $message;
		}
	}
	//if there is an error...
	else
	{
		//set that to be the returned message
		$message = 'Ooops!  Your upload triggered the following error:  '.$_FILES['archivo']['error'];

		echo $message;
	}
}

?>