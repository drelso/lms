<?php
// Interfaz de materia

session_start();

ini_set("display_errors", "1");
error_reporting(E_ALL);

echo 'Grupo 1';


$id = 0;
$usuario;

// Si el usuario está registrado
// instancia un objeto de clase usuario
//
if(isset($_SESSION['usuario_registrado'])) {
	require_once('inc/clases/usuario.class.php');
	
	$id = intval($_SESSION['usuario_registrado']);
	
	if($id > 0) {
		$usuario = new Usuario($id);
	} // if($id > 0) {
	
} // if(isset($_SESSION['usuario_registrado'])) {

// Si existe el objeto de clase usuario
// genera la estructura del perfil
//
if(isset($usuario) && !empty($usuario)
		&& isset($_GET['id'])) {
	
	require_once('inc/db/bd.class.php');
	$bd = new BD();
	
	$usuarioEnGrupo = $bd->query( "SELECT * FROM usuario_grupo WHERE id_usuario = " . $usuario->getID() . " AND id_grupo = " . $_GET['id'] );
	
	if( $usuarioEnGrupo == false ) {
		echo 'Hubo un error con la base de datos:' . $bd->error();
	} else {
	
		if( $usuarioEnGrupo->num_rows == 0 ) {
			
		} else {
			
			echo BASEDIR;
			//header("Location: " . BASEDIR);
			//die();
		} // if( $usuarioEnGrupo->num_rows == 0 ) { ... else ...
	} // if( $usuarioEnGrupo == false ) {
} // if(isset($usuario) && !empty($usuario) ...
?>