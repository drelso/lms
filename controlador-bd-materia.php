<?php
// Ingresar datos de materia a base de datos


// Checa si la sesión ya existe
if(session_id() == '' || !isset($_SESSION)) {
    session_start();
}



ini_set("display_errors", "1");
error_reporting(E_ALL);

$usuario;

// Si el usuario está registrado
// instancia un objeto de clase usuario
//
if(isset($_SESSION['usuario_registrado'])) {
	require_once('inc/clases/usuario.class.php');
	
	$id = intval($_SESSION['usuario_registrado']);
	
	if($id > 0) {
		$usuario = new Usuario($id);
	} // if($id > 0) {
	
} // if(isset($_SESSION['usuario_registrado'])) {


// Si existe el objeto de clase usuario
// genera la estructura del perfil
//
if(isset($usuario) && !empty($usuario)) {
	
	require_once('inc/db/bd.class.php');
	$bd = new BD();
	
	

	
	// Ingresar o actualizar las preferencias
	// de paleta de color y estructura gráfica
	// del usuario
	//
	if( isset($_POST['preferencias']) ) {
		
		$preferenciasExistentes = $bd->query( "SELECT id_usuario FROM preferencias WHERE id_usuario = " . $usuario->getID() );
		
		
		if( $preferenciasExistentes == false ) {
			echo 'Hubo un error con la base de datos:' . $bd->error();
		} else {
			
			$estructura = ( isset($_POST['estructura']) ) ? $_POST['estructura'] : 'NULL';
			$idcolores = ( isset($_POST['idcolores']) ) ? $_POST['idcolores'] : 'NULL';
			
			$primarioa = ( isset($_POST['primarioa']) ) ? $_POST['primarioa'] : 'NULL';
			$primariob = ( isset($_POST['primariob']) ) ? $_POST['primariob'] : 'NULL';
			$primarioc = ( isset($_POST['primarioc']) ) ? $_POST['primarioc'] : 'NULL';
			$primariod = ( isset($_POST['primariod']) ) ? $_POST['primariod'] : 'NULL';
			$primarioe = ( isset($_POST['primarioe']) ) ? $_POST['primarioe'] : 'NULL';
			
			$secundario1a = ( isset($_POST['secundario1a']) ) ? $_POST['secundario1a'] : 'NULL';
			$secundario1b = ( isset($_POST['secundario1b']) ) ? $_POST['secundario1b'] : 'NULL';
			$secundario1c = ( isset($_POST['secundario1c']) ) ? $_POST['secundario1c'] : 'NULL';
			$secundario1d = ( isset($_POST['secundario1d']) ) ? $_POST['secundario1d'] : 'NULL';
			$secundario1e = ( isset($_POST['secundario1e']) ) ? $_POST['secundario1e'] : 'NULL';
			
			$secundario2a = ( isset($_POST['secundario2a']) ) ? $_POST['secundario2a'] : 'NULL';
			$secundario2b = ( isset($_POST['secundario2b']) ) ? $_POST['secundario2b'] : 'NULL';
			$secundario2c = ( isset($_POST['secundario2c']) ) ? $_POST['secundario2c'] : 'NULL';
			$secundario2d = ( isset($_POST['secundario2d']) ) ? $_POST['secundario2d'] : 'NULL';
			$secundario2e = ( isset($_POST['secundario2e']) ) ? $_POST['secundario2e'] : 'NULL';
			
			
			
			if( $preferenciasExistentes->num_rows != 0 ) {
				
				// Hay preferencias guardadas para este usuario
				// que deben sobreescribirse
				
				$actualizar = $bd->query("UPDATE preferencias SET
											estructura = " . intval($estructura) . ",
											idcolores = " . $bd->escapar($idcolores) . ",
											
											primarioa = " . $bd->escapar($primarioa) . ",
											primariob = " . $bd->escapar($primariob) . ",
											primarioc = " . $bd->escapar($primarioc) . ",
											primariod = " . $bd->escapar($primariod) . ",
											primarioe = " . $bd->escapar($primarioe) . ",
											
											secundario1a = " . $bd->escapar($secundario1a) . ",
											secundario1b = " . $bd->escapar($secundario1b) . ",
											secundario1c = " . $bd->escapar($secundario1c) . ",
											secundario1d = " . $bd->escapar($secundario1d) . ",
											secundario1e = " . $bd->escapar($secundario1e) . ",
											
											secundario2a = " . $bd->escapar($secundario2a) . ",
											secundario2b = " . $bd->escapar($secundario2b) . ",
											secundario2c = " . $bd->escapar($secundario2c) . ",
											secundario2d = " . $bd->escapar($secundario2d) . ",
											secundario2e = " . $bd->escapar($secundario2e)
											. " WHERE id_usuario = " . $usuario->getID()
										);
				
				if( $actualizar == false ) {
					echo 'Hubo un error con la base de datos:' . $bd->error();
				} // if( $actualizar == false ) {
				
				
			} else {
				
				
				// No existen preferencias para este usuario
				
				$insertar = $bd->query("INSERT INTO preferencias
											(
											id_usuario,
											
											estructura,
											idcolores,
											
											primarioa,
											primariob,
											primarioc,
											primariod,
											primarioe,
											
											secundario1a,
											secundario1b,
											secundario1c,
											secundario1d,
											secundario1e,
											
											secundario2a,
											secundario2b,
											secundario2c,
											secundario2d,
											secundario2e
											)
											VALUES (" .
											
											$usuario->getID() . "," .
											
											intval($estructura) . "," .
											$bd->escapar($idcolores) . "," .
											
											$bd->escapar($primarioa) . "," .
											$bd->escapar($primariob) . "," .
											$bd->escapar($primarioc) . "," .
											$bd->escapar($primariod) . "," .
											$bd->escapar($primarioe) . "," .
											
											$bd->escapar($secundario1a) . "," .
											$bd->escapar($secundario1b) . "," .
											$bd->escapar($secundario1c) . "," .
											$bd->escapar($secundario1d) . "," .
											$bd->escapar($secundario1e) . "," .
											
											$bd->escapar($secundario2a) . "," .
											$bd->escapar($secundario2b) . "," .
											$bd->escapar($secundario2c) . "," .
											$bd->escapar($secundario2d) . "," .
											$bd->escapar($secundario2e) . ")"
										);
				
				if( $insertar == false ) {
					echo 'Hubo un error con la base de datos:' . $bd->error();
				} // if( $insertar == false ) {
				
			} // if( $preferenciasExistentes->num_rows != 0 ) { ... else ...
			
			
			echo 'Preferencias actualizadas';
			
		} // if( $preferenciasExistentes == false ) { ... else ...
	} // if( isset($_POST['preferencias']) ) {
	
	
	
	if( isset($_POST['contenido']) ) {
		
		echo 'Almacenando preferencias de contenido';
		
		$idGrupo = ( isset($_POST['idGrupo']) ) ? $_POST['idGrupo'] : 'NULL';
		$idTema = ( isset($_POST['idTema']) ) ? $_POST['idTema'] : 'NULL';
		$idContenido = ( isset($_POST['idContenido']) ) ? $_POST['idContenido'] : 'NULL';
		
		echo $idGrupo . '   ';
		echo $idTema . '   ';
		echo $idContenido . '   ';
		
		$insertar = $bd->query("INSERT INTO eleccion_recursos
											(
											id_usuario,
											id_grupo,
											id_tema,
											id_contenido
											)
											VALUES (" .
											
											$usuario->getID() . "," .
											
											intval($idGrupo) . "," .
											intval($idTema) . "," .
											intval($idContenido) . ")"
										);
				
		if( $insertar == false ) {
			echo 'Hubo un error con la base de datos:' . $bd->error();
		} else {
			echo 'Preferencias de contenido almacenadas';
		} // if( $insertar == false ) {
			
	} // if( isset($_POST['contenido']) ) {
	
	
	
	if( isset($_POST['evaluacion']) ) {
		
		if( isJson($_POST['datos']) ) {
			
			
			$datos = json_decode($_POST['datos']);
			
			//var_dump($datos);
			
			$calificacion = 0.0;
			$totalDeReactivos = count($datos);
			
			$idEvaluacion = 0;
			$idGrupo = 0;
			
			$primerDato = true;
			
			foreach( $datos as $dato ) {
				
				if( $primerDato ) {
					$idEvaluacion = intval($dato->idEvaluacion);
					$idGrupo = intval($dato->idGrupo);
					$primerDato = false;
				} // if( $primerDato ) {
				
				
				$respuestas = '';
				$primeraRespuesta = true;
				
				
				foreach( $dato->respuestas as $respuesta ) {
					
					$respuesta = intval($respuesta);
					
					if( $primeraRespuesta ) {
						$respuestas = $respuesta;
						
						$primeraRespuesta = false;
					} else {
						$respuestas .= ',' . $respuesta;
					} // if( $primeraRespuesta ) {
					
				} // foreach( $dato->respuestas as $respuesta ) {
				
				$insertar = $bd->query("INSERT INTO respuestas_evaluaciones
											(
											id_usuario,
											id_contenido,
											orden,
											respuestas,
											fecha
											)
											VALUES (" .
											
											$usuario->getID() . "," .
											
											intval($dato->idEvaluacion) . "," .
											intval($dato->orden) . "," .
											"'" . $respuestas . "'," .
											"CURRENT_TIMESTAMP)"
										);
				
				if( $insertar == false ) {
					echo 'Hubo un error con la base de datos:' . $bd->error();
				} // if( $insertar == false ) {
				
				
				$select = $bd->query("SELECT correctas FROM evaluaciones WHERE id_contenido = " . intval($dato->idEvaluacion) . " AND orden = " . intval($dato->orden));
				
				$respuestasCorrectas = array();
				
				if( $select == false ) {
					echo 'Hubo un error con la base de datos:' . $bd->error();
				} else {
					if( $select->num_rows != 0 ) {
						
						foreach( $select as $respuestaCorrecta ) {
							$respuestasCorrectas = explode(",",$respuestaCorrecta['correctas']);
							break;
						} // foreach( $select as $respuestaCorrecta ) {
						
						
						// Si la diferencia de los dos arreglos de respuestas
						// es un arreglo vacío, las respuestas concuerdan. Por
						// lo tanto la respuesta es correcta y se suma la cantidad
						// correspondiente a la calificación
						//
						$diferenciaDeArreglos = array_diff($dato->respuestas,$respuestasCorrectas);
						
						if( empty($diferenciaDeArreglos) ) {
							$calificacion += 100.0/$totalDeReactivos;
						} // if(empty(array_diff($dato->respuestas,$respuestasCorrectas))) {
					} else {
						echo 'No se encontraron datos de la evaluación';
					} // if( $select->num_rows != 0 ) {
				} // if( $select == false ) { ... else ...
				
			} // foreach( $datos as $dato ) {
			
			echo 'Calificación de la evaluación: <span>' . round( $calificacion, 2) . "%</span>";
			
			
			$insertar = $bd->query("INSERT INTO calificaciones
											(
											id_usuario,
											id_contenido,
											id_grupo,
											calificacion,
											fecha
											)
											VALUES (" .
											
											$usuario->getID() . "," .
											
											$idEvaluacion . "," .
											$idGrupo . "," .
											$calificacion . "," .
											"CURRENT_TIMESTAMP)"
										);
				
			if( $insertar == false ) {
				echo 'Hubo un error con la base de datos:' . $bd->error();
			} // if( $insertar == false ) {
				
		} else {
			echo 'Hubo un error al recibir los datos';
		} // if( isJson($_POST['datos']) ) {
		
	} // if( isset($_POST['evaluacion']) ) {
	
	
	
	if( isset($_POST['retroalimentacion']) ) {
		
		$idGrupo = ( isset($_POST['idGrupo']) ) ? $_POST['idGrupo'] : 'NULL';
		
		$respuesta1 = ( isset($_POST['respuesta1']) ) ? intval($_POST['respuesta1']) : 'NULL';
		$respuesta2 = ( isset($_POST['respuesta2']) ) ? intval($_POST['respuesta2']) : 'NULL';
		$respuesta3 = ( isset($_POST['respuesta3']) ) ? intval($_POST['respuesta3']) : 'NULL';
		$respuesta4 = ( isset($_POST['respuesta4']) ) ? intval($_POST['respuesta4']) : 'NULL';
		$respuesta5 = ( isset($_POST['respuesta5']) ) ? intval($_POST['respuesta5']) : 'NULL';
		$respuesta6 = ( isset($_POST['respuesta6']) ) ? intval($_POST['respuesta6']) : 'NULL';
		$respuesta7 = ( isset($_POST['respuesta7']) ) ? $bd->escapar($_POST['respuesta7']) : 'NULL';
		$respuesta8 = ( isset($_POST['respuesta8']) ) ? $bd->escapar($_POST['respuesta8']) : 'NULL';
		$respuesta9 = ( isset($_POST['respuesta9']) ) ? $bd->escapar($_POST['respuesta9']) : 'NULL';
		
		$insertar = $bd->query("INSERT INTO retroalimentacion
											(
											id_usuario,
											id_grupo,
											
											respuesta1,
											respuesta2,
											respuesta3,
											respuesta4,
											respuesta5,
											respuesta6,
											
											respuesta7,
											respuesta8,
											respuesta9,
											
											fecha
											)
											VALUES (" .
											
											$usuario->getID() . "," .
											
											intval($idGrupo) . "," .
											
											$respuesta1 . "," .
											$respuesta2 . "," .
											$respuesta3 . "," .
											$respuesta4 . "," .
											$respuesta5 . "," .
											$respuesta6 . "," .
											
											$respuesta7 . "," .
											$respuesta8 . "," .
											$respuesta9 . "," .
											
											"CURRENT_TIMESTAMP)"
										);
				
		if( $insertar == false ) {
			echo 'Hubo un error con la base de datos:' . $bd->error();
		} // if( $insertar == false ) {
		
	} // if( isset($_POST['retroalimentacion']) ) {
	
} else {
	echo 'No tiene permiso para actualizar la base de datos';
} // if(isset($usuario) && !empty($usuario) ...


// Función que determina si la cadena
// recibida es JSON o no
function isJson($string) {
	json_decode($string);
	return (json_last_error() == JSON_ERROR_NONE);
} // function isJson($string) {
?>