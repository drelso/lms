<?php
// Agregar materia


// BORRAR
// Checa si la sesión ya existe
if(session_id() == '' || !isset($_SESSION)) {
    session_start();
}

// DESCOMENTAR
//include('inc/header.php');

// BORRAR CUANDO SE AGREGUE EL HEADER
echo '<meta charset="UTF-8">';


error_reporting(E_ALL);
ini_set('display_errors', 1);

require_once(  __DIR__ . '/inc/config/config.php' );



if( isset( $_GET['error'] ) ) {
	echo '<h1 class="mensaje-error">' . urldecode( $_GET['error'] ) . '</h1>';
} else {
	
	if( isset( $_GET['id'] ) ) {
		
		require_once(  __DIR__ . '/inc/db/bd.class.php' );
		$bd = new BD();
		
		$idMateria = $_GET['id'];
		
		$id = 0;
		$usuario;
		$validado = false;

		// Si el usuario está registrado
		// instancia un objeto de clase Administrador
		//
		if( isset( $_SESSION['usuario_registrado'] ) ) {
			
			require_once('inc/clases/administrador.class.php');
			require_once('inc/clases/profesor.class.php');
			
			$id = intval( $_SESSION['usuario_registrado'] );
			
			if($id > 0) {
				$validado = Administrador::esAdministrador( $id );
				
				if( !$validado ) { $validado = Profesor::profesorTieneMateria($id,$idMateria); }
			} // if($id > 0) {
			
		} // if(isset($_SESSION['usuario_registrado'])) {
		
		
		if( $validado ) {
						
			echo '<div class="main editar">';

			echo '<h1>Editar materia</h1>';
			
			$departamentoMateria = '';
			
			
			// Consulta a tabla de usuarios
			$resultados = $bd->query("SELECT * FROM materia WHERE id = " . $bd->escapar( urldecode( $idMateria ) ) );
			
			if( $resultados == false ) {
				echo 'Hubo un error con la base de datos:' . $bd->error();
			} else {
				
				if( $resultados->num_rows > 0 ) {
					foreach( $resultados as $resultado ) {
						
						$nombreMateria = $resultado['nombre'];
						$departamentoMateria = $resultado['id_departamento'];
						
						require_once(  __DIR__ . '/inc/admin-func/administrar-materia.php' );
						
						echo '<div id="resultado-materia"></div>';
						
						break;
					} // foreach( $resultados as $resultado ) {
				} // if( $resultados->num_rows > 0 ) {
			} // if( $resultados == false ) { ... else ...
			
			
			echo '</div> <!-- /main -->';
			
		} else {
		
			// Redirige al usuario si no está validado como administrador
			$mensajeError = 'Error: no tiene permiso para editar esta materia';
			header( 'Location: ' . BASEDIR . '?error=' . htmlentities( urlencode( $mensajeError ) ) );
			die();
			
		} // if( $validado ) { ... else ...
		
	} // if( isset( $_GET['id'] ) ) {
	
	
} // if( isset( $_GET['error'] ) ) { ... else ...
?>