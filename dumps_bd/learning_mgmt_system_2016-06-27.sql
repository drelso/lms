-- MySQL dump 10.13  Distrib 5.7.9, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: learning_mgmt_system
-- ------------------------------------------------------
-- Server version	5.5.41-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `calificaciones`
--

DROP TABLE IF EXISTS `calificaciones`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `calificaciones` (
  `id_usuario` int(11) unsigned NOT NULL,
  `id_contenido` int(11) unsigned NOT NULL,
  `id_grupo` int(11) unsigned NOT NULL,
  `calificacion` double NOT NULL DEFAULT '0',
  `fecha` datetime DEFAULT NULL,
  PRIMARY KEY (`id_usuario`,`id_contenido`),
  KEY `calificaciones_contenido` (`id_contenido`),
  KEY `calificaciones_grupo` (`id_grupo`),
  CONSTRAINT `calificaciones_contenido` FOREIGN KEY (`id_contenido`) REFERENCES `contenidos` (`id`),
  CONSTRAINT `calificaciones_grupo` FOREIGN KEY (`id_grupo`) REFERENCES `grupos` (`id`),
  CONSTRAINT `calificaciones_usuario` FOREIGN KEY (`id_usuario`) REFERENCES `usuarios` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `calificaciones`
--

LOCK TABLES `calificaciones` WRITE;
/*!40000 ALTER TABLE `calificaciones` DISABLE KEYS */;
INSERT INTO `calificaciones` VALUES (3,312,10,100,'2016-06-25 11:32:36'),(3,314,10,0,'2016-06-25 11:33:30');
/*!40000 ALTER TABLE `calificaciones` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contenidos`
--

DROP TABLE IF EXISTS `contenidos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contenidos` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `id_tema` int(11) unsigned NOT NULL,
  `nombre` varchar(255) DEFAULT NULL,
  `informacion` varchar(10000) DEFAULT NULL,
  `archivo` varchar(255) DEFAULT NULL,
  `iframe` varchar(1000) DEFAULT NULL,
  `tipo_de_aprendizaje` int(11) NOT NULL,
  `tipo_de_contenido` int(11) unsigned NOT NULL,
  `orden` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `contenido_tipocontenido` (`tipo_de_contenido`),
  KEY `contenido_tema` (`id_tema`),
  CONSTRAINT `contenido_tema` FOREIGN KEY (`id_tema`) REFERENCES `tema` (`id`),
  CONSTRAINT `contenido_tipocontenido` FOREIGN KEY (`tipo_de_contenido`) REFERENCES `tipo_contenido` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=319 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contenidos`
--

LOCK TABLES `contenidos` WRITE;
/*!40000 ALTER TABLE `contenidos` DISABLE KEYS */;
INSERT INTO `contenidos` VALUES (7,4,'A','AA',NULL,NULL,0,1,1),(8,4,'B','BB',NULL,NULL,0,3,2),(9,4,'C','CC',NULL,NULL,0,2,3),(10,4,'E','GG',NULL,NULL,0,4,4),(11,8,'Bienvenida','Hola a tod@s',NULL,NULL,0,1,1),(12,9,'Primera presentación','Presentación I',NULL,NULL,0,2,1),(13,7,'Otro tema','',NULL,NULL,0,3,1),(125,67,'Diagnostico','',NULL,NULL,0,5,1),(127,68,'R','RR',NULL,NULL,0,1,1),(128,68,'S','SS',NULL,NULL,0,3,2),(198,114,'Video','','ai-2.jpg',NULL,0,3,1),(199,114,'Imagen','','The-Future-of-AI-1030x634-1.jpg',NULL,0,2,2),(200,115,'Audio','Sobre este audio.',NULL,NULL,0,4,1),(307,158,'Narrativo','Puro texto puro texto puro texto puro texto puro texto puro texto puro texto',NULL,NULL,0,1,1),(308,158,'Fondo','Una imagen de fondo','The-Future-of-AI-1030x634-3.jpg',NULL,0,2,2),(309,158,'Un video','Prueba subiendo un video','mov_bbb-1.mp4',NULL,0,3,3),(310,158,'Podcast','Prueba de audio','horse.mp3',NULL,0,4,4),(311,158,'Presentación','Prueba de presentación','Akun_Web3.pdf',NULL,0,6,5),(312,158,'Prueba 2','',NULL,NULL,0,5,6),(313,159,'Audio','Presentación','MARZO 29-3.pdf',NULL,0,6,1),(314,159,'Pregunta sobre audio','',NULL,NULL,0,5,2),(315,160,'B','BB',NULL,NULL,0,3,1),(316,160,'C','CC',NULL,NULL,0,1,2),(318,163,'Video iframe','Prueba de video iframe',NULL,'<iframe width=\"560\" height=\"315\" src=\"https://www.youtube.com/embed/AU1GVkYD78w\" frameborder=\"0\" allowfullscreen></iframe>',0,3,1);
/*!40000 ALTER TABLE `contenidos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `departamento`
--

DROP TABLE IF EXISTS `departamento`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `departamento` (
  `id` varchar(11) NOT NULL DEFAULT '',
  `nombre` varchar(255) NOT NULL DEFAULT '',
  `director` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `departamento`
--

LOCK TABLES `departamento` WRITE;
/*!40000 ALTER TABLE `departamento` DISABLE KEYS */;
INSERT INTO `departamento` VALUES ('COM','Computación','Miguel González'),('EDU','Educación','María Rodríguez E'),('FIS','Física','Alejandro Martínez'),('MAT','Matemáticas','José González');
/*!40000 ALTER TABLE `departamento` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `eleccion_recursos`
--

DROP TABLE IF EXISTS `eleccion_recursos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `eleccion_recursos` (
  `id_usuario` int(11) NOT NULL,
  `id_grupo` int(11) DEFAULT NULL,
  `id_tema` int(11) DEFAULT NULL,
  `id_contenido` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `eleccion_recursos`
--

LOCK TABLES `eleccion_recursos` WRITE;
/*!40000 ALTER TABLE `eleccion_recursos` DISABLE KEYS */;
INSERT INTO `eleccion_recursos` VALUES (3,10,158,309),(3,10,158,308),(3,10,159,313),(3,10,160,316),(3,10,158,309),(3,10,159,313),(3,10,160,315),(3,10,159,313),(3,10,160,316);
/*!40000 ALTER TABLE `eleccion_recursos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `estilos_de_aprendizaje`
--

DROP TABLE IF EXISTS `estilos_de_aprendizaje`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `estilos_de_aprendizaje` (
  `id_usuario` int(11) NOT NULL,
  `activo` float DEFAULT NULL,
  `reflexivo` float DEFAULT NULL,
  `sensitivo` float DEFAULT NULL,
  `intuitivo` float DEFAULT NULL,
  `visual` float DEFAULT NULL,
  `verbal` float DEFAULT NULL,
  `secuencial` float DEFAULT NULL,
  `global` float DEFAULT NULL,
  PRIMARY KEY (`id_usuario`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `estilos_de_aprendizaje`
--

LOCK TABLES `estilos_de_aprendizaje` WRITE;
/*!40000 ALTER TABLE `estilos_de_aprendizaje` DISABLE KEYS */;
INSERT INTO `estilos_de_aprendizaje` VALUES (96,0,0.0909091,0.272727,0,0.454545,0,0.0909091,0),(97,0,0.0909091,0.636364,0,0.636364,0,0.636364,0),(98,0.454545,0,0.636364,0,0.454545,0,0.454545,0),(99,1,0,1,0,1,0,1,0),(100,0.0909091,0,0.272727,0,0.636364,0,0.0909091,0),(101,0.272727,0,0.454545,0,0.818182,0,0.818182,0),(102,0,0.454545,0.272727,0,1,0,0.454545,0),(103,0.0909091,0,0.272727,0,0,0.454545,0,0.272727),(104,0,0.0909091,0.272727,0,0,0.272727,0.0909091,0),(105,0.454545,0,0.818182,0,0,0.454545,0.818182,0),(106,0.272727,0,0.636364,0,0.0909091,0,0.272727,0),(107,0,0.0909091,0.454545,0,0.454545,0,0,0.454545),(108,0.272727,0,0.636364,0,0.636364,0,0.454545,0),(109,0.272727,0,0.636364,0,0.636364,0,0.454545,0);
/*!40000 ALTER TABLE `estilos_de_aprendizaje` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `estructuras`
--

DROP TABLE IF EXISTS `estructuras`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `estructuras` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `estilos` varchar(10000) NOT NULL DEFAULT '',
  `id_html_1` int(11) unsigned NOT NULL,
  `id_html_2` int(11) unsigned NOT NULL,
  `id_html_3` int(11) unsigned NOT NULL,
  `id_html_4` int(11) unsigned NOT NULL,
  `id_html_5` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `estructura_html_2` (`id_html_2`),
  KEY `estructura_html_1` (`id_html_1`),
  KEY `estructura_html_3` (`id_html_3`),
  KEY `estructura_html_4` (`id_html_4`),
  KEY `estructura_html_5` (`id_html_5`),
  CONSTRAINT `estructura_html_1` FOREIGN KEY (`id_html_1`) REFERENCES `html` (`id`),
  CONSTRAINT `estructura_html_2` FOREIGN KEY (`id_html_2`) REFERENCES `html` (`id`),
  CONSTRAINT `estructura_html_3` FOREIGN KEY (`id_html_3`) REFERENCES `html` (`id`),
  CONSTRAINT `estructura_html_4` FOREIGN KEY (`id_html_4`) REFERENCES `html` (`id`),
  CONSTRAINT `estructura_html_5` FOREIGN KEY (`id_html_5`) REFERENCES `html` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `estructuras`
--

LOCK TABLES `estructuras` WRITE;
/*!40000 ALTER TABLE `estructuras` DISABLE KEYS */;
/*!40000 ALTER TABLE `estructuras` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `evaluaciones`
--

DROP TABLE IF EXISTS `evaluaciones`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `evaluaciones` (
  `id_contenido` int(11) unsigned NOT NULL,
  `pregunta` varchar(5000) DEFAULT NULL,
  `imagenPregunta` varchar(255) DEFAULT NULL,
  `correctas` varchar(100) NOT NULL DEFAULT '',
  `orden` int(11) NOT NULL DEFAULT '0',
  `respuesta_1` varchar(1000) DEFAULT NULL,
  `imagenRespuesta_1` varchar(255) DEFAULT NULL,
  `respuesta_2` varchar(1000) DEFAULT NULL,
  `imagenRespuesta_2` varchar(255) DEFAULT NULL,
  `respuesta_3` varchar(1000) DEFAULT NULL,
  `imagenRespuesta_3` varchar(255) DEFAULT NULL,
  `respuesta_4` varchar(1000) DEFAULT NULL,
  `imagenRespuesta_4` varchar(255) DEFAULT NULL,
  `respuesta_5` varchar(1000) DEFAULT NULL,
  `imagenRespuesta_5` varchar(255) DEFAULT NULL,
  `respuesta_6` varchar(1000) DEFAULT NULL,
  `imagenRespuesta_6` varchar(255) DEFAULT NULL,
  `respuesta_7` varchar(1000) DEFAULT NULL,
  `imagenRespuesta_7` varchar(255) DEFAULT NULL,
  `respuesta_8` varchar(1000) DEFAULT NULL,
  `imagenRespuesta_8` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_contenido`,`orden`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `evaluaciones`
--

LOCK TABLES `evaluaciones` WRITE;
/*!40000 ALTER TABLE `evaluaciones` DISABLE KEYS */;
INSERT INTO `evaluaciones` VALUES (125,'A?',NULL,'0,2',1,'a',NULL,'b',NULL,'c',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(125,'F?',NULL,'2',2,'d',NULL,'e',NULL,'f',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(125,'H?',NULL,'1',3,'g',NULL,'h',NULL,'i',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(312,'1?',NULL,'0',1,'1',NULL,'2',NULL,'3',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(312,'12?',NULL,'0,2',2,'10',NULL,'11',NULL,'12',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(312,'21',NULL,'1',3,'20',NULL,'21',NULL,'22',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(314,'T?',NULL,'0',1,'t',NULL,'u',NULL,'v',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `evaluaciones` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `grupos`
--

DROP TABLE IF EXISTS `grupos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `grupos` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `id_materia` varchar(11) NOT NULL DEFAULT '',
  `id_profesor` int(11) unsigned DEFAULT NULL,
  `id_interfaz` int(11) unsigned DEFAULT NULL,
  `id_periodo` int(11) unsigned NOT NULL,
  `numero` int(11) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `grupo_periodo` (`id_periodo`),
  KEY `grupo_interfaz` (`id_interfaz`),
  KEY `grupo_profesor` (`id_profesor`),
  KEY `grupo_materia` (`id_materia`),
  CONSTRAINT `grupo_interfaz` FOREIGN KEY (`id_interfaz`) REFERENCES `interfaz` (`id`),
  CONSTRAINT `grupo_materia` FOREIGN KEY (`id_materia`) REFERENCES `materia` (`id`),
  CONSTRAINT `grupo_periodo` FOREIGN KEY (`id_periodo`) REFERENCES `periodos` (`id`),
  CONSTRAINT `grupo_profesor` FOREIGN KEY (`id_profesor`) REFERENCES `usuarios` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `grupos`
--

LOCK TABLES `grupos` WRITE;
/*!40000 ALTER TABLE `grupos` DISABLE KEYS */;
INSERT INTO `grupos` VALUES (1,'1',4,NULL,1,0),(2,'1',5,NULL,1,0),(3,'2',4,NULL,1,0),(4,'2',3,NULL,1,0),(5,'1',3,NULL,1,0),(7,'1',3,NULL,1,1),(8,'1',3,NULL,1,2),(9,'MT3201',4,NULL,1,0),(10,'4',19,NULL,5,1),(11,'4',110,NULL,1,1);
/*!40000 ALTER TABLE `grupos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `html`
--

DROP TABLE IF EXISTS `html`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `html` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `id_tipo_contenido` int(11) unsigned NOT NULL,
  `html` varchar(10000) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `html_tipocontenido` (`id_tipo_contenido`),
  CONSTRAINT `html_tipocontenido` FOREIGN KEY (`id_tipo_contenido`) REFERENCES `tipo_contenido` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `html`
--

LOCK TABLES `html` WRITE;
/*!40000 ALTER TABLE `html` DISABLE KEYS */;
/*!40000 ALTER TABLE `html` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `inteligencias_multiples`
--

DROP TABLE IF EXISTS `inteligencias_multiples`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `inteligencias_multiples` (
  `id_usuario` int(11) NOT NULL,
  `verbal` float DEFAULT NULL,
  `logico_matematica` float DEFAULT NULL,
  `espacial` float DEFAULT NULL,
  `musical` float DEFAULT NULL,
  `kinestesica` float DEFAULT NULL,
  `intrapersonal` float DEFAULT NULL,
  `interpersonal` float DEFAULT NULL,
  `naturalista` float DEFAULT NULL,
  PRIMARY KEY (`id_usuario`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `inteligencias_multiples`
--

LOCK TABLES `inteligencias_multiples` WRITE;
/*!40000 ALTER TABLE `inteligencias_multiples` DISABLE KEYS */;
INSERT INTO `inteligencias_multiples` VALUES (96,4.5,3.8,3.6,3.1,4.1,2.9,4.5,3.4),(97,5,4.3,4.6,4.3,4.9,4.2,4.6,3.5),(98,4.9,4.9,4.6,4.6,4.8,3.5,4.7,3.9),(99,4,4,4,4,4,4,4,4),(100,4.8,4.2,4.2,3.8,4.2,4.7,3.8,4.1),(101,4.3,4.2,3.3,3.4,3.2,4.7,4.7,2.7),(102,4.4,4.2,4.8,3.8,3.9,3.7,3.5,4.1),(103,3.7,3.1,4.6,3.3,4.3,4.7,3.5,2.3),(104,3.8,4.5,4.4,2.6,4.2,2.1,2.8,3),(105,5,4.5,4.7,4.2,4.6,4.8,4.9,3.2),(106,5,4.9,4.5,3.7,4.5,3.1,4.3,3.7),(107,3.8,3.6,3.5,3.5,3.6,3.6,3.5,3.9),(108,4.7,4.6,3.1,4,4.2,4,4.5,2.9),(109,4.4,4,3.8,3.9,3.7,1.8,3.7,2.8);
/*!40000 ALTER TABLE `inteligencias_multiples` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `interfaz`
--

DROP TABLE IF EXISTS `interfaz`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `interfaz` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `id_paleta` int(11) unsigned NOT NULL,
  `id_estructura` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `interfaz_paleta` (`id_paleta`),
  KEY `interfaz_estructura` (`id_estructura`),
  CONSTRAINT `interfaz_estructura` FOREIGN KEY (`id_estructura`) REFERENCES `estructuras` (`id`),
  CONSTRAINT `interfaz_paleta` FOREIGN KEY (`id_paleta`) REFERENCES `paleta_de_colores` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `interfaz`
--

LOCK TABLES `interfaz` WRITE;
/*!40000 ALTER TABLE `interfaz` DISABLE KEYS */;
/*!40000 ALTER TABLE `interfaz` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `materia`
--

DROP TABLE IF EXISTS `materia`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `materia` (
  `id` varchar(11) NOT NULL DEFAULT '',
  `nombre` varchar(255) NOT NULL DEFAULT '',
  `id_departamento` varchar(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `materia_departamento` (`id_departamento`),
  CONSTRAINT `materia_departamento` FOREIGN KEY (`id_departamento`) REFERENCES `departamento` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `materia`
--

LOCK TABLES `materia` WRITE;
/*!40000 ALTER TABLE `materia` DISABLE KEYS */;
INSERT INTO `materia` VALUES ('1','Administración del desarrollo de software','COM'),('2','Técnicas de programación','COM'),('3','Metodología de la Investigación','COM'),('4','Inteligencia Artificial','COM'),('5','Sistemas Distribuidos','COM'),('F3001','Introducción a la física','FIS'),('MT3201','Cálculo I','MAT'),('TC2001','Bases de datos distribuidas','COM');
/*!40000 ALTER TABLE `materia` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `paleta_de_colores`
--

DROP TABLE IF EXISTS `paleta_de_colores`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `paleta_de_colores` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `id_color_1` int(11) unsigned NOT NULL,
  `id_color_2` int(11) unsigned NOT NULL,
  `id_color_3` int(11) unsigned NOT NULL,
  `id_color_4` int(11) unsigned NOT NULL,
  `id_color_5` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `paletacolor_1` (`id_color_1`),
  KEY `paletacolor_2` (`id_color_2`),
  KEY `paletacolor_3` (`id_color_3`),
  KEY `paletacolor_4` (`id_color_4`),
  KEY `paletacolor_5` (`id_color_5`),
  CONSTRAINT `paletacolor_1` FOREIGN KEY (`id_color_1`) REFERENCES `colores` (`id`),
  CONSTRAINT `paletacolor_2` FOREIGN KEY (`id_color_2`) REFERENCES `colores` (`id`),
  CONSTRAINT `paletacolor_3` FOREIGN KEY (`id_color_3`) REFERENCES `colores` (`id`),
  CONSTRAINT `paletacolor_4` FOREIGN KEY (`id_color_4`) REFERENCES `colores` (`id`),
  CONSTRAINT `paletacolor_5` FOREIGN KEY (`id_color_5`) REFERENCES `colores` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `paleta_de_colores`
--

LOCK TABLES `paleta_de_colores` WRITE;
/*!40000 ALTER TABLE `paleta_de_colores` DISABLE KEYS */;
/*!40000 ALTER TABLE `paleta_de_colores` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `periodos`
--

DROP TABLE IF EXISTS `periodos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `periodos` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) DEFAULT NULL,
  `descripcion` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `periodos`
--

LOCK TABLES `periodos` WRITE;
/*!40000 ALTER TABLE `periodos` DISABLE KEYS */;
INSERT INTO `periodos` VALUES (1,'20153','Agosto - Diciembre 2015'),(2,'ds','sdf'),(3,'20151','Enero – Mayo 2015'),(4,'20152','Junio – Septiembre 2015'),(5,'20161','Enero – Mayo 2016');
/*!40000 ALTER TABLE `periodos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `preferencias`
--

DROP TABLE IF EXISTS `preferencias`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `preferencias` (
  `id_usuario` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `estructura` tinyint(4) NOT NULL,
  `idcolores` varchar(64) DEFAULT '',
  `primarioa` varchar(6) DEFAULT NULL,
  `primariob` varchar(6) DEFAULT NULL,
  `primarioc` varchar(6) DEFAULT NULL,
  `primariod` varchar(6) DEFAULT NULL,
  `primarioe` varchar(6) DEFAULT NULL,
  `secundario1a` varchar(6) DEFAULT NULL,
  `secundario1b` varchar(6) DEFAULT NULL,
  `secundario1c` varchar(6) DEFAULT NULL,
  `secundario1d` varchar(6) DEFAULT NULL,
  `secundario1e` varchar(6) DEFAULT NULL,
  `secundario2a` varchar(6) DEFAULT NULL,
  `secundario2b` varchar(6) DEFAULT NULL,
  `secundario2c` varchar(6) DEFAULT NULL,
  `secundario2d` varchar(6) DEFAULT NULL,
  `secundario2e` varchar(6) DEFAULT NULL,
  PRIMARY KEY (`id_usuario`),
  UNIQUE KEY `id_usuario_UNIQUE` (`id_usuario`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `preferencias`
--

LOCK TABLES `preferencias` WRITE;
/*!40000 ALTER TABLE `preferencias` DISABLE KEYS */;
INSERT INTO `preferencias` VALUES (3,1,'5000u0kllll1h++89I7rXeByi9v','AA3939','FFF5F5','FFBEBE','740F0F','4C0000','AA6C39','FFF9F5','FFDCBE','743D0F','4C2200','882D61','FFF5FB','E1A8C8','5D0C3A','3D0022');
/*!40000 ALTER TABLE `preferencias` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `primer_registro`
--

DROP TABLE IF EXISTS `primer_registro`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `primer_registro` (
  `id_usuario` int(11) NOT NULL,
  `registrado` int(11) NOT NULL,
  UNIQUE KEY `id_usuario_UNIQUE` (`id_usuario`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `primer_registro`
--

LOCK TABLES `primer_registro` WRITE;
/*!40000 ALTER TABLE `primer_registro` DISABLE KEYS */;
INSERT INTO `primer_registro` VALUES (3,1),(23,1),(102,1);
/*!40000 ALTER TABLE `primer_registro` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `registros`
--

DROP TABLE IF EXISTS `registros`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `registros` (
  `id_usuario` int(11) unsigned NOT NULL,
  `id_grupo` int(11) unsigned NOT NULL,
  `completado` double DEFAULT NULL,
  `calificacion` double DEFAULT NULL,
  PRIMARY KEY (`id_usuario`),
  KEY `registros_grupo` (`id_grupo`),
  CONSTRAINT `registros_grupo` FOREIGN KEY (`id_grupo`) REFERENCES `grupos` (`id`),
  CONSTRAINT `registros_usuario` FOREIGN KEY (`id_usuario`) REFERENCES `usuarios` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `registros`
--

LOCK TABLES `registros` WRITE;
/*!40000 ALTER TABLE `registros` DISABLE KEYS */;
/*!40000 ALTER TABLE `registros` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `respuestas_evaluaciones`
--

DROP TABLE IF EXISTS `respuestas_evaluaciones`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `respuestas_evaluaciones` (
  `id_usuario` int(11) NOT NULL,
  `id_contenido` int(11) NOT NULL,
  `orden` int(11) NOT NULL,
  `respuestas` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fecha` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `respuestas_evaluaciones`
--

LOCK TABLES `respuestas_evaluaciones` WRITE;
/*!40000 ALTER TABLE `respuestas_evaluaciones` DISABLE KEYS */;
INSERT INTO `respuestas_evaluaciones` VALUES (3,312,1,'0',NULL),(3,312,2,'1',NULL),(3,312,3,'0',NULL),(3,312,1,'2','2016-06-24 11:39:19'),(3,312,2,'1','2016-06-24 11:39:19'),(3,312,3,'0','2016-06-24 11:39:19'),(3,312,1,'1','2016-06-24 11:40:42'),(3,312,2,'1','2016-06-24 11:40:42'),(3,312,3,'0','2016-06-24 11:40:42'),(3,312,1,'1','2016-06-24 11:42:27'),(3,312,1,'0,1,2','2016-06-24 11:43:31'),(3,312,2,'0,1','2016-06-24 11:43:31'),(3,312,3,'1,2','2016-06-24 11:43:31'),(3,312,1,'1','2016-06-24 12:23:23'),(3,312,2,'0,2','2016-06-24 12:23:23'),(3,312,3,'0,1','2016-06-24 12:23:23'),(3,312,1,'1','2016-06-24 19:07:51'),(3,312,2,'0,2','2016-06-24 19:07:51'),(3,312,3,'0','2016-06-24 19:07:51'),(3,312,1,'0','2016-06-25 11:32:36'),(3,312,2,'0,2','2016-06-25 11:32:36'),(3,312,3,'1','2016-06-25 11:32:36'),(3,314,1,'0,1','2016-06-25 11:33:30');
/*!40000 ALTER TABLE `respuestas_evaluaciones` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `retroalimentacion`
--

DROP TABLE IF EXISTS `retroalimentacion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `retroalimentacion` (
  `id_usuario` int(11) NOT NULL,
  `id_grupo` int(11) NOT NULL,
  `respuesta1` int(11) DEFAULT NULL,
  `respuesta2` int(11) DEFAULT NULL,
  `respuesta3` int(11) DEFAULT NULL,
  `respuesta4` int(11) DEFAULT NULL,
  `respuesta5` int(11) DEFAULT NULL,
  `respuesta6` int(11) DEFAULT NULL,
  `respuesta7` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `respuesta8` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `respuesta9` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fecha` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `retroalimentacion`
--

LOCK TABLES `retroalimentacion` WRITE;
/*!40000 ALTER TABLE `retroalimentacion` DISABLE KEYS */;
INSERT INTO `retroalimentacion` VALUES (3,10,3,2,1,2,2,3,'Audio','Nada','Algo útil','2016-06-25 13:04:53');
/*!40000 ALTER TABLE `retroalimentacion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tema`
--

DROP TABLE IF EXISTS `tema`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tema` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `id_materia` varchar(11) NOT NULL DEFAULT '',
  `nombre` varchar(255) DEFAULT NULL,
  `orden` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `tema_materia` (`id_materia`),
  CONSTRAINT `tema_materia` FOREIGN KEY (`id_materia`) REFERENCES `materia` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=164 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tema`
--

LOCK TABLES `tema` WRITE;
/*!40000 ALTER TABLE `tema` DISABLE KEYS */;
INSERT INTO `tema` VALUES (4,'2','Introducción',1),(5,'2','Primer tema',2),(6,'2','Conclusión',3),(7,'MT3201','Tema I',3),(8,'MT3201','Tema II',1),(9,'MT3201','Tema III',2),(67,'3','Tema I',1),(68,'3','Tema II',2),(114,'5','Arquitectura Distribuida',1),(115,'5','Cliente - Servidor',2),(158,'4','Tema A01',1),(159,'4','Tema A02',2),(160,'4','Tema A03',3),(163,'1','Tema 1',1);
/*!40000 ALTER TABLE `tema` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tipo_contenido`
--

DROP TABLE IF EXISTS `tipo_contenido`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tipo_contenido` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tipo_contenido`
--

LOCK TABLES `tipo_contenido` WRITE;
/*!40000 ALTER TABLE `tipo_contenido` DISABLE KEYS */;
INSERT INTO `tipo_contenido` VALUES (1,'Texto'),(2,'Imagen'),(3,'Video'),(4,'Audio'),(5,'Evaluación'),(6,'Presentación');
/*!40000 ALTER TABLE `tipo_contenido` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tipo_usuario`
--

DROP TABLE IF EXISTS `tipo_usuario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tipo_usuario` (
  `id_usuario` int(11) unsigned NOT NULL,
  `id_tipo` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id_usuario`,`id_tipo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tipo_usuario`
--

LOCK TABLES `tipo_usuario` WRITE;
/*!40000 ALTER TABLE `tipo_usuario` DISABLE KEYS */;
INSERT INTO `tipo_usuario` VALUES (1,2),(1,3),(3,1),(3,2),(3,3),(4,2),(5,2),(8,3),(9,3),(10,3),(11,3),(12,3),(13,3),(14,3),(15,3),(18,3),(19,2),(20,3),(21,3),(22,3),(23,3),(96,3),(97,3),(98,3),(99,3),(100,3),(101,3),(102,3),(103,3),(104,3),(105,3),(106,3),(107,3),(108,3),(109,3),(110,2);
/*!40000 ALTER TABLE `tipo_usuario` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tipos_usuarios`
--

DROP TABLE IF EXISTS `tipos_usuarios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tipos_usuarios` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tipos_usuarios`
--

LOCK TABLES `tipos_usuarios` WRITE;
/*!40000 ALTER TABLE `tipos_usuarios` DISABLE KEYS */;
INSERT INTO `tipos_usuarios` VALUES (1,'Administrador'),(2,'Profesor'),(3,'Estudiante');
/*!40000 ALTER TABLE `tipos_usuarios` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuario_departamento`
--

DROP TABLE IF EXISTS `usuario_departamento`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usuario_departamento` (
  `id_usuario` int(11) unsigned NOT NULL,
  `id_departamento` varchar(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_usuario`,`id_departamento`),
  KEY `departamento_usuario` (`id_departamento`),
  CONSTRAINT `departamento_usuario` FOREIGN KEY (`id_departamento`) REFERENCES `departamento` (`id`),
  CONSTRAINT `usuario_departamento` FOREIGN KEY (`id_usuario`) REFERENCES `usuarios` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuario_departamento`
--

LOCK TABLES `usuario_departamento` WRITE;
/*!40000 ALTER TABLE `usuario_departamento` DISABLE KEYS */;
INSERT INTO `usuario_departamento` VALUES (3,'COM'),(4,'COM'),(18,'COM'),(19,'COM'),(110,'COM'),(1,'EDU'),(4,'EDU'),(5,'EDU'),(10,'EDU'),(18,'FIS'),(19,'FIS'),(18,'MAT');
/*!40000 ALTER TABLE `usuario_departamento` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuario_grupo`
--

DROP TABLE IF EXISTS `usuario_grupo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usuario_grupo` (
  `id_usuario` int(11) unsigned NOT NULL,
  `id_grupo` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id_usuario`,`id_grupo`),
  KEY `usuariogrupo_grupo` (`id_grupo`),
  CONSTRAINT `usuariogrupo_grupo` FOREIGN KEY (`id_grupo`) REFERENCES `grupos` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuario_grupo`
--

LOCK TABLES `usuario_grupo` WRITE;
/*!40000 ALTER TABLE `usuario_grupo` DISABLE KEYS */;
INSERT INTO `usuario_grupo` VALUES (96,1),(97,1),(98,1),(99,1),(100,1),(101,1),(102,1),(103,1),(104,1),(105,1),(106,1),(107,1),(108,1),(109,1),(3,2),(20,2),(21,2),(22,2),(23,2),(96,3),(97,3),(98,3),(99,3),(100,3),(103,3),(104,3),(105,3),(106,3),(107,3),(108,3),(109,3),(3,10),(20,10),(21,10),(22,10),(23,10);
/*!40000 ALTER TABLE `usuario_grupo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuarios`
--

DROP TABLE IF EXISTS `usuarios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usuarios` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) DEFAULT '',
  `matricula` varchar(10) DEFAULT NULL,
  `correo` varchar(255) DEFAULT NULL,
  `curriculum` varchar(10000) DEFAULT NULL,
  `contrasena` varchar(255) DEFAULT NULL,
  `nivel_estudios` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=111 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuarios`
--

LOCK TABLES `usuarios` WRITE;
/*!40000 ALTER TABLE `usuarios` DISABLE KEYS */;
INSERT INTO `usuarios` VALUES (1,'Diego RamÃ­rez EchavarrÃ­a','a123','diegoraech1@gmail.com','Licenciado en ProducciÃ³n ElectrÃ³nica y DiseÃ±o de Audio. Candidato a Maestro en Ciencias de la ComputaciÃ³n.','$2y$10$9miyQ9GLjtP6Jtk0kGOQjuGOQ0Sk56axnwoZB9hSlzi.dec339M4O',1),(3,'Diego Ramírez Echavarría','A01336922','diegoraech@gmail.com','Licenciado en Producción Electrónica y Diseño de Audio. Candidato a Maestro en Ciencias de la Computación.','$2y$10$6nZ1PXDsonxhBLio/E7Av.8mUE9/yNVu3ljS1zmIJkUTYXzUE.LdS',2),(4,'Julieta Noguez','L01001010','jnoguez@itesm.mx','Profesora/Investigadora\\nGrupo de Investigación e Innovación en Educación\\nEscuela de Educación, Humanidades y Ciencias Sociales','$2y$10$uUpH2UwT6qtzVqg5H.1hq.wvDzBPF0M8KBOrkBcKxcyFqAdZ.lQ9G',8),(5,'Juan Pérez Martínrez','A112','prueb3a@itesm.mx','Edité a Juanito, le puse la contraseña \'00000000\' por mientras','$2y$10$BjCY0KxHBaO0WHkpubvPP.cFZqFUJQI2KkULjNrYqVtMB5c6k8a.u',5),(6,'José Rodríguez','A01112233','jrodriguez@correo.com','curriculum','12345678',3),(7,'Arturo Chávez','A01336924','achavez@correo.com','curriculum','$2y$10$obfCjEmc76VkR1EWSlhro.gUs8eLr4k3iTSEqF85xGjworuYP0Q4e',3),(8,'Matías Fuentes','A01336925','mfuentes@correo.com','curriculum','$2y$10$kjzYPRDK1Ib8q/90G6jwHOAuu25.91U/hEZaictkq5sD5G5IKoysq',3),(9,'Andrea Suárez','A01326455','asuarez@prueba.com','curriculum','$2y$10$Pho12yv6zDmYJV9FIbojXeQgfW4T8mq6w5.tyqrBPAFbsueedjMte',3),(10,'María Rodríguez','A01326454','arodriguez@prueba.com','curriculum','$2y$10$aLYkZ/k7rqpC0OtjpUopdOvJ2TE1xg/9xHl7jg0uVLm5wIFuBCzdC',4),(11,'Rodrigo Pérez','A01326450','rperez@correo.com','curriculum','$2y$10$RjbxaIaJyOV.P.uMrNKez.iBAOgmkBN9UXkprEyy3vbKJliwRABgS',4),(12,'Fernanda Rangel','A01326448','frangel@correo.com','curriculum','$2y$10$LG5j0cAk/KSSegSnaWi4Zu9Jg.GN1cwm6g7Hetz6bKNqiYxsT/xti',4),(13,'Jorge Jiménez','A01326457','jjimenez@correo.com','curriculum','$2y$10$3AqxUvJ.3QYxK5bgRB06tuAV5I693d4SvhpEK0roxXWY5Hxf/6ak6',4),(14,'Ana Aguilera','A01326455','aaguilera@correo.com','CV','$2y$10$jZ7xRKJHU9gb4exPD3HztOxXOZeTXx1dCjAY5llETpvIU8rJ.EJHm',4),(15,'Jaime Ortiz','A01326459','jortiz@correo.com','CV','$2y$10$WONe5ZWQI0mkr22issDpFeYX71CxTgpBAoQN3LBq6itXN6UBgc68S',4),(18,'Jorge Alcántara','A01326465','jalcantara@correo.com','CV','$2y$10$EzKRY67GRJP/98RQSXGovei8AFnk5BVAvaaVNi3cIKEgs3mXVCOZW',4),(19,'Jesús Hernández','A01326452','jhernandez@correo.com','CV','$2y$10$sTTh31kbdUWLadt4082Cg.8.u993jdS54esxVTD14aaBKZxHkOfDe',4),(20,'Juan','a01111111','juan@juan.com','','$2y$10$7UteQIcp/wdjSbm4Qn0mD.GoWu3N0oQ2VW7dr/Do7tBPj/gUYIgl.',0),(21,'Ramón','a01111112','ramon@ramon.com','','$2y$10$Op6ObJ/93bpoBowCcM2fRO9ZunFsjFXKaVz2AUKX0gOowx/VkovFC',0),(22,'Andrea','a01111113','andrea@andrea.com','','$2y$10$XMCis.0fL8/lvk8FyyGmpeWAAsTBzGjvO7wY0G77/AgLNpe2qUDpG',0),(23,'Jorge','a0123456','jorge@jorge.com','','$2y$10$rlz3pOK/FDn8.3dxj/lHh.ceW9rHXDrR9hLpV.DZSjCI3WWuNhbQ2',0),(96,'Issac Arzate Soto','A01214624','A01214624@itesm.mx','','$2y$10$lfCN23BpBJrSqmtbiAjatuGJa1DjZWYKRyBFXMcNSRCvaFTX6Qhy6',0),(97,'Jorge Rodrigo Ruíz Alvarez','A01272832','A01272832@itesm.mx','','$2y$10$JSiY74W7yReCHvqBOAgBIuQSA8WOJdETzcw.AObzD7WO5EUvCIsam',0),(98,'Daniela Martín Martínez','A01323113','A01323113@itesm.mx','','$2y$10$Hkqs5nKW8aj2vuxB1d4Dv.jVYyKByR5Iq7t/yrT8Kq5lzKX7Ygsiu',0),(99,'José María Rendón Rodríguez','A01332732','A01332732@itesm.mx','','$2y$10$csyjTeTas/a3hGiWbvm40.7465.7keEuQdn8frYbyYoP2NuSl62OG',0),(100,'Jorge Arturo César Martínez','A01334126','A01334126@itesm.mx','','$2y$10$aJ.1JCoBhOENMmHBaOZutOsSNKX/Reza4YmEiBZYz7F8VKVmnNN5.',0),(101,'Carlos Rogger Lazaro Barcena','A01335167','A01335167@itesm.mx','','$2y$10$bD9Y/CVsspnJ/Fg7LYlVLe8kYbxKt5nksQ7.P7ZNdOsyyUUUfTrrG',0),(102,'Pablo Enríquez Escamilla','A01336386','A01336386@itesm.mx','','$2y$10$8zAmNwycDO1EodXgapkFOOkBxFPU3Udm91v3B3mIDqUzJu04CNjqm',0),(103,'Jacobo Misael Tapia De la Rosa','A01336590','A01336590@itesm.mx','','$2y$10$4A0KocRgr/P8kqLrWe4M3uA48mIR30FyTqy7E5ZjejbnhG3v3IlNG',0),(104,'Ketzia Lizette Dante-Hidalgo Bouchot','A01336592','A01336592@itesm.mx','','$2y$10$vT3H.5wizas7r6l1FWrYbOAld4KtuHjNP20EpZtbVFNfSdpYaB2FO',0),(105,'Alejandro Ruíz Sánchez','A01337362','A01337362@itesm.mx','','$2y$10$Kv/BOO1qgMSL0RHhW9cwkek9KvWBkPbvtv1OSz3iRBNX.7qra2ii6',0),(106,'Itzel Daniela Reyes Perez','A01337712','A01337712@itesm.mx','','$2y$10$NAUfv.vGMmlAELyGBAvHI.7Uer/MbfdeHTyfmywidl83uc4GKSMni',0),(107,'Ricardo Raúl Salcedo Trejo','A01337828','A01337828@itesm.mx','','$2y$10$YoPM6zWGRXQleI7lWg4bMe7rUR6s.XKXKj3qcwD312whtl2de50VC',0),(108,'Carlos Bustani Granguillhome','A01338021','A01338021@itesm.mx','','$2y$10$.3Ug6AHvDZxRc7Ire/ZipOviA2pL60bx7I4kqeFQOdVhsL9CkGzIi',0),(109,'Alan Augusto Herrera Sanchez','A01338336','A01338336@itesm.mx','','$2y$10$Z20Ci7pAi3fyIj932/udI.1.Pu9QzQ4XbzeAYWvZYeAKcZCkYHgQ6',0),(110,'Joaquín Salazar','L01112233','joaquin@joaquin.com','','$2y$10$Qy4CH8.gI0uVyA2ePBsBo.Hu2KZsWu2C7sTzXL.t68dm6WTjUKgT.',7);
/*!40000 ALTER TABLE `usuarios` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-06-27 20:34:27
