<?php
// Resultados obtenidos en la aplicación del ejercicio

include('inc/header.php');

echo '<link rel="stylesheet" href="inc/css/resultados.css"/>';

error_reporting(E_ALL);
ini_set('display_errors', 1);


if( isset($usuario) && !empty($usuario) ) {
	
	if( in_array( 1, $usuario->getTipo() ) ) {
		echo 'ES ADMINISTRADOR';
	} // if( in_array( 1, $usuario->getTipo ) ) {
	
	if( isset($_GET['idgrupo']) ) {
		
		require_once( 'inc/db/bd.class.php');
		$bd = new BD();
		
		$idGrupo = intval($_GET['idgrupo']);
		
		$existeGrupo = $bd->query('SELECT id_materia FROM grupos WHERE id = ' . $idGrupo);
		
		if( $existeGrupo == false ) {
			echo 'Hubo un error con la base de datos: ' . $bd->error();
		} else if( $existeGrupo->num_rows > 0 ) {
			
			require_once('inc/clases/grupo.class.php');
			$grupo = new Grupo( $idGrupo );
			
			require_once('inc/clases/materia.class.php');
			$materia = new Materia( $grupo->getIdMateria() );
			
			echo '<div class="main">';
			
			echo '<h1>' . $materia->getNombre() . '</h1>';
			echo '<h3>Grupo ' . $grupo->getNumero() . '</h3>';
			
			foreach( $grupo->getEstudiantes() as $estudiante ) {
				
				echo '<section class="estudiante">';
				
				echo '<h2 class="nombre-estudiante">' . $estudiante->getNombre() . '</h2>';
				echo '<h3>' . $estudiante->getMatricula() . '</h3>';
				
				if( !$estudiante->getRegistrado() ) {
					echo '<h4 class="usuario-no-registrado">Usuario no registrado</h4>';
				} else {
				
					$preferencias = $estudiante->getPreferencias();
					
					if( !empty($preferencias) ) {
						
						$resultadoColor = '<div id="resultado-color">';
						
						$resultadoColor .= '<div class="muestra-color" style="background: #' . $preferencias['primarioe'] . '"></div>';
						$resultadoColor .= '<div class="muestra-color" style="background: #' . $preferencias['primariob'] . '"></div>';
						$resultadoColor .= '<div class="muestra-color" style="background: #' . $preferencias['primarioc'] . '"></div>';
						$resultadoColor .= '<div class="muestra-color" style="background: #' . $preferencias['secundario1a'] . '"></div>';
						$resultadoColor .= '<div class="muestra-color" style="background: #' . $preferencias['secundario2a'] . '"></div>';
						$resultadoColor .= '</div> <!-- /resultado-color -->';
						
						
						echo $resultadoColor;
						
						$estructura = intval($preferencias['estructura']);
						
						$estructuraNum = ( $estructura >= 1 && $estructura <= 4 ) ? $estructura : 1;
						include( 'inc/materia/estructuras/estructura.php' );
						
					} // if( !empty($preferencias) ) {
					
					
					foreach( $materia->getTemas() as $tema ) {
						
						echo '<h2 class="titulo-tema">Tema: ' . $tema->getNombre() . '</h2>';
						
						$eleccionRecurso = $bd->query("SELECT id_contenido FROM eleccion_recursos WHERE id_usuario = " . intval($estudiante->getID()) . " AND id_grupo = " . intval($grupo->getID()) . " AND id_tema = " . intval($tema->getID()) );
						
						if( $eleccionRecurso == false ) {
							echo 'Hubo un error con la base de datos: ' . $bd->error();
						} else if( $eleccionRecurso->num_rows > 0 ) {
							
							$idContenido = 0;
							
							foreach( $eleccionRecurso as $idElegido ) {
								$idContenido = $idElegido['id_contenido'];
								break;
							} // foreach( $eleccionRecurso as $idElegido ) {
							
							if( $idContenido != 0 ) {
								require_once('inc/clases/contenido.class.php');
								$contenido = new Contenido( $idContenido );
								$tipoDeContenido = $contenido->getTipoDeContenido();
								
								echo '<h2>Recurso elegido: ' . $tipoDeContenido->getNombre() . '</h2>';
							} // if( $idContenido != 0 ) {
						} else {
							echo '<h2>Sin recurso elegido para ' . $tema->getNombre() . '</h2>';
						} // if( $eleccionRecurso == false ) { ... else if ...
						
						
						$evaluaciones = $bd->query("SELECT contenidos.nombre, evaluaciones.*
													FROM evaluaciones
													JOIN contenidos
													ON evaluaciones.id_contenido = contenidos.id
													WHERE contenidos.tipo_de_contenido = 5
													AND contenidos.id_tema = " . intval($tema->getID()) 
												);
						
						if( $evaluaciones == false ) {
							echo 'Hubo un error con la base de datos: ' . $bd->error();
						} else if( $evaluaciones->num_rows > 0 ) {
							
							echo '<section class="resultados-evaluacion">';
							
							
							foreach( $evaluaciones as $evaluacion ) {
								
								$correctas =  explode(',', $evaluacion['correctas']);
								
								$calificaciones = $bd->query("SELECT calificacion, fecha
															FROM calificaciones
															WHERE id_contenido = " . intval($evaluacion['id_contenido']) .
															" AND id_usuario = " . intval($estudiante->getID()) .
															" ORDER BY fecha" );
								
								
								if( $calificaciones == false ) {
									echo 'Hubo un error con la base de datos: ' . $bd->error();
								} else if( $calificaciones->num_rows > 0 ) {
									foreach( $calificaciones as $calificacion ) {
										echo '<h2 class="calificacion">Calificación: ' . round($calificacion['calificacion'],2) . '% <span>' . $calificacion['fecha'] . '</span></h2>';
									} // foreach( $calificaciones as $calificacion ) {
								} // if( $calificaciones == false ) {
								
								break;
								
								
								
								
								
								
								/*
								echo '<section class="resultados-pregunta">';
									
								echo '<h3 class="titulo-pregunta">Pregunta: ' . $evaluacion['pregunta'] . '</h3>';
								
								if( !empty( $evaluacion['imagenPregunta'] ) ) {
									echo '<img class="imagen-pregunta" src="' . BASEDIR . '/uploads/' . $evaluacion['imagenPregunta'] . '"/>';
								} // if( !empty( $evaluacion['imagenPregunta'] ) ) {
								
								echo '<h3>Respuestas elegidas</h3>';
								
								$fechaAnterior = -1;
								$fechaActual = 0;
								
								$respuestas = $bd->query("SELECT respuestas, fecha
															FROM respuestas_evaluaciones
															WHERE id_usuario = " . intval($estudiante->getID()) .
															" AND id_contenido = " . intval($evaluacion['id_contenido']) .
															" AND orden = " . intval($evaluacion['orden']) .
															" AND fecha = '" . $calificacion['fecha'] . "';"
															//" ORDER BY fecha" 
														);
								
								if( $respuestas == false ) {
									echo 'Hubo un error con la base de datos: ' . $bd->error();
								} else if( $respuestas->num_rows > 0 ) {
									foreach( $respuestas as $respuesta ) {
										$respuestasEstudiante = explode(',', $respuesta['respuestas']);
										
										foreach( $respuestasEstudiante as $numRespuesta ) {
											
											if( $fechaAnterior != $fechaActual ) {
												if( !empty($fechaActual) ) {
													echo '<h4>' . $fechaActual . '</h4>';
												} else {
													echo '<h4>Sin fecha</h4>';
												} // if( !empty($fechaActual) ) { ... else ...
												$fechaAnterior = $fechaActual;
											} // if( $fechaAnterior != $fechaActual ) {
											
												
											echo '<div class="respuesta-elegida">';
											
											echo '<p>' . $evaluacion['respuesta_' . ($numRespuesta+1)] . '</p>';
											
											$fechaActual = $respuesta['fecha'];
											
											
											
											
											if( !empty($evaluacion['imagenRespuesta_' . ($numRespuesta+1)]) ) {
												echo '<img src="' . BASEDIR . '/uploads/' . $evaluacion['imagenRespuesta_' . ($numRespuesta+1)] . '" />';
											} // if( !empty($evaluacion['imagenRespuesta_' + ($numRespuesta+1)]) ) {
											
											echo '</div> <!-- /respuesta-elegida -->';
											
										} // foreach( $respuestasEstudiante as $numRespuesta ) {
										
									} // foreach( $respuestas as $respuesta ) {
								} // if( $respuestas == false ) {
								
								echo '</section> <!-- /resultados-pregunta -->';
								*/
								
								
								
								
								
								
								
							} // foreach( $evaluaciones as $evaluacion ) {
							
							echo '</section> <!-- /resultados-evaluacion -->';
							
						} // if( $evaluaciones == false ) { ... else if ...
						
					} // foreach( $materia->getTemas() as $tema ) {
					
					
				
				} // if( !$estudiante->getRegistrado() ) { ... else ...
				
				echo '</section> <!-- /estudiante-' . $estudiante->getID() . ' -->';
				
			} // foreach( $grupo->getEstudiantes() as $estudiante ) {
			
			echo '</div> <!-- /main -->';
		} else {
			echo '<h1>El grupo seleccionado no existe</h1>';
		} // if( $existeGrupo == false ) { ... else if ... else ...
		
	} else {
		echo '<h1>El grupo seleccionado no existe</h1>';
	} // if( isset($_GET['idgrupo']) ) { ... else ...
	
} // if( isset($usuario->getID()) ) {

?>