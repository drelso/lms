<?php
// Realiza la actualización de la materia
// en la base de datos

$data = json_decode(file_get_contents('php://input'), true);
//print_r($data);


require_once(  __DIR__ . '/inc/clases/materia.class.php' );
require_once(  __DIR__ . '/inc/clases/contenido.class.php' );
require_once(  __DIR__ . '/inc/clases/evaluacion.class.php' );
require_once(  __DIR__ . '/inc/clases/tema.class.php' );


$idMateria = $data['id'];
$nombreMateria = $data['nombreMateria'];
$departamento = $data['departamento'];

$materia = new Materia( $idMateria );

$materia->setNombre( $nombreMateria );
$materia->setDepartamento( $departamento );

$materia->borrarContenido();

/*
echo 'ID: ' . $idMateria . '<br>';
echo 'Nombre: ' . $nombreMateria . '<br>';
echo 'Departamento: ' . $departamento . '<br>';

echo 'Nuevo módulo:';
*/

$ordenTema = 1;

foreach( $data['temas'] as $tema ) {
	
	//echo 'Tema ' . $ordenTema . ': ' . $tema['nombre'] . '<br>';
	
	$nuevoTema = Tema::agregarTema( $idMateria, $ordenTema, $tema['nombre'] );
	
	$ordenContenido = 1;
	
	foreach( $tema['contenidos'] as $contenido ) {
		
		$nombre		= $contenido['nombre'];
		$tipo		= $contenido['tipo'];
		$archivo	= $contenido['archivo'];
		$iframe		= $contenido['iframe'];
		$texto		= $contenido['texto'];
		
		/*
		echo 'Contenido: ' . $nombre . '<br>';
		echo 'Tipo: ' . $tipo . '<br>';
		echo 'Archivo: ' . $archivo . '<br>';
		echo 'Texto: ' . $texto . '<br>';
		*/
		
		
		if( count( $contenido['preguntas'] ) == 0 ) {
			
			$archivoContenido = ( !empty( $archivo ) ) ? $archivo : NULL;
			$iframeContenido = ( !empty( $iframe ) ) ? $iframe : NULL;
			
			$contenidoObjeto = Contenido::agregarContenido( $nuevoTema->getID(), $nombre, $texto, 0, $tipo, $ordenContenido, $archivoContenido, $iframeContenido );
			
		} else {
			
			$ordenReactivo = 1;
			$reactivos = array();
			
			foreach( $contenido['preguntas'] as $pregunta ) {
				
				//echo 'Pregunta: ' . $pregunta['pregunta'] . '<br>';
				
				$reactivos[$ordenReactivo] = array();
				
				$reactivos[$ordenReactivo]['pregunta'] = $pregunta['pregunta'];
				$reactivos[$ordenReactivo]['imagen'] = $pregunta['imagen'];
				
				if( count( $pregunta['respuestas'] ) > 0 ) {
					
					$reactivos[$ordenReactivo]['respuestas'] = array();
					$reactivos[$ordenReactivo]['correctas'] = array();
					$reactivos[$ordenReactivo]['orden'] = $ordenReactivo;
					
					foreach( $pregunta['respuestas'] as $respuesta ) {
						
						echo 'Respuesta: ' . $respuesta['respuesta'] . ' ' . $respuesta['correcta'] . ' ' . $respuesta['imagen'] . '<br>';
						
						$numElementos = array_push(
												$reactivos[$ordenReactivo]['respuestas'],
												array($respuesta['respuesta'], $respuesta['imagen'])
											);
						
						if( $respuesta['correcta'] ) { array_push($reactivos[$ordenReactivo]['correctas'], ( $numElementos - 1 ) ); }
						
					} // foreach( $pregunta['respuestas'] as $respuesta ) {
					
				} // if( count( $pregunta['respuestas'] ) > 0 ) {
				
				// Incrementa el orden en que se almacenan los contenidos
				$ordenReactivo++;
					
			} // foreach( $contenido['preguntas'] as $pregunta ) {
			
			
			Evaluacion::agregarEvaluacion( $nuevoTema->getID(), $nombre, $texto, 0, $tipo, $ordenContenido, $reactivos );
			
			
			//var_dump($reactivos);
			
		} // if( count( $contenido['preguntas'] ) == 0 ) {
		
		// Incrementa el orden en que se almacenan los contenidos
		$ordenContenido++;
	
		
	} // foreach( $tema['contenidos'] as $contenido ) {
	
	// Incrementa el orden en que se almacenan los módulos
	$ordenTema++;
	
} // foreach( $data['temas'] as $tema ) {



/*
if( isset( $_POST['agregar-tema-enviado'] ) ) {

	$temas = $_POST['tema'];

	var_dump($temas);

} // if( isset( $_POST['agregar-tema-enviado'] ) ) {
*/
?>