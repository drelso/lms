<?php
// Interfaz de materia

// Agrega el encabezado global
include('inc/header.php');

ini_set("display_errors", "1");
error_reporting(E_ALL);

/*
$id = 0;
$usuario;

// Si el usuario está registrado
// instancia un objeto de clase usuario
//
if(isset($_SESSION['usuario_registrado'])) {
	require_once('inc/clases/usuario.class.php');
	
	$id = intval($_SESSION['usuario_registrado']);
	
	if($id > 0) {
		$usuario = new Usuario($id);
	} // if($id > 0) {
	
} // if(isset($_SESSION['usuario_registrado'])) {
*/

// Si existe el objeto de clase usuario
// genera la estructura del perfil
//
if(isset($usuario) && !empty($usuario)
		&& isset($_GET['id'])) {
	
	require_once('inc/func/recomendar-recurso.func.php');
	
	$estilosDeAprendizaje = $usuario->getEstilosDeAprendizaje();
	
	//var_dump($estilosDeAprendizaje);
	
	$recomendaciones = recomendarRecurso( $estilosDeAprendizaje );
	
	//var_dump($recomendaciones);
	
	require_once('inc/db/bd.class.php');
	$bd = new BD();
		
	$usuarioEnGrupo = $bd->query( "SELECT * FROM usuario_grupo WHERE id_usuario = " . $usuario->getID() . " AND id_grupo = " . $_GET['id'] );
	
	if( $usuarioEnGrupo == false ) {
		echo 'Hubo un error con la base de datos:' . $bd->error();
	} else {
	
		if( $usuarioEnGrupo->num_rows != 0 ) {
			
			require_once('inc/clases/grupo.class.php');
			require_once('inc/clases/materia.class.php');
			
			
			$grupo = new Grupo( $_GET['id'] );
			$materia = new Materia( $grupo->getIdMateria() );
			
			echo '<header class="encabezado-materia">';
			
			echo '<h1 id="titulo-materia" data-id="' . $materia->getID() . '" data-idgrupo="' . $grupo->getID() . '">' . $materia->getNombre() . '</h1>';
			
			echo '<ul id="menu-materia">';
			
			echo '<li data-id="1">Colores y Estructuras</li>';
			
			
			$temasMateria = $materia->getTemas();
			
			$contPaso = 2;
			
			foreach( $temasMateria as $tema ) {
				
				$tieneContenido = false;
				$tieneEvaluacion = false;
				
				$contenidos = Tema::getContenidos( $tema->getID() );
				
				foreach( $contenidos as $contenido ) {
					
					$tipoContenido = $contenido->getTipoDeContenido();
					
					// El tipo de contenido 5 es para evaluaciones
					if( $tipoContenido->getID() == 5 ) { $tieneEvaluacion = true; }
					else { $tieneContenido = true; }
				} // foreach( $contenidos as $contenido ) {
				
				
				if( $tieneContenido ) {
					echo '<li data-id="' . $contPaso . '">' . $tema->getNombre() . '</li>';
					$contPaso++;
				} // if( $tieneContenido ) {
					
				if( $tieneEvaluacion ) {
					echo '<li data-id="' . $contPaso . '">' . $tema->getNombre() . ' <hr>Evaluación</li>';
					$contPaso++;
				} // if( $tieneContenido ) {
				
			} // foreach( $temasMateria as $tema ) {
			
			echo '<li data-id="' . $contPaso . '">Encuesta final</li>';
			
			echo '</ul> <!-- /menu-materia -->';
			
			echo '</header> <!-- /encabezado-materia -->';
			
			
			echo '<div class="main materia">';
			
			
			echo '<section id="paso-1" class="paso-materia">';
			require_once('inc/materia/paleta-color.inc.php');
			
			require_once('inc/materia/estructuras.inc.php');
			
			echo '<h3 class="error" id="error-paso-1"></h3>';
			
			echo '<button class="siguiente-paso" data-id="1">Continuar</button>';
			
			echo '</section> <!-- /paso-1 -->';
			
			require_once('inc/materia/contenidos-materia.inc.php');
			
			echo '</div> <!-- /main -->';
			
			echo '<script src="inc/js/materia.js"></script>';
			
			// Agrega el pie de página global
			include('inc/footer.php');
			
		} else {
			header("Location: " . BASEDIR);
			die();
		} // if( $usuarioEnGrupo->num_rows == 0 ) { ... else ...
	} // if( $usuarioEnGrupo == false ) {
} // if(isset($usuario) && !empty($usuario) ...
?>